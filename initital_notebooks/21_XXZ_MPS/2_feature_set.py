from NN.TensorFlowModels import PaperNN
from helper.Batcher import Batcher
from helper.FeatureSetFactories import XXZMPSCorrelationFactory
from helper.plotting import TSNEView, XXZPhaseDiagram
import pandas as pd

if __name__ == '__main__':
    factory = XXZMPSCorrelationFactory(L=50)
    data = factory.simulate()

    # view = TSNEView(_data[:, 2::3])

    ulabel = data.ulabel

    nn = PaperNN()

    parameters = []
    for scan_direction in ["delta", "h"]:
        for window_width in [0.5]:
            # scan at every point in generated _data, leave padding for training beyond points
            scan_window = ulabel[scan_direction].between(ulabel[scan_direction].min() + window_width / 2,
                                                         ulabel[scan_direction].max() - window_width / 2,
                                                         inclusive=True)
            thresholds = data.ulabel[scan_window]
            parameters.append(Batcher.grid_search_v2(scan_direction=scan_direction,
                                                     window_width=window_width,
                                                     threshold_h=thresholds["h"].unique(),
                                                     threshold_delta=thresholds["delta"].unique()))

    parameters = pd.concat(parameters)

    p = XXZPhaseDiagram(neural_network=nn, initial_data=parameters,
                        features=data,
                        store="../../_data/19_xxz_exact/xxz_phase_diagram_0.5.h5")

    p.run_all(save_interval=10)