import numpy as np
import sys

from pandas import DataFrame
import pandas as pd

from helper.plotting import XXZMPSRawViewer, XXZMPSMagnetization, RuntimeViewer

sys.path.append('../../')
import helper.my_mps as mps
import helper.models_mps as models


def get_parameters(myObservables, L):
    myConv = mps.MPSConvParam(max_bond_dimension=20, max_num_sweeps=6,
                              local_tol=1E-14)

    parameters = []

    for h_x in np.linspace(0, 5, 22):
        for delta in np.linspace(-2.5, 2.5, 25):
            parameters.append({
                'simtype': 'Finite',
                # Directories
                'job_ID': 'XXZ',
                'unique_ID': 'hx_{}_delta_{}'.format(h_x, delta),
                'Write_Directory': 'config_L={}_TMP/'.format(L),
                'Output_Directory': 'config_L={}_OUTPUTS/'.format(L),
                # System size and Hamiltonian common_parameters
                'L': L,
                'delta': delta,
                'h_x': h_x,
                # ObservablesConvergence common_parameters
                'verbose': 0,
                'MPSObservables': myObservables,
                'MPSConvergenceParamets': myConv,
                'logfile': True
            })
    return parameters


if __name__ == '__main__':

    viewers = []
    phase_diagrams = []
    run_times = []

    for L in [10, 50]:
        # prepare model
        print("Preparing model, simulating if necessary, reading output")
        H, Operators, Observables = models.xxz_model_1d({})
        parameters = get_parameters(Observables, L=L)
        MainFiles = mps.WriteFiles(parameters, Operators, H, PostProcess=False)

        Outputs = mps.read_or_simulate(parameters, MainFiles, parallel=True)

        run_times.append(RuntimeViewer.from_outputs(Outputs))
        phase_diagrams.append(XXZMPSMagnetization.from_outputs(Outputs))
        viewers.append(XXZMPSRawViewer.from_outputs(Outputs))

    pass
