from NN.KerasModels import XXZ3Layer
from SpinData.Data_v3 import FeatureSet
from helper.plotting import XXZMagnetizationImages, XXZSpinConfigurationImages

if __name__ == '__main__':
    data = FeatureSet('../../features/xxz_exact/ordered-True_r_scaling-None_11spins_1000samples_fullh.h5')

    # view = SpinConfigurationOrderings2D(spin_configurations=_data)

    data = data[:, 2::3]

    train = {'h': (0.2, 0.4), 'delta': (-2, 0)}
    test = {'h': (0.2, 0.4), 'delta': (-1.5, -0.5)}
    train, test = data.split_label_value(train, test)

    labels_train = train.binary_single_2d_label(-1, 'delta')
    labels_test = test.binary_single_2d_label(-1, 'delta')

    nn = XXZ3Layer(data.n_features)
    nn.train(train.x, labels_train, validation_data=(test.x, labels_test),
             epochs=10, batch_size=1000, learning_rate=0.0005)

    pass