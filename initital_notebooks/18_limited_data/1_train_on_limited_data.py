import numpy as np
import sys

from helper.Trainers import NoisyTrainer
from helper.plotting import NoisyCorrelationFunctions, IsingNoiseResponse

sys.path.append('../../')
from helper import my_pyplot as plt
from helper.mps_parameters import get_output_ch17
from helper.helper import generate_mps_correlation_data_set
from NN.TensorFlowModels import PaperNN
from helper.Batcher import Batcher, BatchExecutor

if __name__ == '__main__':
    L = 700
    cutoff = 100
    r = 1
    exp = None

    load_correlations = '../../features/MPS/range{}exp{}_L={}_n=201_cut={}_full.npz'.format(r, exp, L, cutoff)

    data = np.load(load_correlations)
    correlations = data["_data"]
    g = data["labels"]

    # p = NoisyCorrelationFunctions(correlations, g)

    # prepare model
    # print("Preparing model, simulating if necessary, reading output")
    # Outputs, common_parameters = get_output_ch17(r, exp, L, directory="../17_long_range_interaction", read_only=False)

    # feat, label = generate_mps_correlation_data_set(Outputs, common_parameters, None, cutoff, L)

    parameters = Batcher.grid_search_v2(begin=0.5049999999999999, end=1.4949999999999999,
                                        noise_train_rel=np.linspace(0, 1, 5),
                                        noise_test_rel=np.linspace(0, 1, 5),
                                        noise_train_abs=0, noise_test_abs=0,
                                        n_train=np.linspace(1, 120, 11),
                                        n_test=np.linspace(1, 12000, 5))

    noisy_plotter = IsingNoiseResponse(correlations=correlations,
                                       g_list=g,
                                       store='../../_data/18_noises/little_training _data.foo.h5',
                                       draw=True)

    noisy_plotter.run_all()
