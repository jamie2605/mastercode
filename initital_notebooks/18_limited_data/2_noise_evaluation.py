from helper.Batcher import Batcher
from helper.plotting import NoiseImage
import pandas as pd

if __name__ == '__main__':
    b = Batcher('../../_data/18_noises/little_training _data.h5')
    results = b.get_completed(exclude_times=True)

    ni = NoiseImage(data=results)
    pass
