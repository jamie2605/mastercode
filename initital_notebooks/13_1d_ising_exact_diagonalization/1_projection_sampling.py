import numpy as np
import matplotlib.pyplot as plt
import typing
from datetime import datetime

import time
import os
import sys
sys.path.append("../../")

from tools.helper import sampler
from physics.models_qutip import ising_hamiltonian


def write_configuration_file(file: str, sampling_generator: typing.Iterable[str]):
    with open(file, 'w') as configuration_file:
        configuration_file.writelines(sampling_generator)


def create_data_set(directory: str, n_particle: int, hs: np.ndarray, n_train_per_h: int = 1000, n_test_per_h: int = 100):
    """
    Create directory of sampled configuration files from 1D transverse field Ising hamiltonian
    :param n_particle: Number of particles to create
    :param directory: location to create directory structure
    :param hs: at which values of h to fetch_sampling_results
    :param n_train_per_h: how many training points per h value
    :param n_test_per_h: how many test points per h value
    :return:
    """
    for subdir_name, n in zip(['train', 'test'], [n_train_per_h, n_test_per_h]):
        os.mkdir(os.path.join(directory, subdir_name))

    for i_h, h in enumerate(hs):
        print("{}: Diagonalizing Hamiltonian for h={} ({}/{})".format(datetime.now(), h, i_h, len(hs)))
        # find eigenvalues
        save_to = '/home/ben/masterphase/Mastercode/_data/13_ising_probabilities'

        ham = ising_hamiltonian(n_particle, h, J=1)
        prob_distribution = ham.ground_state_configuration_probabilities(gpu=True, save_to=save_to)

        print("Sampling configurations and saving to file")
        for subdir_name, n in zip(['train', 'test'], [n_train_per_h, n_test_per_h]):
            subdir = os.path.join(directory, subdir_name)
            file_name = os.path.join(subdir, "config_{}.dat".format(h))

            sampling_generator = sampler(prob_distribution, n)

            # fetch_sampling_results configurations and write to file
            write_configuration_file(file_name, sampling_generator)


if __name__ == "__main__":
    N = 13
    path = '/home/ben/masterphase/Mastercode/Ising/quantum/13x120temps'
    hs = np.linspace(0, 2, 120)

    create_data_set(path, N, hs, n_train_per_h=2000, n_test_per_h=1000)


