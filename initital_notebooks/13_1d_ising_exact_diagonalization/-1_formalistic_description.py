import numpy as np
import matplotlib.pyplot as plt


import qutip as qt
import time
import sys
sys.path.append("../../")

from helper.models_qutip import ising_hamiltonian as hamiltonian
from helper.plotting import color_map
from helper.helper import bitwise_boolean

up = qt.basis(2, 0)
down = qt.basis(2, 1)


def projection_op(state_no: int, dimension: int):
    return qt.tensor([up*up.dag() if b else down*down.dag() for b in bitwise_boolean(state_no, dimension)])


def state(state_no: int, dimension: int):
    return qt.tensor([up if b else down for b in bitwise_boolean(state_no, dimension)])


if __name__ == "__main__":
    N = 3
    hs = np.linspace(0, 10, 100)
    c, m = color_map(hs)

    for i, h in enumerate(hs):
        H = hamiltonian(N, h, J=1)
        E, psi = H.eigenstates()

        if E[1] - E[0] < 1e-10:
            # degenerate ground state
            print("degenerate ground state for h={}".format(h))
            ground = [psi[0], psi[1]]
        else:
            ground = [psi[0]]

        probabilities = np.zeros(2 ** N)
        for psi_0 in ground:
            for g, state_number in enumerate(range(2 ** N)):
                proj_full = projection_op(state_number, N)
                psi = state(state_number, N)

                prob = psi.dag() * proj_full * psi_0
                prob = np.real(prob.data.todense().item(0))
                probabilities[g] += prob / len(ground)

        plt.plot(probabilities, c=c[i])
        print(np.sum(probabilities))

    plt.colorbar(m)
    plt.show()
