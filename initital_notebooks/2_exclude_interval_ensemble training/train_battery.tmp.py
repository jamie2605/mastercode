import numpy as np
import matplotlib.pyplot as plt
import math
from helper.helper import nn_exclude_training
from Ising.Data import IsingData
data = IsingData("Ising/large_dataset/")
x = 1.2+0.1*np.arange(22)
x, y = np.meshgrid(x,x)
intervals = [(x, y) for x, y in zip(x.flatten(), y.flatten())]
intervals_small = [(start, end) for end, start in intervals if end > start]
intervals_small=np.array(intervals_small)


for _ in range(10):
    presults, nns = nn_exclude_training(data.train_data, 
                                      nn="Paper", 
                                      exclude=intervals_small,
                                      keepNN=False,
                                      n_epochs=6,
                                      save_results_single="_data/exclude_data/huge_PNN_std")
