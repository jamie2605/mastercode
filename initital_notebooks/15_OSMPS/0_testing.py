import numpy as np
import sys

sys.path.append('../../')
import helper.my_mps as mps
from helper.plotting import *

from helper.models_mps import ising_model_1d as build_model
from helper.events import MPSResultHandler
from helper.plotting import mps_plot_mag_and_runtime


def get_parameters(myObservables):
    myConv = mps.MPSConvParam(max_bond_dimension=20, max_num_sweeps=6,
                              local_tol=1E-14)

    # Specify constants and parameter lists
    J = 1.0
    glist = np.linspace(0.5, 2, 99)
    parameters = []
    L = 200

    for g in glist:
        parameters.append({
            'simtype': 'Finite',
            # Directories
            'job_ID': 'Ising_Statics',
            'unique_ID': 'g_' + str(g),
            'Write_Directory': '1_TMP/',
            'Output_Directory': '1_OUTPUTS/',
            # System size and Hamiltonian common_parameters
            'L': L,
            'J': J,
            'g': g,
            # ObservablesConvergence common_parameters
            'verbose': 0,
            'MPSObservables': myObservables,
            'MPSConvergenceParamets': myConv,
            'logfle': False
        })
    return parameters


if __name__ == '__main__':

    # prepare model
    H, Operators, Observables = build_model()
    parameters = get_parameters(Observables)
    MainFiles = mps.WriteFiles(parameters, Operators, H, PostProcess=False)

    result_handler = MPSResultHandler()
    mps_plot_mag_and_runtime(result_handler, parameters)

    # run mps optimization
    mps.runMPS(MainFiles, parallel=True, async=True, result_handler=result_handler)

