import numpy as np
import sys

sys.path.append('../../')
import helper.my_mps as mps
import matplotlib.pyplot as plt
from helper.plotting import color_map
from helper.plotting import save_to_plot_stream, CorrelationZZAnimation, plot_magnetizations

import helper.models_mps as models
from helper.mps_parameters import get_parameters_ch17
from time import sleep


if __name__ == '__main__':

    # prepare model
    H, Operators, Observables = models.ising_model_1d(700)
    parameters = get_parameters_ch17(Observables, prepend_file="config_", L=700)
    MainFiles = mps.WriteFiles(parameters, Operators, H, PostProcess=False)
    Outputs = mps.read_or_simulate(parameters, MainFiles, force_simulate=False)

    glist = [p['g'] for p in parameters]
    sigmax_list = [abs(np.mean(o['sigmax'])) for o in Outputs]
    zz = [o['zz'] for o in Outputs]

    plt.rc('text', usetex=True)
    zfig, xfig = plot_magnetizations(glist, zz, sigmax_list)
    c = CorrelationZZAnimation(zz, glist)

    save_to_plot_stream([zfig, xfig, c.fig])

    plt.show()
    pass




