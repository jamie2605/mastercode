import numpy as np
import sys

sys.path.append('../../')
import helper.my_mps as mps
from helper import my_pyplot as plt
from helper.plotting import color_map
from helper.plotting import view_mean_features
from helper.helper import get_all_feature_vectors

import helper.models_mps as models
import time


def get_parameters(myObservables, L=20):
    myConv = mps.MPSConvParam(max_bond_dimension=20, max_num_sweeps=6,
                              local_tol=1E-14)

    # Specify constants and parameter lists
    J = 1.0
    glist = np.linspace(0.01, 1.99, 201)
    parameters = []

    for g in glist:
        parameters.append({
            'simtype': 'Finite',
            # Directories
            'job_ID': 'Ising_Statics',
            'unique_ID': 'g_' + str(g),
            'Write_Directory': 'config_L={}_TMP/'.format(L),
            'Output_Directory': 'config_L={}_OUTPUTS/'.format(L),
            # System size and Hamiltonian common_parameters
            'L': L,
            'J': J,
            'g': g,
            # ObservablesConvergence common_parameters
            'verbose': 0,
            'MPSObservables': myObservables,
            'MPSConvergenceParamets': myConv,
            'logfile': True,
            'Discrete_generators': ['gen'],
            'Discrete_quantum_numbers': [0]
        })
    return parameters


if __name__ == '__main__':

    L = 700
    cutoff = 100
    save_to = '../../Ising/MPS/L={}_n=201_cut={}_full.npz'.format(L, cutoff)

    # prepare model
    print("Preparing model, simulating if necessary, reading output")
    H, Operators, Observables = models.ising_model_1d(L, observe_transverse_mag=True,
                                                      observe_density_matrix=False, observe_zz_corr=True)
    parameters = get_parameters(Observables, L=L)
    MainFiles = mps.WriteFiles(parameters, Operators, H, PostProcess=False)

    Outputs = mps.read_or_simulate(parameters, MainFiles, parallel=True)

    glist = [p['g'] for p in parameters]
    sigmax_list = [abs(np.mean(o['sigmax'])) for o in Outputs]
    zz = [o['zz'] for o in Outputs]

    print("Creating _data set")
    feat = get_all_feature_vectors(zz, cutoff=cutoff)
    label = np.repeat(glist, L-cutoff)

    print("Generating plot")
    view_mean_features(feat, label, cutoff, L)

    print("saving to {}".format(save_to))
    np.savez(save_to, features=feat, labels=label)
