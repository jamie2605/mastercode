import numpy as np
import sys

sys.path.append('../../')
import helper.my_mps as mps
from helper.plotting import *

from helper.Batcher import Batcher
from helper.helper import *

if __name__ == '__main__':

    all_plots = False

    # prepare batcher_class
    b = Batcher("../../_data/15_MPS-L-dependence/initial.h5")
    df = b.get_completed(exclude_times=True)

    # load _data
    plot_data = []
    for i, row in df.iterrows():
        if row.L < 27:
            continue

        print(row.L)

        g, mag = read_magnetization_mps(row.parameters)
        order = np.argsort([t[0] for t in row.result])
        execution_times = [row.result[o][1] for o in order]

        plot_data.append((row.L, g, mag, execution_times))

    # plot_data
    ex_plot, ex_ax = plt.subplots()
    mag_plot, mag_ax = plt.subplots()
    ex_scale_plot, ex_scale_ax = plt.subplots()

    for L, g, mag, execution_times in plot_data:
        ex_ax.semilogy(g, execution_times, label=L)
        mag_ax.plot(g, mag, label=L)

    ex_ax.legend(title='L')
    ex_ax.set_ylabel("execution time/s")
    ex_ax.set_xlabel("g")
    ex_plot.draw()

    mag_ax.legend(title='L')
    mag_ax.set_ylabel("magnetization")
    mag_ax.set_xlabel("g")
    mag_plot.draw()

    total_times = [sum(t[3]) for t in plot_data]
    L = [t[0] for t in plot_data]
    plt.scatter(L, total_times)

    fit = np.polyfit(L, total_times, 3)
    l = np.linspace(min(L), max(L))
    plt.plot(l, np.poly1d(fit)(l))

    plt.ylabel("Total execution time/s")
    plt.xlabel("L")
    plt.show()








