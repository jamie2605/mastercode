import numpy as np
import sys

sys.path.append('../../')
import helper.my_mps as mps
from helper.plotting import *

import helper.models_mps as models
from helper.events import MPSResultHandler
from helper.plotting import mps_plot_mag_and_runtime
from helper.Batcher import Batcher


def get_parameters(myObservables, L=200):
    myConv = mps.MPSConvParam(max_bond_dimension=20, max_num_sweeps=6,
                              local_tol=1E-14)

    # Specify constants and parameter lists
    J = 1.0
    glist = np.linspace(0.1, 2, 100)
    parameters = []

    for g in glist:
        parameters.append({
            'simtype': 'Finite',
            # Directories
            'job_ID': 'Ising_Statics',
            'unique_ID': 'g_' + str(g),
            'Write_Directory': 'L={}_TMP/'.format(L),
            'Output_Directory': 'L={}_OUTPUTS/'.format(L),
            # System size and Hamiltonian common_parameters
            'L': L,
            'J': J,
            'g': g,
            # ObservablesConvergence common_parameters
            'verbose': 0,
            'MPSObservables': myObservables,
            'MPSConvergenceParamets': myConv,
            'logfle': False
        })
    return parameters


if __name__ == '__main__':

    # prepare model
    H, Operators, Observables = models.ising_model_1d(700)

    # prepare batcher_class
    b = Batcher("../../_data/15_MPS-L-dependence/initial.h5")
    b.set_data(Batcher.grid_search(L=[10, 20, 50, 100, 200, 300, 500, 700], parameters=None, result=None))

    handlers = []
    plotters = []
    for row in b.run():
        parameters = get_parameters(Observables, L=row.L)
        MainFiles = mps.WriteFiles(parameters, Operators, H, PostProcess=False)

        result_handler = MPSResultHandler()
        handlers.append(result_handler)
        row.parameters = parameters

        def save_row():
            row.result = result_handler.result_list

        result_handler.on_completed(save_row)

        # start plots
        plotters.append(mps_plot_mag_and_runtime(result_handler, parameters))

        # run mps optimization
        mps.runMPS(MainFiles, parallel=True, async=True, result_handler=result_handler)

        # wait for next loop iteration
        result_handler.wait_til_completed()
