import numpy as np
import matplotlib.pyplot as plt
import os
import sys
sys.path.append("../../")

from Ising.Data_v2 import IsingData, ConfigurationSet
from helper.correlation_entropy import calculate_correlation_entropy
from helper.plotting import color_map


def calculate_all_entropies(config_set: ConfigurationSet, data_dir='../../_data/entropy/alps/', plot=False):
    correlations = []
    full_correlation = []
    for t in config_set.u_label:
        print(t)
        configs = config_set.configuration[d.train.temperature == t]

        corr, corr_r = calculate_correlation_entropy(configs, l=int(np.sqrt(d.train.n_features)))

        if plot:
            plt.plot(corr_r)
            plt.title(t)
            plt.show()

        correlations.append(corr_r)
        full_correlation.append(corr)

    correlations = np.array(correlations)
    full_correlation = np.array(full_correlation)

    np.save(os.path.join(data_dir, 'temps'), config_set.u_label)
    np.save(os.path.join(data_dir, 'corr_entropy_r'), correlations)
    np.save(os.path.join(data_dir, 'corr_entropy'), full_correlation)

    return config_set.u_label, correlations, full_correlation


def plot_entropies(temp, corr_r):
    rs = np.arange(np.shape(corr_r)[1])
    c, m = color_map(rs)
    for r in rs:
        plt.plot(temp, corr_r[:, r], c=c[r])

    plt.colorbar(m)
    plt.show()


if __name__ == "__main__":
    d = IsingData("../../Ising/alps_thermal_small")

    #temp, corr, corr_r = calculate_all_entropies(d_npz.train)

    temp = np.load('../../_data/entropy/alps/temps.npy')
    corr_r = np.load('../../_data/entropy/alps/corr_entropy_r.npy')
    plot_entropies(temp, corr_r)




