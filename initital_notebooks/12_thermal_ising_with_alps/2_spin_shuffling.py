import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append("../../")


from Ising.Data import IsingData
from helper.helper import nn_partial_view, nn_fully_randomized, reshape_data