import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append("../../")

from helper.helper import nn_exclude_training
from Ising.Data import IsingData
data = IsingData("../../Ising/alps_thermal_small/")


plt.rcParams['figure.figsize'] = (16, 8)


# generate needed intervals
x = 1.2+0.1*np.arange(22)
x, y = np.meshgrid(x, x)
intervals = [(x, y) for x, y in zip(x.flatten(), y.flatten())]
intervals_small = [(start, end) for end, start in intervals if end > start]
intervals_small = np.array(intervals_small)


presults, nns = nn_exclude_training(data.train_data,
                                    nn="Paper",
                                    exclude=intervals_small,
                                    keepNN=False,
                                    save_results_single="../../_data/exclude_data/alps_initial",
                                    n_epochs=4)
