import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append("../../")

from helper.ExcludeAnalyzer import ExcludeAnalyzer
analyzer = ExcludeAnalyzer("../../_data/exclude_data/alps_initial",
                           exclude_outliers=False,
                           select_slice=None)

graphs = analyzer.T_curve(display_std=True, select_slice=slice(2))

_ = analyzer.shift_images()
