import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append("../../")

from helper.Batcher import Batcher


if __name__ == "__main__":
    b = Batcher("../../_data/partial_view_alps/site_randomization/larger_set.h5", read_only=True)

    for i, row in b.iterrows():
        if row["n_sites"] in [5, 10, 25, 50, 200]:
            continue
        result = row["result"]
        plt.scatter(result[0], result[-1], label=row["n_sites"])
    plt.legend(title="# spin sites used")
    plt.show()
