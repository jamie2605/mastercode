import sys
import matplotlib.pyplot as plt
import numpy as np
sys.path.append("../../")
from Ising.Data import IsingData
import os


if __name__ == "__main__":
    data = IsingData('../../Ising/alps_thermal_small')
    data.plot_magnetization_curve()


