import sys
import matplotlib.pyplot as plt
import numpy as np
sys.path.append("../../")
from Ising.Data import IsingData
import os


def view_magnetization_trajectory(samples):
    plt.plot(np.mean(samples, axis=-1))


def subsample(directory, data, n_train, n_test):

    dirs = []
    for folder in ["train", "test"]:
        d = os.path.join(directory, folder)
        assert not os.path.isdir(d)
        os.mkdir(d)

        dirs.append(d)

    # subsample every temperature
    for t in np.unique(data[2]):
        current_data = data[0][data[2] == t, :]

        select_i = np.random.randint(len(current_data), size=n_test+n_train)

        train_data = current_data[select_i[:n_train], :]
        test_data = current_data[select_i[n_train:], :]

        for d, dat in zip(dirs, [train_data, test_data]):
            dat = np.hstack((np.zeros((len(dat), 1)), dat))
            file_path = os.path.join(d, "config_{}.dat".format(1/t))
            np.savetxt(file_path, dat, '%1.0d')


if __name__ == "__main__":
    data = IsingData.load_data('configs3', lambda x: not x % 10 == 0)

    temperatures = np.unique(data[2])

    subsample("../../Ising/alps_thermal_small/", data, 1000, 100)

    mag = []

    for T in temperatures:
        view_magnetization_trajectory(data[0][data[2] == T, :])
        mag.append(np.mean(np.abs(np.mean(data[0][data[2] == T, :], axis=1))))

    plt.plot(temperatures, mag)
    plt.show()
