import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from helper.plotting import color_map


u_temps = np.load('../../_data/entropy/temps.npy')
corr_entropy_r = np.load('../../_data/entropy/corr_entropy_r.npy')
corr_entropy_r_neg = np.load('../../_data/entropy/corr_entropy_r_neg.npy')
corr_entropy_r_nan = np.load('../../_data/entropy/corr_entropy_r_nan.npy')
corr_entropy_r_stacked = np.load('../../_data/entropy/corr_entropy_r_stacked.npy')
corr_entropy_r_shuffled = np.load('../../_data/entropy/corr_entropy_r_shuffled.npy')


def plot_entropy_3d(entropy, t):
    # calculate intermediate plot up to current temperature
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    r, t = np.meshgrid(range(np.shape(entropy)[1]), t)
    ax.plot_surface(r, t, entropy)
    ax.set_ylabel('temperature')
    ax.set_xlabel('r')
    ax.set_zlabel('correlation_entropy')
    ax.set_zlim([-1, 1])


def plot_entropy_2d(entropy, t):
    fig2d = plt.figure()
    ax = fig2d.add_subplot(111)
    r = np.shape(entropy)[1]
    c, m = color_map(np.arange(r))
    for i in range(r):
        ax.plot(t, entropy[:, i], c=c[i])
    ax.axvline(2.269)
    ax.set_xlabel('temperature')
    ax.set_ylabel('correlation entropy')

    plt.colorbar(m, label='distance between sites')


if __name__ == "__main__":
    plot_entropy_3d(corr_entropy_r_nan, u_temps)
    plot_entropy_2d(corr_entropy_r_nan, u_temps)
    plt.xlim([2.1, 3.5])
    plt.title("Correlation Entropy")

    plot_entropy_3d(corr_entropy_r_shuffled, u_temps)
    plot_entropy_2d(corr_entropy_r_shuffled, u_temps)
    plt.xlim([2.1, 3.5])
    plt.title("Correlation Entropy for shuffled spins")

    # plot_entropy_2d(corr_entropy_r_neg, u_temps)
    # plt.title("Correlation Entropy (inverted configs)")

    # plot_entropy_3d(corr_entropy_r_stacked, u_temps)
    # plot_entropy_2d(corr_entropy_r_stacked, u_temps)

    plt.show()
