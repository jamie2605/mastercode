import matplotlib.pyplot as plt
import time
from helper.Batcher import Batcher
import numpy as np
from matplotlib import animation
from matplotlib.widgets import Slider


class TransverseIsingAnimator:
    def __init__(self, file, particle=15):
        self.file, self.data = None, None
        self.fig, self.ax, self.lines, self.slider = None, None, None, None
        self.animation = None

        self.prepare_data(file, particle)
        self.prepare_plot()

    def prepare_data(self, file, particle):
        self.file = file
        experiment = Batcher(self.file, read_only=True)

        data = experiment.get_completed()

        data = data.loc[data['particle'] == particle]

        data.reset_index()
        data.set_index('h', inplace=True)
        data = data.loc[data['repetition'] == 0]

        self.data = data

    def prepare_plot(self):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        self.ax.set_position([0.15, 0.2, 0.75, 0.7])

        self.ax.set_ylabel("expected $\sigma_z$ ")
        self.ax.set_xlabel("time")

        data_dim = np.shape(self.data["sz_expect"][0])

        self.lines = []
        for p in range(data_dim[0]):
            self.lines.append(self.ax.plot(np.random.uniform(-1, 1, data_dim[1]))[0])

        slider_ax = plt.axes([0.1, 0.02, 0.8, 0.02], facecolor='lightgoldenrodyellow')
        self.slider = Slider(slider_ax, "", 0, 49, 0)
        self.slider.valtext.set_visible(False)

        self.slider.on_changed(self.update)

        self.animation = animation.FuncAnimation(self.fig, self.animate,
                                                init_func=self.init_animation,
                                                frames=49, interval=100, blit=False)

        Writer = animation.writers['html']
        writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
        self.animation.save('animation.html', writer=writer)
        self.update(0)

        plt.show()

    def start(self):
        self.animation.event_source.start()

    def stop(self):
        self.animation.event_source.stop()

    def init_animation(self):
        return self.lines

    def animate(self, h_i):
        self.ax.set_title("h={}".format(round(self.data.index[h_i], 2)))
        for i, l in enumerate(self.lines):
            l.set_ydata(self.data.sz_expect.iloc[h_i][i])
        return self.lines

    def update(self, val):
        self.stop()
        self.animate(int(val))
        self.fig.canvas.draw_idle()


if __name__ == "__main__":
    a = TransverseIsingAnimator("/home/ben/masterphase/Mastercode/_data/spin_chain_initial/large_grid.h5")
    a.start()
