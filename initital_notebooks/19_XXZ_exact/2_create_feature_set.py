import sys

sys.path.append("../../")
from helper.Batcher import Batcher
from helper.FeatureSetFactories import XXZSpinConfigurationFactory
import numpy as np

if __name__ == '__main__':
    # Model definitions

    params = []
    #params.append((1, None, True))
    #params.append((1, None, False))
    params.append((1, -3, False))
    #params.append((3, None, False))
    #params.append((3, -3, False))

    for dimensionality, rscaling, ordered in params:
        N = 10
        common_parameters = {'model': 'XXZ',
                             'dimensionality': dimensionality,
                             'n': N,
                             'J': 1,
                             # 'cloud_sigma': 1,  # only applicable to unordered system
                             'r_scaling': rscaling,  # None is NN neighbor
                             'ordered': ordered}

        print("Started with {}".format(common_parameters))

        rs = [np.random.multivariate_normal(np.zeros(dimensionality), N * np.eye(dimensionality), size=N)
              for _ in range(10)]

        sweep = Batcher.grid_search(h=np.arange(0, 10, 0.2), delta=np.arange(-2, 2, 0.2), r=range(len(rs)))
        sweep['r'] = rs * int(len(sweep) / len(rs))
        n_points = 1000

        template = "../../features/xxz_exact/ordered-{}_r_scaling-{}_{}spins_{}samples_extended_h_rs.h5"
        filename = template.format(common_parameters['ordered'],
                                   common_parameters['r_scaling'],
                                   common_parameters['n'],
                                   n_points)

        meta = {"common_parameters": common_parameters}

        # Create feature set, skip rows that already exist
        xxz_factory = XXZSpinConfigurationFactory(common_parameters=common_parameters,
                                                  sweep_parameters=sweep,
                                                  #cache=filename,
                                                  meta=meta)
        feature_set = xxz_factory.create_feature_set(n_points=n_points)
        feature_set.save(filename)

    pass
