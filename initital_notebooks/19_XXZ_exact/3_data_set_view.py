from SpinData.Data_v3 import FeatureSet


from helper.plotting import TSNEView

if __name__ == '__main__':
    data = FeatureSet('../../features/xxz_exact/ordered-True_r_scaling-None_10spins_1000samples.h5')

    r = [data[::100, data.features.keys()[i::3]] for i in range(3)]

    t = [TSNEView(xi) for xi in r]
    pass