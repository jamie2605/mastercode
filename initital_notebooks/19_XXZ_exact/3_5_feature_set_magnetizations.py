import pickle

from SpinData.Data_v3 import FeatureSet
from helper.plotting import XXZSpinConfigurationGraphs, XXZSpinConfigurationImages, MultiSpinConfigurationImages
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':

    # _data = FeatureSet('../../features/xxz_exact/ordered-False_r_scaling--3_10spins_1000samples_extended_h_rs.h5')
    #
    # # store spin positions separately for performance, index on first spin position
    # first_r = _data.labels['r'].apply(lambda x: x[0][0])
    # _, u_i = np.unique(first_r.values, return_index=True)
    # rs = {first: np.squeeze(full) for first, full in zip(first_r[u_i], _data.labels['r'].iloc[u_i])}
    # _data.labels['r'] = first_r
    #
    # _data.save('foo.bar.tmp')
    # pickle.dump(rs, open('foo.bar.tmp2', 'wb'))

    # use cache instead of code above for fast testing
    # _data = FeatureSet('foo.bar.tmp')
    # rs = pickle.load(open('foo.bar.tmp2', 'rb'))
    #
    # view = SpinConfigurationOrderings2DWithPositions(spin_configurations=_data, rs=rs)

    # del _data

    view = []
    for file in ['../../features/xxz_exact/ordered-False_r_scaling--3_10spins_1000samples_fullh.h5',
                 '../../features/xxz_exact/ordered-False_r_scaling--3_11spins_1000samples_fullh.h5',
                 '../../features/xxz_exact/ordered-True_r_scaling-None_11spins_1000samples_fullh.h5',
                 '../../features/xxz_exact/ordered-True_r_scaling--3_10spins_1000samples_fullh.h5'
                 ]:
        data = FeatureSet(file)

        view.append(XXZSpinConfigurationImages(spin_configurations=data))
    # graphs = XXZSpinConfigurationGraphs(spin_configurations=_data)

    plt.show()
