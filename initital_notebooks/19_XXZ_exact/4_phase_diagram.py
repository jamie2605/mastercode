from NN.KerasModels import XXZ3Layer
from NN.TensorFlowModels import ConvNN, PaperNN
from SpinData.Data_v3 import FeatureSet
from helper.Batcher import Batcher
from helper.plotting import XXZPhaseDiagram
import numpy as np
import pandas as pd

if __name__ == '__main__':
    data = FeatureSet('../../features/xxz_exact/ordered-False_r_scaling--3_11spins_1000samples_fullh.h5')

    nn = XXZ3Layer(data.n_features)

    ulabel = data.ulabel

    parameters = []
    for scan_direction in ["delta"]:
        for window_width in [0.8]:
            # scan at every point in generated _data, leave padding for training beyond points
            scan_window = ulabel[scan_direction].between(ulabel[scan_direction].min() + window_width / 2,
                                                         ulabel[scan_direction].max() - window_width / 2,
                                                         inclusive=True)
            thresholds = data.ulabel[scan_window]
            parameters.append(Batcher.grid_search_v2(scan_direction=scan_direction,
                                                     window_width=window_width,
                                                     threshold_h=thresholds["h"].unique(),
                                                     threshold_delta=thresholds["delta"].unique()))

    parameters = pd.concat(parameters)

    p = XXZPhaseDiagram(neural_network=nn,  initial_data=parameters,
                        features=data,
                        store="../../_data/19_xxz_exact/2_keras2layer.h5")

    p.run_all(save_interval=10)

    pass
