from helper.Batcher import Batcher
from helper.models_qutip import _xxz_hamiltonian
import numpy as np
import helper.my_pyplot as plt
import pandas as pd


if __name__ == '__main__':

    b = Batcher(store='../../_data/19_xxz_exact/timings.h5')

    do_simulation = False
    if do_simulation:
        b.set_data(Batcher.grid_search(repetition=np.arange(10),
                                       n=np.arange(1, 15),
                                       device=["cpu", "gpu"],
                                       interaction=None, delta=None, omega=None,
                                       p=None))

        for row in b.run(randomize=False):
            n = row.n

            # for runtime testing, set common_parameters randomly
            interaction = np.random.rand(n, n)
            delta = np.random.rand()
            omega = np.random.rand(n)

            ham = _xxz_hamiltonian(interaction, delta, omega)
            print("finding configurations")
            p = ham.ground_state_configuration_probabilities(gpu=row.device == "gpu")

            row.interaction = interaction
            row.delta = delta
            row.omega = omega
            row.p = p

    results = b.get_completed()
    results.loc[:, "duration"] = results.duration.apply(lambda x: x.total_seconds())
    results = results.set_index(["n", "device"])["duration"]

    duration = results.groupby(level=[0, 1]).mean()
    error = results.groupby(level=[0, 1]).std()

    duration.unstack(1).plot(yerr=error.unstack(1), logy=True)
    plt.ylabel('execution time/s')
    plt.show()
    pass