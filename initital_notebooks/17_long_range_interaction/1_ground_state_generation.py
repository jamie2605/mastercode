import numpy as np
import sys

sys.path.append('../../')
from helper import my_pyplot as plt
from helper.mps_parameters import get_output_ch17
from helper.helper import generate_mps_correlation_data_set, get_all_feature_vectors

if __name__ == '__main__':

    L = 700
    cutoff = 100

    for r, exp in [(2, -3), (np.inf, -3), (np.inf, -6)]:
        save_to = '../../Ising/MPS/range{}exp{}_L={}_n=201_cut={}_full.npz'.format(r, exp, L, cutoff)

        # prepare model
        print("Preparing model, simulating if necessary, reading output")
        Outputs, parameters = get_output_ch17(r, exp, L)

        glist = [p['g'] for p in parameters]
        sigmax_list = [abs(np.mean(o['sigmax'])) for o in Outputs]
        zz = [o['zz'] for o in Outputs]

        print("Creating _data set")
        feat = get_all_feature_vectors(zz, cutoff=cutoff)
