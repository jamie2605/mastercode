import numpy as np

import sys
sys.path.append("../../")
from SpinData.Data_v2 import MPSIsingDataSelectTrainI
from NN.TensorFlowModels import PaperNN
import helper.my_pyplot as plt
from helper.Batcher import Batcher
import os
import time


if __name__ == '__main__':
    # Load _data
    cutoff = 100
    L = 700
    interactions = [(1, None), (2, -3), (np.inf, -3), (np.inf, -6)]

    # prepare battery
    nn = PaperNN(cutoff, device="gpu")
    b = Batcher('../../_data/17_MPS_long_range/full_sweep.h5')
    b.set_data(b.grid_search(begin=np.arange(0, 100, 20),
                             end=np.arange(100, 200, 20),
                             noise_train=[None, 0.01, 0.1],
                             noise_test=[None, 0.01, 0.1],
                             interaction=np.arange(len(interactions)),
                             repetition=np.arange(10),
                             ulabel=None, r0=None, r1=None, crossing=None,
                             accuracy=None, std=None),
               ignore_if_already_set=True)

    d_npz = []
    for r, exp in interactions:
        file_name = '../../Ising/MPS/range{}exp{}_L={}_n=201_cut={}_full.npz'.format(r, exp, L, cutoff)
        d_npz.append(np.load(file_name))

    # run experiment
    for row in b.run(intermediate_save=100):
        # skip _data
        if d_npz[row.interaction] is None:
            row.executed = None
            continue
        nn.restart_session()

        data = MPSIsingDataSelectTrainI(d_npz[row.interaction], train_i=[row.begin, row.end])

        # make some noise
        if row.noise_train is not None:
            data.train.add_feature_noise(row.noise_train)

        if row.noise_test is not None:
            data.test.add_feature_noise(row.noise_test)

        # train network
        nn.train_d(data, epochs=500)

        # evaluate_and_show and save
        row.ulabel, row.accuracy, row.r0, row.r1, row.std = nn.evaluate_t_dependence_v2(data.test)

        crossing = row.ulabel[np.argwhere(np.diff(np.sign(row.r0 - row.r1)) != 0).reshape(-1) + 0]
        row.interaction = interactions[row.interaction]
        row.crossing = crossing
