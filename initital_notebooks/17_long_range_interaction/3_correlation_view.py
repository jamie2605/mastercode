import numpy as np

import sys
sys.path.append("../../")
from helper.mps_parameters import get_output_ch17
from helper.plotting import CorrelationZZAnimation
from helper.plotting import plot_magnetizations
from NN.TensorFlowModels import PaperNN
import helper.my_pyplot as plt


if __name__ == '__main__':

    # Load _data
    Outputs, parameters = get_output_ch17(2, -3)
    glist = [p['g'] for p in parameters]
    sigmax_list = [abs(np.mean(o['sigmax'])) for o in Outputs]
    zz = [o['zz'] for o in Outputs]

    plt.rc('text', usetex=True)
    zfig, xfig = plot_magnetizations(glist, zz, sigmax_list)
    c = CorrelationZZAnimation(zz, glist)

    pass

