import numpy as np

import sys

sys.path.append("../../")
import helper.my_pyplot as plt
from helper.Batcher import Batcher
from helper.plotting import NoiseInteractionDeviationImage, \
    NoiseInteractionResponsePlotter, NoiseInteractionPlotMeanResponse


if __name__ == '__main__':
    b = Batcher('../../_data/17_MPS_long_range/full_sweep.h5')

    interactions = [(1, None), (2, -3), (np.inf, -3), (np.inf, -6)]
    results = b.get_completed(exclude_times=True)

    # convert indices
    ulabel = results["ulabel"].iloc[0]
    for col in ["begin", "end"]:
        results[col] = ulabel[np.array(results[col], dtype=int)]

    di = NoiseInteractionDeviationImage(results, interactions)
    mr = NoiseInteractionPlotMeanResponse(results, interactions)
    rp = NoiseInteractionResponsePlotter(results, interactions)

    pass