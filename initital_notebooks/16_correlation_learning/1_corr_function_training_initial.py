import numpy as np

import sys
sys.path.append("../../")
from Ising.Data_v2 import MPSIsingData
from NN.TensorFlowModels import PaperNN
import helper.my_pyplot as plt


if __name__ == '__main__':
    data = MPSIsingData('/home/ben/masterphase/Mastercode/Ising/MPS/L=700_n=201_cut=100.npz')
    nn = PaperNN(100)
    nn.train_d(data, progress_update=1, epochs=5)

    nn.plot_test_response(data.test)
