import numpy as np

import sys
sys.path.append("../../")
from Ising.Data_v2 import MPSIsingDataSelectTrainI
from NN.TensorFlowModels import PaperNN
import helper.my_pyplot as plt
from helper.Batcher import Batcher


if __name__ == '__main__':
    nn = PaperNN(100, device="cpu")
    d = np.load('../../Ising/MPS/L=700_n=201_cut=100_full.npz')

    b = Batcher('../../_data/16_MPS_generalization/2point_sweep_500epochs.h5')
    b.set_data(b.grid_search(begin=np.arange(0, 100, 5),
                             end=np.arange(100, 200, 5),
                             repetition=np.arange(10),
                             ulabel=None, r0=None, r1=None, crossing=None,
                             accuracy=None, std=None),
               ignore_if_already_set=True)

    for row in b.run(intermediate_save=100):
        nn.restart_session()

        data = MPSIsingDataSelectTrainI(d, train_i=[row.begin, row.end])

        nn.train_d(data, progress_update=1, epochs=500)

        ulabel, acc, r0, r1, std = nn.evaluate_t_dependence_v2(data.test)
        crossing = ulabel[np.argwhere(np.diff(np.sign(r0 - r1)) != 0).reshape(-1) + 0]

        row.ulabel = ulabel
        row.r0 = r0
        row.r1 = r1
        row.accuracy = acc
        row.std = std
        row.crossing = crossing

    pass
