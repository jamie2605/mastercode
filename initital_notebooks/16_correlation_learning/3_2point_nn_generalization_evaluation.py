import numpy as np

import sys

sys.path.append("../../")
from Ising.Data_v2 import MPSIsingDataSelectTrainI
from NN.TensorFlowModels import PaperNN
import helper.my_pyplot as plt
from helper.Batcher import Batcher
import pandas as pd
from helper.plotting import DeviationImage, ResponsePlotter, PlotMeanResponse


if __name__ == '__main__':
    b = Batcher('../../_data/16_MPS_generalization/2point_sweep_500epochs.h5')

    results = b.get_completed(exclude_times=True)
    ulabel = results.ulabel.iloc[0]

    coordinates = [(ulabel[r["begin"]], ulabel[r["end"]]) for i, r in results.iterrows() if len(r.crossing) > 0]
    crossing = np.array([c[0]+0.005 for c in results.crossing.values if len(c) > 0])
    di = DeviationImage(coordinates, crossing)

    mr = PlotMeanResponse(results)

    display = ["r0", "r1", "crossing", "train0", "train1", "accuracy", "std"]
    rp = ResponsePlotter(results, display)

    mr.save_current_to_plot_stream()

    pass