import os
import numpy as np
import re
import sys
import pandas as pd

sys.path.append("../../")
import helper.my_pyplot as plt
from helper.Batcher import Batcher
from helper.plotting import NoiseInteractionDeviationImage, \
    MachineLearningNetworkComparison, NoiseInteractionPlotMeanResponse


def load_machine_learning_data(file_name='../../_data/17_MPS_long_range/full_sweep.h5'):
    b = Batcher(file_name)

    results = b.get_completed(exclude_times=True)

    results = results.loc[(results["noise_test"].isnull()) & (results["noise_train"].isnull())]

    # convert indices
    ulabel = results["ulabel"].iloc[0]
    for col in ["begin", "end"]:
        results[col] = ulabel[np.array(results[col], dtype=int)]

    return results


def load_network_data(file_path='../../kathinka/plot_data'):
    interaction_folders = {(1, None): "nn",
                           (2, -3): "nnn",
                           (np.inf, -3): "long-range-r-3"}

    results = []
    for interaction, folder in interaction_folders.items():
        folder = os.path.join(file_path, folder)
        files = os.listdir(folder)

        matches = {}
        for c in ["clustering", "density", "disparity", "pearson"]:
            pattern = re.compile("{}_([0-9]+).txt".format(c))
            matches[c] = [(m.group(0), m.group(1)) for m in [pattern.search(f) for f in files] if m is not None]

        system_sizes = [int(m[1]) for m in matches["clustering"]]

        current_interaction_data = []
        for l in system_sizes:
            current_size_data = pd.DataFrame()

            for c in matches.keys():
                file = os.path.join(folder, "{}_{}.txt".format(c, l))
                column = pd.read_table(file, header=None, dtype=float)
                glist = np.linspace(0, 2, len(column))
                column["h"] = glist
                column = column.set_index("h")

                if len(current_size_data) == 0:
                    current_size_data = pd.DataFrame(index=glist)

                current_size_data[c] = column

            current_size_data["size"] = l
            current_interaction_data.append(current_size_data)
        current_interaction_data = pd.concat(current_interaction_data)
        current_interaction_data["interaction"] = [interaction] * len(current_interaction_data)

        results.append(current_interaction_data)

    return pd.concat(results)


if __name__ == '__main__':
    interactions = [(1, None), (2, -3), (np.inf, -3), (np.inf, -6)]

    data_ml = load_machine_learning_data()
    data_network = load_network_data()

    rp = MachineLearningNetworkComparison(data=data_ml, interactions=interactions, data_network=data_network)

    pass