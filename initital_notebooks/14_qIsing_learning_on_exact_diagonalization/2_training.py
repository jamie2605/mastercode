import numpy as np
import pandas as pd

import sys
sys.path.append("../../")

from Ising.Data_v2 import QIsingData
from helper.Batcher import Batcher
import helper.my_pyplot as plt
from helper.plotting import color_map
from helper.Batcher import marginalize_to_index


def perform_simulation(b: Batcher):
    from NN.TensorFlowModels import PaperNN
    # init _data, nn
    data = QIsingData('/home/ben/masterphase/Mastercode/Ising/quantum/12x120temps')
    nn = PaperNN(12)

    for row in b.run():
        nn.reinit()
        data.train.label_threshold = row.threshold
        data.test.label_threshold = row.threshold

        nn.train_d(data, epochs=30)
        row.result = nn.evaluate_t_dependence_v2(data.test)


def get_global_accuracies(accuracies):
    return np.mean(accuracies[1])


def extract(df: pd.DataFrame, extract_col, extract_names=None, drop_original=True):

    def _extract(indexable, i):
        return indexable[i]

    assert extract_col in df.keys()

    for i, key in enumerate(extract_names):
        df[key] = df[extract_col].apply(_extract, i=i)

    if drop_original:
        df = df.drop(extract_col, axis=1)

    return df


def std_plot(df: pd.DataFrame, select_i):
    c, m = color_map(df.index[select_i].values)

    for i_c, i_r in enumerate(select_i):
        row = df.iloc[i_r]
        plt.plot(row['magnetic field strength'], row['std'], c=c[i_c])

    plt.colorbar(m, label='magnetic field threshold')
    plt.ylabel("Accuracy across test set")
    plt.xlabel('magnetic field strength')
    plt.show()


def evalute_results(results: pd.DataFrame):

    results = results.set_index('threshold')
    results = extract(results, 'result', ['magnetic field strength', 'accuracy', 'response0', 'response1', 'std'])
    results['global_accuracy'] = results['accuracy'].apply(np.mean)
    averaged, std = marginalize_to_index(results, return_std=True)

    averaged['global_accuracy'].plot(yerr=std['global_accuracy'], legend=False)
    plt.ylabel("Accuracy across test set")
    plt.show()

    std_plot(averaged, [30])

    select_i = np.where(np.logical_and(averaged.index > 0.8, averaged.index < 1.2))[0]
    std_plot(averaged, select_i)


    pass


if __name__ == "__main__":

    # init _executor
    b = Batcher('/home/ben/masterphase/Mastercode/_data/q_find_phase_transition/initial.h5')
    if b.empty:
        b.set_data(Batcher.grid_search(rep=np.arange(10), threshold=np.linspace(0.5, 1.5, 100), result=None))

    perform_simulation(b)

    evalute_results(b.get_completed(True))
