import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA

import time
import os
import sys
sys.path.append("../../")

from Ising.Data_v2 import QIsingData, ConfigurationSet


def reduced_dim_plot(embedded_data, colors, title=None):
    plt.scatter(embedded_data[:, 0], embedded_data[:, 1], c=colors, s=1)
    plt.title(title)
    plt.show()


if __name__ == "__main__":
    d = QIsingData('/home/ben/masterphase/Mastercode/Ising/quantum/12x120temps')

    d.plot_magnetization_curve()
    d.train.plot_magnetization_curve()
    plt.show()

    pca = PCA(n_components=2)
    pca.fit(d.test.configuration)
    pca_test = pca.transform(d.test.configuration)
    reduced_dim_plot(pca_test, d.test.magnetization, 'PCA test _data magnetization')
    reduced_dim_plot(pca_test, d.test.binary_label, 'PCA test _data label')

    pca = PCA(n_components=2)
    pca.fit(d.train.configuration)
    pca_train = pca.transform(d.train.configuration)
    reduced_dim_plot(pca_train, d.train.magnetization, 'PCA train _data magnetization')
    reduced_dim_plot(pca_train, d.train.binary_label, 'PCA train _data label')

    TSNE_test = TSNE(n_components=2).fit_transform(d.test.configuration)
    reduced_dim_plot(TSNE_test, d.test.magnetization, 'TSNE test _data magnetization')
    reduced_dim_plot(TSNE_test, d.test.binary_label, 'TSNE test _data label')

    TSNE_train = TSNE(n_components=2).fit_transform(d.train.configuration)
    reduced_dim_plot(TSNE_train, d.train.magnetization, 'TSNE train _data magnetization')
    reduced_dim_plot(TSNE_train, d.train.binary_label, 'TSNE train _data label')
