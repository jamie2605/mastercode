import math

import tensorflow as tf

from .BaseNN import TensorFlowNN


class ConvNN(TensorFlowNN):
    def __init(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _build_model(self):
        L = self.N ** 0.5

        # weights and layers
        w = dict()
        l = dict()

        # input spin configurations
        l["x"] = tf.placeholder(tf.float32, shape=[None, L*L])
        # correct label (<Tc or >Tc)
        y_ = tf.placeholder(tf.float32, shape=[None, 2])

        # unflatten array for y translation invariance
        l["x_2D"] = tf.reshape(l["x"], shape=[-1, L, L, 1])

        # CONVOLUTIONAL LAYER
        filters1 = 4
        w["W_conv1"] = TensorFlowNN.weight_variable([3, 3, 1, filters1])
        w["b_conv1"] = TensorFlowNN.bias_variable([filters1])
        l["h1_conv"] = tf.nn.relu(self.conv2d(l["x_2D"], w["W_conv1"]) + w["b_conv1"])
        #l["h1_pool"] = self.max_pool_2x2(l["h1_conv"])

        # flatten array again
        neurons_out = L*L*filters1
        flat = tf.reshape(l["h1_conv"], [-1, neurons_out])

        # FULLY CONNECTED LAYERS
        #w["W1"] = TensorFlowNN.weight_variable([neurons_out, 1024])
        #w["b1"] = TensorFlowNN.bias_variable([1024])
        #l["h2"] = tf.nn.relu(tf.matmul(flat, w["W1"]) + w["b1"])

        w["W2"] = TensorFlowNN.weight_variable([neurons_out, 2])
        w["b2"] = TensorFlowNN.bias_variable([2])

        l["y"] = tf.matmul(flat, w["W2"]) + w["b2"]

        # Loss function
        self._cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=l["y"]))

        # export important nodes
        self._x = l["x"]
        self._y_ = y_
        self._y = l["y"]

        # export weights
        self.weights = w
        self.layers = l

    def _train_step(self, learning_rate):
        step = tf.train.AdamOptimizer(learning_rate).minimize(self._cross_entropy)
        # initialization already performed, but Adam introduces new variables
        init_op = tf.global_variables_initializer()
        self._sess.run(init_op)
        return step

    def get_convolution(self, test):
        return self.layers["h1_conv"].eval(feed_dict={self._x: test}, session=self._sess)

    # shortcut aliases
    @staticmethod
    def conv2d(x, W):
        return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

    @staticmethod
    def max_pool_2x2(x):
        return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                              strides=[1, 2, 2, 1], padding='SAME')


class PaperNN(TensorFlowNN):
    def __init(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _build_model(self):
        n = self.N

        # input spin configurations
        x = tf.placeholder(tf.float32, shape=[None, n])
        # correct label (<Tc or >Tc)
        y_ = tf.placeholder(tf.float32, shape=[None, 2])

        # determine number of neurons in hidden layer
        hidden_neurons = math.floor(n*1.5)

        # weights to optimize
        W1 = TensorFlowNN.weight_variable([n, hidden_neurons])
        b1 = TensorFlowNN.bias_variable([hidden_neurons])

        W2 = TensorFlowNN.weight_variable([hidden_neurons, 2])
        b2 = TensorFlowNN.bias_variable([2])

        # actual model (fully connected with relu)
        h = tf.nn.relu(tf.matmul(x, W1) + b1)
        y = tf.matmul(h, W2) + b2

        # Loss function
        self._cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y_, logits=y))

        # export important nodes
        self._x = x
        self._y_ = y_
        self._y = y

    def _train_step(self, learning_rate):
        step = tf.train.AdamOptimizer(learning_rate).minimize(self._cross_entropy)
        # initialization already performed, but Adam introduces new variables
        init_op = tf.global_variables_initializer()
        self._sess.run(init_op)
        return step


class SimpleNN(TensorFlowNN):
    def __init(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _build_model(self):
        L = self.N
        # input spin configurations
        x = tf.placeholder(tf.float32, shape=[None, L])
        # correct label (<Tc or >Tc)
        y_ = tf.placeholder(tf.float32, shape=[None, 2])

        # weights to optimize
        W = tf.Variable(tf.truncated_normal([L, 2], stddev=0.1))
        b = tf.Variable(tf.constant(0.1, shape=[2]))

        # actual model
        y = tf.nn.relu(tf.matmul(x, W) + b)

        # Loss function
        self._cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))

        # export important nodes
        self._x = x
        self._y_ = y_
        self._y = y

    # def _train_step(self, learning_rate):
    #     return tf.train.GradientDescentOptimizer(learning_rate).minimize(self._cross_entropy)

    def _train_step(self, learning_rate):
        step = tf.train.AdamOptimizer(learning_rate).minimize(self._cross_entropy)
        # initialization already performed, but Adam introduces new variables
        init_op = tf.global_variables_initializer()
        self._sess.run(init_op)
        return step