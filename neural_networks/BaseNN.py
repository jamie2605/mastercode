import tensorflow as tf
import time
import numpy as np
import math
from abc import abstractmethod, ABCMeta, ABC
import keras

import keras.backend as K
import pandas as pd
from pandas import DataFrame

from features import FeatureSet


class BaseNN(ABC):
    @abstractmethod
    def train(self, train_data, train_labels, learning_rate=None, epochs=None, batch_size=None, validation_data=None,
              progress_update=None, progress_update_type=None):
        """
        General method to train a neural network
        :param train_data: 2D array of input _data (2D since flattened images)
        :param train_labels: nx2 array of boolean g (format that tensor flow likes)
        :param learning_rate:
        :param epochs: how often to sweep through the training _data
        :param batch_size: how many datapoints to use for stochastic gradient descent
        :param validation_data: optional: check convergence with validation set (valid features, valid default_labels)
        :param progress_update: how often current accuracy and epoch are displayed, can be None, "end" or number
        :param progress_update_type: whether number of progress update means seconds or epochs, can be "time" or "epoch"
        :return:
        """
        pass

    def train_f(self, train_data, column, threshold, valid_data=None, **kwargs):
        """
        Wrapper to pass training _data as FeatureSet to train function quicker
        :param train_data:
        :param valid_data:
        :param thresholds: dict describing when to consider training _data in which phase.
        :param kwargs:
        :return:
        """

        if valid_data is not None:
            valid_feat = valid_data.features.values
            valid_labels = valid_data.binary_single_2d_label(threshold=threshold, column=column)
            validation_data = (valid_feat, valid_labels)
        else:
            validation_data = None

        return self.train(train_data.features.values, train_data.binary_single_2d_label(threshold, column),
                          validation_data=validation_data, **kwargs)

    @abstractmethod
    def evaluate(self, test_configs: np.ndarray, labels: np.ndarray) -> float:
        """
        return global accuracy for a given test config
        :param test_configs: 2D array
        :param labels: 2D array, 1 hot representation
        :return: float of global accuracy
        """
        raise NotImplementedError

    def evaluate_along_label(self, test_configs: np.ndarray, true_labels: np.ndarray, axis_values) -> pd.DataFrame:
        """
        return accuracy of predicted
        :param test_configs: test _data as n x d array
        :param true_labels: true default_labels in one_hot representation
        :param axis_values: label on continuum, corresponding to test default_labels
        :return:
        """
        assert len(true_labels) == len(axis_values)
        response = self.predict(test_configs)

        y_pred = np.argmax(response, axis=1)
        y_true = np.argmax(true_labels, axis=1)

        index = pd.Index(axis_values)
        responses = {"r{}".format(i): response[:, i] for i in range(np.shape(response)[1])}
        results = {'accuracy':  np.array(y_pred == y_true, dtype=float), 'std': y_pred, **responses}
        df = DataFrame(results, index=index)
        results = df.groupby(level=0).mean()
        results['std'] = df[['std']].groupby(level=0).std()

        return results

    def evaluate_along_label_f(self, test: FeatureSet, threshold: float, column: str = None) -> pd.DataFrame:
        """
        wrapper for evaluate_and_show along label to pass test set directly
        :param test: test _data
        :param threshold: label threshold
        :param column: which label to use
        :return: results
        """
        if column is None:
            assert len(test.labels.columns) == 1
            column = test.labels.columns[0]
        true_labels = test.binary_single_2d_label(threshold, column)
        axis_values = test.labels[column]

        results = self.evaluate_along_label(test.x, true_labels, axis_values)
        results.index.rename(column, inplace=True)

        return results

    @abstractmethod
    def predict(self, test: np.ndarray) -> np.ndarray:
        """

        :param test: 2d array of test _data
        :return: confidence for each class
        """
        pass

    @abstractmethod
    def predict_classes(self, test: np.ndarray) -> np.ndarray:
        """

        :param test: 2d array of test _data n x d
        :return: most likely class as 1d array of length n
        """
        pass

    @abstractmethod
    def restart_session(self):
        pass


class KerasNN(BaseNN):
    def __init__(self, input_shape, model_parameters=None):
        self.input_shape = input_shape
        if model_parameters is None:
            model_parameters = {}

        self.session = tf.Session()
        K.set_session(self.session)
        self.graph = tf.get_default_graph()

        with self.graph.as_default():
            self.model = self.build_model(model_parameters)

    def train(self, train_data, train_labels, *args, **kwargs):
        with self.graph.as_default():
            learning_rate = kwargs.pop('learning_rate', None)
            if learning_rate is not None:
                K.set_value(self.model.optimizer.lr, learning_rate)
            self.model.fit(train_data, train_labels, *args, **kwargs)

    def evaluate(self, test_features):
        with self.graph.as_default():
            return self.model.evaluate(test_features)

    def predict(self, test_features):
        with self.graph.as_default():
            return self.model.predict(test_features)

    def predict_classes(self, test_features):
        with self.graph.as_default():
            return self.model.predict_classes(test_features)

    def restart_session(self):
        session = K.get_session()
        for layer in self.model.layers:
            if hasattr(layer, 'kernel_initializer'):
                layer.kernel.initializer.run(session=session)

    @abstractmethod
    def build_model(self, parameters)-> keras.models.Model:
        pass


class TensorFlowNN(BaseNN):
    def __init__(self, N, model_parameters=None, load_weights="", device="gpu"):
        self.N = N

        # to be created in build_model
        self._y = None
        self._y_ = None
        self._x = None

        if device == "cpu":
            import os
            os.environ['CUDA_VISIBLE_DEVICES'] = ''

        self._sess = self._build_tensor_flow_session()

    def _build_tensor_flow_session(self):
        self._sess = tf.Session()
        self._build_model()

        init_op = tf.global_variables_initializer()
        self._sess.run(init_op)

        return self._sess

    def restart_session(self, device="gpu"):
        self._sess.close()
        tf.reset_default_graph()
        self._sess = self._build_tensor_flow_session()

    def __del__(self):
        self._sess.close()

    def train(self, train_data, train_labels,
              learning_rate=1e-5, epochs=100, batch_size=120,
              validation_data=None,
              progress_update=20,
              progress_update_type="time"):

        """
        General method to train a neural network
        :param train_data: 2D array of input _data (2D since flattened images)
        :param train_labels: nx2 array of boolean g (format that tensor flow likes)
        :param learning_rate:
        :param epochs: how often to sweep through the training _data
        :param batch_size: how many datapoints to use for stochastic gradient descent
        :param validation_data: optional: check convergence with validation set (valid_features, valid_labels)
        :param progress_update: how often current accuracy and epoch are displayed, can be None, "end" or number
        :param progress_update_type: whether number of progress update means seconds or epochs, can be "time" or "epoch"
        :return:
        """

        N = np.shape(train_data)[0]
        assert (N == np.shape(train_labels)[0])

        # check that feature sizes match
        f = np.shape(train_data)[1]
        assert (f == self.N)

        sess = self._sess
        train_step = self._train_step(learning_rate)

        start_time = time.time()
        last_output = time.time()
        for epoch in range(epochs):
            # shuffle training _data before each epoch
            I = np.arange(N)
            np.random.shuffle(I)
            train_data = train_data[I]
            train_labels = train_labels[I]

            for batch_i in range(math.ceil(N / batch_size)):
                rows = range(batch_i * batch_size, (batch_i + 1) * batch_size)

                if rows[-1] >= N:
                    # skip last batch in epoch if it is not complete (avoid annoying border special cases)
                    continue

                # train minibatch
                sess.run(train_step, feed_dict={self._x: train_data[rows, ...], self._y_: train_labels[rows, ...]})

            # output progress and intermediate accuracy
            output_acc = False
            if progress_update is not None:
                if not progress_update == "end":
                    if progress_update_type == "time":
                        if time.time() - last_output > progress_update:
                            print("After {}s of training (epoch {}/{})".format(round(time.time() - start_time, 3),
                                                                               epoch + 1, epochs))
                            last_output = time.time()
                            output_acc = True
                    else:
                        if epoch % progress_update == progress_update - 1:
                            print("Training to epoch {}/{} took {}s".format(epoch + 1, epochs,
                                                                            round(time.time() - start_time, 3)))
                            output_acc = True

                if epoch == epochs - 1:
                    print("Training {} epochs took {}s".format(epochs, round(time.time() - start_time, 3)))
                    output_acc = True

            if validation_data is not None and output_acc:
                acc = self.evaluate(validation_data[0], validation_data[1])
                print("Accuracy is {}".format(round(acc, 2)))

    def evaluate(self, test_configs, labels):
        correct = tf.equal(tf.argmax(self._y, 1), tf.argmax(self._y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))
        accuracy = accuracy.eval(feed_dict={self._x: test_configs, self._y_: labels}, session=self._sess)
        return accuracy

    def predict(self, test_data: np.ndarray) -> np.ndarray:
        confidences = self._y.eval(feed_dict={self._x: test_data}, session=self._sess)
        return confidences

    def predict_classes(self, test):
        y_pred = self._y.eval(feed_dict={self._x: test[0]}, session=self._sess)
        return y_pred

    def confusion(self, test_configs, test_labels, normalize=True):
        confused = tf.contrib.metrics.confusion_matrix(tf.argmax(self._y, 1), tf.argmax(self._y_, 1))
        confused = confused.eval(feed_dict={self._x: test_configs, self._y_: test_labels}, session=self._sess)

        if normalize:
            # normalize _data-wise
            confused = confused / sum(confused)
        return confused

    # abstraction to init variables outside of flat activation zone quickly
    @staticmethod
    def weight_variable(shape, stddev=0.1):
        initial = tf.truncated_normal(shape, stddev=stddev)
        return tf.Variable(initial)

    @staticmethod
    def bias_variable(shape):
        initial = tf.constant(0.1, shape=shape)
        return tf.Variable(initial)
