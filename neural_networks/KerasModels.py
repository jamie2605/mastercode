import keras
from keras import Sequential
from keras.layers import Dense

from .BaseNN import KerasNN


class DenseModel(KerasNN):

    def build_model(self, parameters):
        num_classes = parameters.pop("num_classes", 2)
        learning_rate = parameters.pop("learning_rate", 0.001)
        num_neurons = parameters.pop("num_neurons", [100])

        model = Sequential()
        model.add(Dense(num_neurons[0], activation='relu', input_dim=self.input_shape))

        for n in num_neurons[1:]:
            model.add(Dense(n, activation='relu'))
        model.add(Dense(num_classes, activation='softmax'))

        model.compile(loss=keras.losses.categorical_crossentropy,
                      optimizer=keras.optimizers.Adam(lr=learning_rate),
                      metrics=['accuracy'])

        return model