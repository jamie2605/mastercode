from abc import ABCMeta

from core.visualization import DataFrameFilterInteractivePlotting


class NetworkMeasureFigure(DataFrameFilterInteractivePlotting, metaclass=ABCMeta):
    def __init__(self, **kwargs):
        self.network_measures = ["clustering", "density", "disparity", "pearson"]

        for i, m in enumerate(["display_{}".format(m) for m in self.network_measures]):
            self.add_state_parameter(m, values=[True, False], increase=str(i))
        super().__init__(**kwargs)

    def add_network_measures(self, results):
        state = self.state_vals
        selected_columns = results[[m for m in self.network_measures if state["display_{}".format(m)]]]
        selected_columns.plot(ax=self.ax)


class LongRangeNetworkMeasureFigure(NetworkMeasureFigure):
    def __init__(self, **kwargs):
        self.add_state_parameter_filter("interaction", "tab", "ctrl+tab")
        super().__init__(**kwargs)

    def draw(self):
        results = self.get_results(drop_filter_keys=True)

        self.ax.cla()
        if len(results) > 0:
            self.add_network_measures(results)

        self.ax.set_title(self.state_vals["interaction"])
