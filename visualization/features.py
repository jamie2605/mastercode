import os
from abc import abstractmethod, ABCMeta

import numpy as np
from typing import Union, List, Dict

import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from pandas import DataFrame
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

from features import FeatureSet
from core.visualization import InteractivePlotting, minmax, color_map, DataFrameFilterInteractivePlotting


class Magnetization1D(InteractivePlotting):
    def __init__(self, mag_data: Union[FeatureSet, List[FeatureSet]],
                 mode='normal', labels=None, title=None,
                 feature_type='spin configuration', **kwargs):
        """
        Show label-Magnetization plot for one or a list of FeatureSets
        :param mag_data: Feature
        :param mode: 'normal'       : x-axis: label value,      y-axis: mean magnetization for all points at label value
                     'all'          : x-axis: label value,      y-axis: signed magnetization for all points
                     'trajectory'   : x-axis: data point index, y-axis: magnetization for this data point
        :param labels: legend entries override
        :param title: plot title
        """
        if not isinstance(mag_data, list):
            mag_data = [mag_data]
        self.mag_data = mag_data
        self.title = title
        self.labels = labels

        assert feature_type in ['spin configuration', 'zz-correlation']
        self.feature_type = feature_type

        assert mode in ['normal', 'all', 'trajectory']
        self.mode = mode
        # 1D only
        assert all([len(d.labels.columns) == 1 for d in mag_data])

        super().__init__(**kwargs)

    def draw(self):
        full_data = self.mag_data

        self.add_magnetization_curves(full_data)

    def add_magnetization_curves(self, full_data):
        for (i, data), plot in zip(enumerate(full_data), self.art['plots']):
            magnetizations = self.calculate_magnetizations(data, self.feature_type, self.mode)
            plot.set_data(magnetizations[:, 0], magnetizations[:, 1])
            if self.labels is not None:
                plot.set_label(self.labels[i])
        if self.mode == 'normal':
            self.ax.set_ylim((0, 1))
        else:
            self.ax.set_ylim((-1, 1))
        if self.title is not None:
            self.ax.set_title(self.title)
        else:
            try:
                meta = data.meta

                title = '{} \n {}, {} spins'
                title = title.format(meta["model"], meta['method'], meta["common_parameters"]["system_size"])
                self.ax.set_title(title)
            except (KeyError, AttributeError, TypeError):
                # ignore
                pass
        self.ax.set_xlim(minmax(magnetizations[:, 0]))
        if self.mode == 'trajectory':
            self.ax.set_xlabel('data point index')
            self.ax.set_ylabel('Magnetization')
        else:
            self.ax.set_xlabel(data.labels.columns[0])
            self.ax.set_ylabel('|Magnetization|')

        if self.labels is not None:
            self.ax.legend()

    @staticmethod
    def calculate_magnetizations(data, feature_type="spin configuration", mode="normal"):
        if feature_type == 'zz-correlation':
            # take entry far off-diagonal as magnetization estimate
            data = data[:, -1:]

        if mode == 'normal':
            magnetizations = np.array([(i, g.mean(axis=1).abs().mean(axis=0))
                                       for i, g in data.unified.groupby(level=0)])
        elif mode == 'trajectory':
            magnetizations = np.vstack((data.features.index, data.features.mean(axis=1).values)).transpose()
        elif mode == 'all':
            magnetizations = np.hstack([(np.repeat(i, len(g)), g.mean(axis=1).values)
                                        for i, g in data.unified.groupby(level=0)]).transpose()

        if feature_type == 'zz-correlation':
            magnetizations[:, 1] = np.sqrt(magnetizations[:, 1])

        return magnetizations

    def prepare_plot(self):
        if self.mode == 'normal':
            size = 3
        else:
            size = 2
        iterator = np.arange(len(self.mag_data))
        c, m = color_map(iterator)
        plots = [self.ax.plot([], [], 'o', markersize=size, c=c[i])[0] for i in iterator]
        return {'plots': plots}


class Magnetization1DScaling(Magnetization1D, metaclass=ABCMeta):
    def __init__(self, mag_data: Dict[int, FeatureSet], **kwargs):
        self.system_length = mag_data.keys()

        self.add_state_parameter("rescale", values=[True, False], increase="r")
        labels = kwargs.pop("labels", self.default_labels)
        super().__init__(mag_data=list(mag_data.values()), labels=labels, **kwargs)

    def draw(self):
        if self.state_vals["rescale"]:
            return super().add_magnetization_curves(self.rescaled_data())
        else:
            return super().draw()

    @abstractmethod
    def rescaled_data(self):
        pass

    @property
    @abstractmethod
    def default_labels(self):
        pass


class Magnetization1DScalingClassicalIsing(Magnetization1DScaling):
    @property
    def default_labels(self):
        return [l ** 2 for l in self.system_length]

    def rescaled_data(self):
        new_features = []
        for l, f in zip(self.system_length, self.mag_data):
            labels = f.labels.copy()
            labels = labels.apply(lambda T: (T - 2.269)/2.269 * l)
            labels.columns = ['tL']
            new_features.append(FeatureSet(features=f.features, labels=labels, meta=f.meta))

        return new_features


class Magnetization1DScalingTFI(Magnetization1DScaling):
    @property
    def default_labels(self):
        return [l for l in self.system_length]

    def rescaled_data(self):
        new_features = []
        for n, f in zip(self.system_length, self.mag_data):
            labels = f.labels.copy()
            labels = labels.apply(lambda h_x: (h_x - 1)/1 * n)
            labels.columns = ['h_x*L']
            new_features.append(FeatureSet(features=f.features, labels=labels, meta=f.meta))

        return new_features


class TSNEView(InteractivePlotting):
    def __init__(self, data: FeatureSet, display_feature_names: bool = True, pre_pca_dim: int=None, **kwargs):
        """
        Do a TSNE plot and display results
        :param data: Feature set to use
        :param display_feature_names: whether to display featureset column names in title
        :param pre_pca_dim: None: only perfom TSNE. Otherwise dimensionality of PCA plot to begin with
        """
        self.feature_set = data
        self.display_feature_names = display_feature_names

        if pre_pca_dim is not None:
            print("Using PCA to reduce dimensionality")
            pca = PCA(n_components=pre_pca_dim)
            fit_data = pca.fit_transform(data.features.values)
        else:
            fit_data = data.features.values

        print(
            "Fitting TSNE data, data shape {}x{}".format(len(self.feature_set), np.shape(fit_data)[1]))
        self.data_embedded = TSNE(n_components=2).fit_transform(fit_data)

        self.add_state_parameter("threshold", values=np.concatenate(([0], data.ulabel)), increase="up", decrease="down")
        self.add_state_parameter("coloring", values=data.labels.keys(), increase="tab")

        super().__init__(**kwargs)

    def prepare_plot(self):
        art = {"scatter": self.ax.scatter([], [], s=2)}
        c, m = color_map([1])
        art['cb'] = self.fig.colorbar(m)
        return art

    def draw(self):
        state_vals = self.state_vals
        threshold = state_vals["threshold"]
        color_label = state_vals["coloring"]

        self.art['scatter'].set_offsets([c for c in zip(self.data_embedded[:, 0], self.data_embedded[:, 1])])

        label_values = self.feature_set.labels[color_label].values
        self.ax.set_xlim(minmax(self.data_embedded[:, 0]))
        self.ax.set_ylim(minmax(self.data_embedded[:, 1]))
        if threshold == 0:
            self.ax.set_title("")
            color, mappable = color_map(label_values)
            self.art['cb'].set_clim(vmin=min(label_values),
                                    vmax=max(label_values))
            self.art['cb'].set_label(color_label)
            self.art['cb'].draw_all()
            self.art['scatter'].set_color(color)
            self.art['cb'].ax.set_visible(True)

        else:
            self.ax.set_title("Classification with threshold {}".format(threshold))
            self.art['cb'].ax.set_visible(False)
            self.art['scatter'].set_facecolor([(0, 1, 1) if p > threshold else (0, 0, 1)
                                               for p in label_values])

        if self.display_feature_names:
            template = "Features: ${}$, {} points"
            title = template.format("$, $".join(self.feature_set.features.columns.tolist()),
                                    len(self.feature_set.features))
            self.ax.set_title(title)


class Orderings2DBase(InteractivePlotting):
    """
    Abstract base class to visualize Magnetization and neel order in 3 dimensions for a 2D phase diagram
    draws 6 images and decorates graphs
    """
    @property
    @abstractmethod
    def L(self):
        pass

    def __init__(self, **kwargs):
        self.axes_labels = kwargs.pop("axes_labels", ["h_x", "delta"])

        kwargs["subplot_args"] = {"nrows": 3, "ncols": 2, "sharex": True, "sharey": True}

        self.add_state_parameter('overlay_phase_diagram', values=[True, False], increase="o")

        super().__init__(**kwargs)

    def prepare_plot(self):
        images = np.reshape([ax.imshow(np.zeros((2, 2))) for ax in self.ax.flatten()], (3, 2))
        art = {"im": images}
        self.fig.suptitle("XXZ model magnetizations ({} spins)".format(self.L))
        self.fig.subplots_adjust(right=0.8)
        art["cb_ax"] = self.fig.add_axes([0.85, 0.15, 0.05, 0.7])
        art["cb"] = self.fig.colorbar(art["im"][0][0], cax=art["cb_ax"])

        art['overlay'] = [self.overlay_phase_diagram(ax) for ax in self.ax.flatten()]

        return art

    @staticmethod
    def overlay_phase_diagram(ax, color='k'):
        scale = 2
        overlay = []
        overlay += ax.plot(scale * np.array([0, 0.7]), [-1, -1], c=color)
        overlay += ax.plot(scale * np.array([0, 2]), [1, 1], c=color)
        overlay += ax.plot(scale * np.linspace(0.7, 2.5, 9),
                           [-1, -0.8, -0.5, -0.2, 0.2, 0.65, 1.1, 1.55, 2], c=color)
        overlay += ax.plot(scale * np.linspace(0.7, 1.25, 5), [-1, -1.1, -1.3, -1.6, -2], c=color)
        return overlay

    def scale_and_decorate(self, ymax=5):
        for xi, dim in enumerate(["x", "y", "z"]):
            for i in range(2):
                ax = self.ax[xi, i]
                if i == 0:
                    ax.set_ylabel(self.axes_labels[1])
                    ax.set_title("Magnetization in {}".format(dim))
                else:
                    ax.set_title("Neel order in {}".format(dim))

                if xi == 2:
                    ax.set_xlabel(self.axes_labels[0])

                ax.set_aspect('auto')
                ax.set_xlim((0, ymax))
                ax.set_ylim((-2, 2))

        visible_phase_diagram = self.state_vals["overlay_phase_diagram"]
        # overlay phase diagram, if requested
        for ax in self.art["overlay"]:
            for o in ax:
                o.set_visible(visible_phase_diagram)

    def draw_images(self, magnetization, neel, xi):
        for i, o in enumerate([magnetization, neel]):
            self.art["im"][xi, i].set_data(o[::-1].values)

            self.art["im"][xi, i].set_extent(
                (o.columns.min(), o.columns.max(), o.index.min(), o.index.max()))
            self.art["im"][xi, i].set_clim(minmax(o.values))


class SpinConfigurationOrderings2D(Orderings2DBase):
    @property
    def L(self):
        return len(self.configs[0].columns)

    def __init__(self, **kwargs):
        configs = kwargs.pop("spin_configurations")
        self.configs = [configs[:, xi::3].unified for xi in range(3)]

        super().__init__(**kwargs)

    def draw(self):
        state_vals = self.get_state_vals()

        for xi, dim in enumerate(["x", "y", "z"]):
            matching = self.get_matching(state_vals, xi)

            magnetization = matching.mean(axis=1).abs().groupby(matching.index.names).mean().unstack(self.axes_labels[0])

            sign_changes = pd.Series(np.sum(np.diff(np.signbit(matching.values)) / (len(matching.columns) - 1), axis=1),
                                     index=matching.index)
            neel = sign_changes.groupby(matching.index.names).mean().unstack(self.axes_labels[0])

            self.draw_images(magnetization, neel, xi)

        self.scale_and_decorate(ymax=np.max(magnetization.columns.values))

    def get_matching(self, state_vals, xi):
        matching = self.configs[xi]
        return matching


class SpinConfigurationOrderings2DWithPositions(SpinConfigurationOrderings2D):
    def __init__(self, **kwargs):
        rs = kwargs["spin_configurations"].labels['r'].unique()
        self.rs = kwargs.pop("rs")

        self.add_state_parameter('r', values=rs, increase="up", decrease="down")

        super().__init__(**kwargs)

    def get_matching(self, state_vals, xi):
        matching = super().get_matching(state_vals, xi)
        matching = matching.iloc[matching.index.get_level_values('r') == state_vals['r']]
        matching.reset_index(level=2, drop=True, inplace=True)
        return matching

    def draw(self):
        super().draw()

        rs = self.rs[self.state_vals['r']]
        self.art["positions"].set_data(rs, [0] * len(rs))
        self.art["positions_ax"].set_xlim(minmax(rs))
        pass

    def prepare_plot(self):
        art = super().prepare_plot()
        art["positions_ax"] = self.fig.add_axes([0.33, 0.95, 0.33, 0.02])
        art["positions_ax"].get_yaxis().set_visible(False)
        art["positions_ax"].set_frame_on(False)
        art["positions"] = art["positions_ax"].plot([], [], 'o')[0]

        self.fig.suptitle('')

        return art


class CorrelationOrderings2D(Orderings2DBase):
    def __init__(self, **kwargs):
        """
        Show magnetizations and neel order based off correlation functions
        """
        self.correlations = kwargs.pop("correlations")

        super().__init__(**kwargs)

    @property
    def L(self):
        return len(self.correlations["xx"].iloc[0])

    def draw(self):

        L = self.L

        padding_top = 2
        padding_right = padding_top
        mag_diags = 4

        neel = np.zeros((L, L))
        neel[padding_top::2, :-padding_right:2] = 1
        neel[padding_top::2, 1:-padding_right:2] = -1
        neel[padding_top + 1::2, :-padding_right:2] = -1
        neel[padding_top + 1::2, 1:-padding_right:2] = 1
        weight_neel = np.triu(neel)
        entries_neel = np.sum(weight_neel != 0)

        magnetization = np.zeros((L, L))
        magnetization[padding_top:, :-padding_right] = 1
        weight_magnetization = np.triu(magnetization, L - max(padding_top, padding_right) - mag_diags)
        entries_mag = np.sum(weight_neel != 0)

        for xi, corr in enumerate(self.correlations.columns):
            correlation = np.stack(self.correlations[corr].values)
            neel = np.sum(weight_neel * correlation, axis=(1, 2)) / entries_neel
            # mag = np.sum(np.sqrt(weight_magnetization * correlation), axis=(1, 2)) / entries_mag
            mag = correlation[:, padding_top, L - padding_right]

            neel = pd.Series(neel, index=self.correlations.index).unstack(self.axes_labels[0])
            mag = pd.Series(mag, index=self.correlations.index).unstack(self.axes_labels[0])

            self.draw_images(mag, neel, xi)

        self.scale_and_decorate()

    @staticmethod
    def from_outputs(outputs, return_df=False, **kwargs):
        # build correct _data format from
        k_list = ['xx', 'yy', 'zz']
        assert all([all([k in o.keys() for k in k_list]) for o in outputs])

        df = observables_to_df(outputs, k_list)

        viewer = CorrelationOrderings2D(correlations=df, **kwargs)

        if return_df:
            return viewer, df
        else:
            return viewer


class CorrelationMatrices2D(DataFrameFilterInteractivePlotting):
    """
    Expects a dataframe with columns sigmax, sigmay, sigmaz, xx, yy, zz
    Visualize MPS correlation functions
    """

    def __init__(self, **kwargs):
        self.title_tex = kwargs.pop("title_tex", False)

        self.axes_labels = kwargs.pop("axes_labels", ["h_x", "delta"])

        kwargs["subplot_args"] = {"nrows": 3, "ncols": 2}

        self.add_state_parameter_filter('h_x', 'ctrl+up', 'ctrl+down')
        self.add_state_parameter_filter('delta', 'up', 'down')

        self.add_state_parameter('y_auto_scaling', values=[True, False], increase="tab")

        super().__init__(**kwargs)

        self.decorate()

    def prepare_plot(self):
        images = [ax[1].imshow([[0, 1], [0, 1]]) for ax in self.ax]
        plots = [ax[0].plot([], [], 'o')[0] for ax in self.ax]
        art = {"im": images, "plots": plots}

        self.fig.subplots_adjust(right=0.8)
        art["cb_ax"] = self.fig.add_axes([0.85, 0.15, 0.05, 0.7])
        art["cb"] = self.fig.colorbar(images[0], cax=art["cb_ax"])

        return art

    def decorate(self):
        L = self.L

        self.art["cb"].set_clim(0, 1)

        for xi, corr in enumerate(["\sigma^x", "\sigma^y", "\sigma^z"]):
            ax = self.ax[xi, 1]
            ax.set_aspect('auto')
            ax.set_xlim((0, L))
            ax.set_ylim((0, L))

            ax.set_xticks([])
            ax.set_yticks([])

            ax.set_title(r'$\langle{}_i{}_j\rangle $'.format(corr, corr))

            im = self.art["im"][xi]
            im.set_data(np.zeros((L, L)))
            im.set_extent((0, L, 0, L))
            im.set_clim(0, 1)

        for xi, mag in enumerate(["\sigma^x", "\sigma^y", "\sigma^z"]):
            ax = self.ax[xi, 0]
            ax.set_xticks([])
            ax.set_title(r'$\langle{}_i\rangle $'.format(mag))

            ax.yaxis.set_major_formatter(FormatStrFormatter('%.4g'))

    @property
    def L(self):
        return len(self.data_frame.iloc[0]["sigmax"])

    def draw(self):
        state_vals = self.get_state_vals()
        results = self.get_results(drop_filter_keys=False).iloc[0]

        for xi, corr in enumerate(["xx", "yy", "zz"]):
            self.art["im"][xi].set_data(results[corr])

        for xi, mag in enumerate(["sigmax", "sigmay", "sigmaz"]):
            self.art["plots"][xi].set_data((range(len(results[mag])), results[mag]))

            # rescale plot
            ax = self.ax[xi, 0]
            ax.relim()
            ax.autoscale()

            if not state_vals["y_auto_scaling"]:
                ax.set_ylim((-1, 1))

        if self.title_tex:
            template = "$n$={} $h_x={:.2g}$ $\Delta={:.2g}$"
        else:
            template = "n={} h_x={:.2g} delta={:.2g}"
        self.fig.suptitle(template.format(len(results["sigmax"]), state_vals[self.axes_labels[0]],
                                          state_vals[self.axes_labels[1]]))

    @staticmethod
    def from_outputs(outputs, return_df=False, **kwargs):
        # build correct _data format from
        k_list = ['xx', 'yy', 'zz', 'sigmax', 'sigmay', 'sigmaz']
        #        assert all([all([o.haskey(k) for k in k_list]) for o in outputs])
        df = observables_to_df(outputs, k_list)
        viewer = CorrelationMatrices2D(data=df, **kwargs)

        if return_df:
            return viewer, df
        else:
            return viewer


class RuntimeViewer2D(InteractivePlotting):
    """
    plot runtime of MPS simulation against label sweeps
    """

    def draw(self):
        im = self.art["im"]
        times = self.runtimes.unstack('h_x')[::-1]
        im.set_data(times.values)
        im.set_extent((times.columns.min(), times.columns.max(), times.index.min(), times.index.max()))
        im.set_clim(minmax(times.values))

        ax = self.ax
        ax.set_xlim((0, 5))
        ax.set_ylim((-2, 2))

        ax.set_ylabel("$\Delta$")
        ax.set_xlabel("$h_x$")

    def prepare_plot(self):
        art = {"im": plt.imshow(np.zeros((2, 2)))}
        art["cb"] = plt.colorbar(art["im"], label="runtime/s")

        return art

    def __init__(self, **kwargs):
        self.runtimes = kwargs.pop("runtime_data")
        super().__init__(**kwargs)

    @staticmethod
    def from_outputs(outputs, **kwargs):
        df = observables_to_df(outputs, [])
        times = np.load(os.path.join(outputs[0]["Write_Directory"], "simulation_times.npy"))
        df["runtime"] = times

        return RuntimeViewer2D(runtime_data=df["runtime"], **kwargs)


def observables_to_df(outputs, c_list, index_list=None):
    if index_list is None:
        index_list = ['h_x', 'delta']
    data = {k: [o[k] for o in outputs] for k in c_list}
    index_t = [tuple([o[i] for i in index_list]) for o in outputs]
    index = pd.MultiIndex.from_tuples(index_t, names=['h_x', 'delta'])
    df = DataFrame(data, index)
    return df
