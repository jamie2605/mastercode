from unittest import TestCase

from core.analysis import Batcher


class TestBatcher(TestCase):
    def setUp(self):
        self.b = Batcher()

    def tearDown(self):
        del self.b

    def test_empty(self):
        self.assertTrue(self.b.empty)
        self.b.set_data(Batcher.grid_search(A=[10, 20]))
        self.assertFalse(self.b.empty)

    def test_completed(self):
        self.b.set_data(Batcher.grid_search(A=[10, 20]))
        self.assertTrue(self.b.get_completed().empty)
        for row, params in self.b.run(output_progress=False):
            pass
        self.assertFalse(self.b.get_completed().empty)

        completed = self.b.get_completed(False)
        self.assertTrue(all([k in completed.columns for k in ["executed", "duration"]]))
        completed = self.b.get_completed(True)
        self.assertFalse(any([k in completed.columns for k in ["executed", "duration"]]))

    def test_get_completed(self):
        self.assertTrue(self.b.get_completed().empty)
        self.b.set_data(Batcher.grid_search(A=[10]))
        self.assertTrue(self.b.get_completed().empty)
        _ = [b for b in self.b.run(output_progress=False)]
        self.assertFalse(self.b.get_completed().empty)

        completed = self.b.get_completed(False)
        self.assertTrue(all([k in completed.columns for k in ["executed", "duration"]]))
        completed = self.b.get_completed(True)
        self.assertFalse(any([k in completed.columns for k in ["executed", "duration"]]))

    def test_run(self):
        self.b.set_data(Batcher.grid_search(A=[10, 20], B=[50, 100]))
        iterations = 0
        for param, row in self.b.run(output_progress=True):
            self.assertTrue(set(param.keys()) == set(self.b.index.names))
            self.assertTrue(set(row.keys()) == set(self.b.columns))
            iterations += 1
        self.assertTrue(iterations == 4)
        self.assertTrue(len(self.b) == 4)

    def test_grid_search(self):
        self.b.set_data(Batcher.grid_search(A=[10, 20], B=[50, 100]))
        self.assertTrue(len(self.b) == 4)

        self.b.set_data(Batcher.grid_search(A=[10, 20], B=[50, 100], C=[20, 50]), force=True)
        self.assertTrue(len(self.b) == 8)

    def test_append_data(self):
        parameters = Batcher.grid_search(a=[10, 20], b=[4, 5])

        self.b.append_data(parameters, True)
        self.assertTrue(self.b.index.nlevels == 2)

        parameters = Batcher.grid_search(c=[10, 20], b=[4, 5])

        self.b.append_data(parameters, True)

        self.assertTrue(self.b.index.nlevels == 3)
        self.assertTrue(len(self.b) == 8)
