"""
Implement a simple event handler

"""
import numpy as np
import time
import matplotlib.pyplot as plt


class Event:
    def __init__(self):
        self._callbacks = []

    def register(self, callback):
        self._callbacks.append(callback)

    def fire(self, *args):
        for callback in self._callbacks:
            # make sure the callback is a function
            assert callable(callback)
            callback(*args)


class MPSResultHandler:
    def __init__(self, draw_interval=2):
        super().__init__()

        self.result_obj = []
        self.result_list = []
        self.last_fire = 0
        self.draw_interval = draw_interval

        self._new_result_event = Event()
        self._completed_event = Event()

    def add_new_result(self, result_tuple):
        self.result_list.append(result_tuple)

        # limit plot update to once every few seconds
        if time.time() - self.last_fire > self.draw_interval or self.completed:
            self._new_result_event.fire()
            self.last_fire = time.time()

        if self.completed:
            self._completed_event.fire()

    def on_new_result(self, callback):
        self._new_result_event.register(callback)

    def on_completed(self, callback):
        self._completed_event.register(callback)

    @property
    def completed(self):
        return len(self.result_obj) == len(self.result_list)

    def get_ordered_results(self):
        order = np.argsort([r[0] for r in self.result_list])
        return [self.result_list[i] for i in order]

    def wait_til_completed(self, check_interval=0.5):
        while not self.completed:
            time.sleep(check_interval)


# wrapper that is able to be used with RealTimePlots in later evaluation, can be initialized from result_list
class MPSResultWrapper(MPSResultHandler):
    def __init__(self, result_list):
        super().__init__()
        self.result_list = result_list

    def add_new_result(self, result_tuple):
        raise NotImplemented

    def on_completed(self, callback):
        callback()

    @property
    def completed(self):
        return True