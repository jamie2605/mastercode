import numpy as np
import scipy.ndimage as ndimage
import math


class Classical2DIsingSimulator:
    def __init__(self, system_size=11, temperature=0.1, interaction=1):
        self.L, self.T, self.J = system_size, temperature, interaction
        self.lattice = np.random.choice(np.array([1, -1], dtype=np.int8), size=(system_size, system_size))

        self.energy = np.sum(self.site_energies(self.lattice))
        self.energies = []

    def site_energies(self, lattice):
        return ndimage.generic_filter(lattice, lambda v: - 0.5 * self.J * sum(v[[1, 3, 5, 8]] * v[4]),
                                      size=3, mode='wrap')

    def step(self, n=100000):
        """
        do a spin flip, calculate change in energy for acceptance probability
        Note: n is number of accepted steps!
        """

        L = self.L
        lattice = self.lattice.copy()

        for i, j in zip(np.random.randint(L, size=n), np.random.randint(L, size=n)):

            # calculate neighbor energy with PBC
            neighbor_sum = lattice[(i - 1) % L, j] + lattice[(i + 1) % L, j] + lattice[i, (j - 1) % L] + \
                           lattice[i, (j + 1) % L]

            # energy change by spin flip
            dE = 2 * self.J * neighbor_sum * lattice[i, j]

            if dE < 0 or np.random.random() < np.exp(-dE / self.T):
                lattice[i, j] *= -1

                self.energy += dE
                self.energies.append(self.energy)

        self.lattice = lattice

    def sample(self, n=math.inf, skip_steps=100):
        """
              To reduce auto-correlations, this generator function only returns every steps-th sampling step
        """
        i = 0
        while i < n:
            # force_simulate time by skipping skip iterations
            self.step(skip_steps + 1)
            yield self.lattice
            i = i + 1
