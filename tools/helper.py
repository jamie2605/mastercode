"""
quick helper functions for use in python notebooks
"""
from datetime import datetime
from typing import List, Tuple, Dict, Any

import numpy as np
import pandas as pd
from pandas import DataFrame

from scipy.spatial.distance import cdist
import numpy as np
from scipy.stats import pearsonr

from features import FeatureSet
from collections import Counter


def build_matrix(coordinates: List[Tuple[float, float]], values: np.ndarray):
    """
    Given 2D coordinates and values at these coordinates, calculate the mean value at every point
    and build a 2D matrix with the mean value at this location (2d pseudo-histogram for use with imshow)
    :param coordinates:
    :param values:
    :return:
    """
    xx, yy = ([c[0] for c in coordinates], [c[1] for c in coordinates])
    x_bins = np.unique(xx)
    y_bins = np.unique(yy)

    image = np.empty((len(y_bins), len(x_bins)))
    image[...] = np.nan
    image_counter = np.zeros((len(y_bins), len(x_bins)))

    x_bin, y_bin = np.digitize(xx, x_bins, True), np.digitize(yy, y_bins, True)

    # fill bins
    for x, y, val in zip(x_bin, y_bin, values):
        if np.isnan(image[y, x]):
            image[y, x] = val
        else:
            image[y, x] += val
        image_counter[y, x] += 1

    # normalize
    for im, counter in zip(np.nditer(image, op_flags=['readwrite']),
                           np.nditer(image_counter, op_flags=['readwrite'])):
        if im[...] == 0:
            continue
        im[...] = im[...] / counter[...]

    image[np.argwhere(image == 0)] = np.nan

    # extent (valid for linear spacing only
    xmin = x_bins[0] - (x_bins[1] - x_bins[0])
    xmax = x_bins[-1] + (x_bins[-1] - x_bins[-2])
    ymin = y_bins[0] - (y_bins[1] - y_bins[0])
    ymax = y_bins[-1] + (y_bins[-1] - y_bins[-2])

    return image, (x_bins, y_bins), (xmin, xmax, ymax, ymin)


def array2d_to_r(array_in):
    """
    Reparameterize an l^2xl^2 array to the distance between sites r = i-j
    The radius is defined on a grid of l x l, pairs of points at given radius are found
    The flattened point coordinates of each pair are evaluated on array_in, the mean over these pairs is returned
    :param array_in: pairwise value to be evaluated between sites i,j
    :return: mean(array_values at distance r) as function of r
    """
    s = np.shape(array_in)
    assert len(np.unique(s)) == 1
    assert np.sqrt(s[0]) % 1 == 0

    l = np.sqrt(np.shape(array_in)[0])
    xx, yy = np.meshgrid(np.arange(l), np.arange(l))

    coord = np.array([[x, y] for x, y in zip(xx.flatten(), yy.flatten())])
    dists = np.round(cdist(coord, coord))

    return [np.mean(array_in[np.where(d == dists)]) for d in np.unique(dists)]


def bitwise_boolean(n: int, length: int = 10):
    """
    Generator that returns true or false based on bit in number
    :param length: number of bits to return in generator
    :param n: The number to be processed bitwise
    :return:
    """
    b = n.bit_length()
    # check that state number n is compatible with hilbert space dimension
    assert length >= b
    while n:
        yield n & 1 == 1
        n >>= 1

    # get leading zeros
    for _ in range(length - b):
        yield False


def find_crossing(r0: np.ndarray, r1: np.ndarray, label_values: np.ndarray) -> float:
    crossing = label_values[np.argwhere(np.diff(np.sign(r0 - r1)) != 0).reshape(-1) + 0]
    if len(crossing) == 0:
        return np.nan
    else:
        return crossing[0]


def calculate_network_measures(network_adj: np.ndarray) -> Dict[str, float]:
    system_size = len(network_adj)

    density = 1/(system_size*(system_size-1)) * np.sum(network_adj)

    not_diagonal2 = network_adj @ network_adj
    np.fill_diagonal(not_diagonal2, 0)
    clustering_num = np.trace(np.linalg.matrix_power(network_adj, 3))
    clustering_denom = np.sum(not_diagonal2)
    clustering = np.nan if (clustering_num == 0 and clustering_denom == 0) else clustering_num/clustering_denom

    adj2 = network_adj ** 2
    disparity_i = lambda i: np.sum(adj2[i, :]) / np.sum(network_adj[i, :]) ** 2
    disparity = np.mean([disparity_i(i) for i in range(system_size)])

    pearson = pearsonr(network_adj[int(system_size / 2)], network_adj[int(system_size / 2 + 1)])[0]

    return {"clustering": clustering,
            "density": density,
            "disparity": disparity,
            "pearson": pearson}


def calculate_network_measures_f(features: FeatureSet):
    """
    Wrapper to calculate network measures from feature set and return new feature set with network measures as features
    :param features: features set with adjecency matrix as features
    :return: feature set with network measures as features
    """
    n = int(np.sqrt(features.n_features))
    network_adj = np.reshape(features.features.values, (-1, n, n))

    measures = []
    for i in range(len(network_adj)):
        measures.append(calculate_network_measures(network_adj[i, ...]))

    measures = pd.DataFrame(measures)

    return FeatureSet(features=measures, labels=features.labels, meta=features.meta)


def dict_key_to_axis(frames: Dict[Any, DataFrame], axis_name, inplace=True):
    """

    :param inplace: If True, the original frames will be modified
    :param frames: Dict of DataFrames, with arbitrary keys
    :param axis_name: axis to add keys to
    :return: unified DataFrame
    """
    if not inplace:
        frames = frames.copy()
    for key, frame in frames.items():
        frame[axis_name] = [key] * len(frame)
        frame.set_index(axis_name, append=True, inplace=True)

    return pd.concat([f for f in frames.values()])


def combine_from_stores(stores: List[str]):
    """
    Load pandas dataframes from hdf storage given by stores and concat horizontally
    :param stores:
    :return:
    """
    data_frames = [pd.read_hdf(s, key='results').sort_index() for s in stores]

    # only use completed data, remove columns executed and duration
    for i in range(len(data_frames)):
        if "executed" in data_frames[i].columns:
            data_frames[i] = data_frames[i].loc[data_frames[i]["executed"].notnull()]
            data_frames[i] = data_frames[i][[c for c in data_frames[i].columns if c not in ["executed", "duration"]]]

    if all([len(df.index) == len(df.index.unique()) for df in data_frames]):
        result = pd.concat(data_frames, axis=1)
    else:
        # make index unique by adding temporary repetition index
        for i in range(len(data_frames)):
            df = data_frames[i]
            occurrences = Counter(df.index.tolist())
            temp_index = np.concatenate([np.arange(counts) for counts in occurrences.values()])
            data_frames[i]["_temp_index"] = temp_index
            data_frames[i].set_index("_temp_index", inplace=True, append=True)

        result = pd.concat(data_frames, axis=1)
        result.index = result.index.droplevel("_temp_index")

    return result
