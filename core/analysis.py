import collections
from abc import abstractmethod, ABC, ABCMeta
import inspect

import pandas as pd
import numpy as np
import os
from datetime import datetime
import warnings
import time
from typing import List, Union


class Batcher:
    def __init__(self, **kwargs):
        store = kwargs.pop("store", None)
        read_only = kwargs.pop("read_only", False)
        initial_data = kwargs.pop("initial_data", None)

        if initial_data is not None:
            assert isinstance(initial_data, pd.DataFrame)

        self._store = store
        self._data_frame_int = pd.DataFrame(columns=["executed", "duration"])
        self.read_only = read_only
        self.last_save_time = 0
        self.unsaved = 0

        if store is not None and os.path.isfile(self._store):
            self.load_store()

        else:
            if read_only:
                raise Exception('{} not found'.format(self._store))
            print('Writing to new store')

        if initial_data is not None:
            if not self._data_frame_int.empty:
                raise Exception("Cannot set initial _data as dataframe was already loaded with _data. "
                                "Don't supply initial _data or write to different file instead")
            else:
                self.set_data(initial_data)

        super().__init__(**kwargs)

    @property
    def empty(self):
        return self._data_frame_int.empty

    @property
    def parameters_set(self):
        return not self._data_frame_int.index.empty

    @property
    def loc(self):
        return self._data_frame_int.loc

    @property
    def iloc(self):
        return self._data_frame_int.iloc

    def iterrows(self):
        return self._data_frame_int.iterrows()

    def __setitem__(self, key, value):
        return self._data_frame_int.__setitem__(key, value)

    def __getitem__(self, item):
        return self._data_frame_int.__getitem__(item)

    @property
    def columns(self):
        return [c for c in self._data_frame_int.columns if c not in ["executed", "duration"]]

    @property
    def index(self):
        return self._data_frame_int.index

    @property
    def completed(self):
        return self._data_frame_int["executed"].isin([None]).sum() == 0

    def __len__(self):
        return self._data_frame_int.__len__()

    def load_store(self):
        with pd.HDFStore(self._store, mode='r') as store:
            self._data_frame_int = store['results']

            if "/meta" not in store.keys():
                print("Warning: old batcher format detected, "
                      "please convert sweeps to indices manually and save_to_store, cancelling for now")
                raise ValueError

        print('loaded store from file')

    def set_data(self, parameter_df: pd.DataFrame, result_columns: List[str] = None,
                 force=False, ignore_if_already_set=False):
        if not force and not self._data_frame_int.empty:
            if ignore_if_already_set:
                print("set_data request ignored, because common_parameters were already set")
                return
            raise Exception("Data frame is not empty, use force=True to override and still set data frame")

        if result_columns is None:
            result_columns = []

        if any([c in ['executed', 'duration'] for c in result_columns + list(parameter_df.columns)]):
            raise ValueError("Columns executed and duration are illegal column names as initial_data")

        self._data_frame_int = parameter_df
        self._data_frame_int['executed'] = None
        self._data_frame_int['duration'] = None

        for c in result_columns:
            self._data_frame_int[c] = None

    def append_data(self, df: pd.DataFrame, add_indices: bool = False):
        if add_indices:
            for key_df, no_key_df in zip([df, self._data_frame_int], [self._data_frame_int, df]):
                for key in key_df.index.names:
                    if key not in no_key_df.index.names and key is not None:
                        no_key_df[key] = np.nan

                        if no_key_df.index.nlevels == 1 and no_key_df.index.names[0] is None:
                            no_key_df.set_index(key, append=False, inplace=True)
                        else:
                            no_key_df.set_index(key, append=True, inplace=True)
            if self._data_frame_int.index.nlevels > 1:
                df = df.reorder_levels(self._data_frame_int.index.names)

        # make sure that levels match exactly, including order
        assert df.index.names == self._data_frame_int.index.names
        # add more rows to internal DataFrame
        self._data_frame_int = self._data_frame_int.append(df)

        return df

    def get_completed(self, exclude_times: bool = False) -> pd.DataFrame:
        """

        :return: data frame with all completed rows

        """
        if len(self._data_frame_int) == 0:
            return self._data_frame_int

        df = self._data_frame_int[self._data_frame_int["duration"].notnull()]
        if exclude_times:
            k = [k for k in df.keys() if k not in ['duration', 'executed']]
            df = df[k]
        return df

    def get_all_parameters(self):
        return pd.DataFrame(index=self._data_frame_int.index)

    def save_to_store(self, store=None):
        if self.read_only:
            raise Exception('Cannot save Batcher since opened in read-only mode')

        if store is not None:
            self._store = store

        if self._store is None:
            raise Exception('Could not save to hdf since no store path given')

        with warnings.catch_warnings():
            # ignore performance warning since _data must be pickled
            warnings.simplefilter("ignore")
            with pd.HDFStore(self._store, mode='w') as store:
                store['results'] = self._data_frame_int
                store['meta'] = pd.Series({"version": 2})

        self.unsaved = 0
        self.last_save_time = time.time()

    def run(self, rerun=False, output_progress=True, intermediate_save=None, save_interval=None, randomize=True):
        # by default, save after every iteration
        if intermediate_save is None and save_interval is None:
            intermediate_save = 1

        if self._data_frame_int.index.empty:
            raise Exception("No parameters found, was initial data set or loaded from store?")

        if rerun:
            to_execute = self._data_frame_int.index
        else:
            to_execute = np.where(self._data_frame_int["executed"].isnull())[0]

        # add randomization for easier intermediate viewing of results and more accurate eta
        if randomize:
            order = np.arange(len(to_execute))
            np.random.shuffle(order)
            to_execute = pd.DataFrame(index=to_execute[order]).index

        skipped = yield from self._run_loop(intermediate_save, output_progress, save_interval, to_execute)

        # definitely save at the end of the loop if any execution was successfull
        if self.unsaved > 0 and self._store is not None:
            self.save_to_store()

        if skipped > 0:
            print("{} rows were skipped, rerun to retry executing them".format(skipped))

    def _run_loop(self, intermediate_save, output_progress, save_interval, to_execute):
        batch_start = datetime.now()
        completed = 0
        skipped = 0

        # alias
        df = self._data_frame_int
        to_execute = list(to_execute)
        total_jobs = len(to_execute)
        while len(to_execute) > 0:
            i = to_execute.pop()

            if len(df.index.names) == 0:
                parameters = {}
            elif len(df.index.names) == 1:
                parameters = {df.index.name: df.index[i]}
            else:
                parameters = dict(zip(df.index.names, df.index[i]))

            if output_progress:
                if completed == 0:
                    eta = 'n/A'
                    eta_done = 'n/A'
                else:
                    eta = (datetime.now() - batch_start) / (completed + skipped) * len(to_execute)
                    eta_done = datetime.now() + eta
                    eta = pretty_time_delta(eta.total_seconds())

                    eta_done = eta_done.strftime("%d.%m %X")
                print("".join(["-"] * 60))
                print("{} Started with {}/{} ({} failed), {} remaining ({})\nParameters:\n{}".format(
                    datetime.now().strftime("%X"), completed + skipped + 1, total_jobs, skipped, eta, eta_done,
                    pd.Series(parameters)))
                print("".join(["-"] * 60))

            start = datetime.now()
            try:
                df.iloc[i, df.columns.get_loc("executed")] = start
            except ValueError:
                print("Execution time could not be stored")

            c_i = [df.columns.get_loc(c) for c in df.columns
                   if c not in ["executed", "duration"]]
            row_container = _Row(df.iloc[i, c_i], parameters)
            yield parameters, row_container

            if row_container.failed:
                # skipped
                print("Skipping test {}".format(i))
                skipped += 1
            else:
                if isinstance(row_container.data, pd.Series):
                    for c in row_container.data.keys():
                        c_i = df.columns.get_loc(c)
                        df.iloc[i, c_i] = row_container[c]

                    df.iloc[i, df.columns.get_loc("duration")] = datetime.now() - start
                elif isinstance(row_container.data, pd.DataFrame):
                    # empty index

                    for new_index in set(row_container.data.index.names) - set(df.index.names):
                        if new_index in df.columns:
                            raise NotImplementedError("For implementation reasons, new index name {} "
                                                      "must not be present in batcher columns".format(new_index))

                        df[new_index] = np.nan
                        df.set_index(new_index, append=True, inplace=True)

                    row_container.data["executed"] = start
                    row_container.data["duration"] = datetime.now() - start
                    # expand dataframe
                    correct_order = row_container.data.reorder_levels(df.index.names)
                    self._data_frame_int = pd.concat([df.iloc[:i, :], correct_order, df.iloc[i + 1:]])
                    df = self._data_frame_int
                    # update indices of to_execute
                    to_execute = [t + len(row_container.data) - 1 if t > i else t for t in to_execute]
                else:
                    raise TypeError("Row container data must be Series or DataFrame")

                completed += 1
                self.unsaved += 1

            if (intermediate_save is not None and self.unsaved > intermediate_save) or \
                    (save_interval is not None and time.time() - self.last_save_time > save_interval):
                if self._store is not None:
                    self.save_to_store()
        return skipped

    @staticmethod
    def grid_search(**columns):
        values = [v if isinstance(v, collections.Iterable) else [v] for v in columns.values()]
        index = pd.MultiIndex.from_product(values, names=[k for k in columns.keys()])
        return pd.DataFrame(index=index)

    # fix _display_columns of class to underlying _data frame
    def __str__(self):
        return self._data_frame_int.__str__()

    def __repr__(self):
        return self._data_frame_int.__repr__()

    def _repr_html_(self):
        return self._data_frame_int._repr_html_()


class Analyzer(ABC):
    """
    Simplify batch testing by supplying standardized interface to evaluate_and_show models
    """

    @abstractmethod
    def evaluate(self, *args, **kwargs) -> Union[dict, pd.Series, pd.DataFrame]:
        pass


class BatchExecutor(Batcher, Analyzer, metaclass=ABCMeta):
    """
    Supervisor to perform all tests of a Analyzer for given common_parameters of a _executor class
    """

    def run(self, *args, **kwargs):

        eval_arguments = inspect.signature(self.evaluate).parameters.keys()
        for parameters, current_row in super().run(*args, **kwargs):
            try:
                filtered_params = {k: v for k, v in parameters.items() if k in eval_arguments}
                results = self.evaluate(**filtered_params)

                yield parameters, results

                # store to batcher
                if isinstance(results, dict):
                    results = pd.DataFrame(pd.Series(results)).T
                else:
                    # must be dict or dataFrame
                    assert isinstance(results, pd.DataFrame)

                current_row.data = results

            except (ValueError, MemoryError, AttributeError, KeyError, AssertionError) as e:
                current_row.failed = True
                print(e)

    def run_all(self, *args, **kwargs):
        for _ in self.run(*args, **kwargs):
            pass


def create_headless_batch_executor(analyzer_cls: Analyzer.__class__):
    class ConcreteHeadlessBatchExecutor(Batcher, analyzer_cls):
        pass

    return ConcreteHeadlessBatchExecutor


class _Row:
    def __init__(self, row: pd.Series, parameters: dict):
        self._data = row
        self.parameters = parameters
        self.failed = False

    def __getitem__(self, column):
        return self._data.__getitem__(column)

    def __setitem__(self, column, value):
        if column not in self._data.keys():
            raise KeyError("Batcher only has columns {}".format(self._data.keys()))
        else:
            self._data.__setitem__(column, value)

    def __repr__(self):
        return "Parameters: \n{}\n\nResults:\n{}".format(pd.Series(self.parameters).__repr__(),
                                                         self._data.__repr__())

    def keys(self):
        return self._data.keys()

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, df: Union[pd.Series, pd.DataFrame]):
        if isinstance(df, pd.Series):
            if not set(df.keys()) == set(self._data.keys()):
                raise KeyError("Only columns {} are present in batcher, batcher got {} "
                               "To be able to add new result columns dynamically, "
                               "use a DataFrame".format(self._data.keys(), self._data.keys()))
            self._data = df
            return

        assert isinstance(df, pd.DataFrame)

        if df.index.nlevels == 1 and (all(df.index.get_level_values(0).isnull()) or len(df) == 1):
            remove_first_level = True
        else:
            remove_first_level = False

        # remove indexes that need to be expanded
        for k in df.index.names:
            if k in self.parameters.keys():
                if self.parameters[k] is not np.nan:
                    raise KeyError("Key {} is illegal to return from evaluator as "
                                   "batcher as a non-null value for this key".format(k))
                self.parameters.pop(k)

        for k in self.parameters.keys():
            if k in df.columns:
                raise KeyError("For technical reasons, parameter key {} may not be par of result columns".format(k))
            df[k] = [self.parameters[k]] * len(df)
            df.set_index(k, append=True, inplace=True)

        if remove_first_level:
            df.index = df.index.droplevel(0)

        self._data = df


# https://gist.github.com/thatalextaylor/7408395
def pretty_time_delta(seconds):
    sign_string = '-' if seconds < 0 else ''
    seconds = abs(int(seconds))
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        return '%s%dd%dh%dm%ds' % (sign_string, days, hours, minutes, seconds)
    elif hours > 0:
        return '%s%dh%dm%ds' % (sign_string, hours, minutes, seconds)
    elif minutes > 0:
        return '%s%dm%ds' % (sign_string, minutes, seconds)
    else:
        return '%s%ds' % (sign_string, seconds)


if __name__ == "__main__":
    """
    Example Usage of batcher class
    """


    def expensive_function_to_compute(a, b):
        time.sleep(1)
        return a * b


    # initialize hdf5 store to load and later write to
    experiment = Batcher(store='foo.h5')

    # prepare input _data, can be an arbitrary dataframe,
    # Batcher.grid_search is equivalent to an exhaustive search through given parameter space
    parameter_space = Batcher.grid_search(A=np.arange(10), B=np.arange(3))
    experiment.set_data(parameter_space, result_columns=['mult'])

    # run method performs experiment,
    # performs intermediate snapshot after every _data by default
    for param, result in experiment.run():
        result.mult = expensive_function_to_compute(param['A'], param['B'])

    # It is possible to load Batcher store while still running and to start evaluation on completed rows
    # For access syntax, see pandas DataFrame
    print(experiment.get_completed())
