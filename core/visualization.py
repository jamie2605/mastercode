import os
import re
import sys
from abc import ABC, abstractmethod
from datetime import datetime
from typing import Callable, Union, List

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib2tikz

from core.analysis import BatchExecutor

import warnings


class InteractivePlotting(ABC):
    def __new__(cls, *args, **kwargs):
        obj = super().__new__(cls)
        obj._keymaps = {}
        obj._state_values_lookup = {}
        obj._parameter_tracker = {}
        obj._state = {}
        return obj

    def __init__(self, **kwargs):
        initial_state = kwargs.pop("initial_state", None)
        subplot_args = kwargs.pop("subplot_args", {})
        figure_title = kwargs.pop("figure_title", None)
        draw = kwargs.pop("draw", True)

        super().__init__(**kwargs)

        if initial_state is not None:
            # check validity of initial state
            assert all([k in self._state_values_lookup for k in self._state.keys()])
            self._state = initial_state
        self.fig, self.ax = plt.subplots(**subplot_args)

        try:
            chapter, subchapter, name = get_chapter()
            title = "{}.{} {} - {}".format(chapter, subchapter, name, self.__class__.__name__)
        except AttributeError:
            # ignore, simply do not set title
            title = "{}".format(self.__class__.__name__)

        self.fig.canvas.set_window_title(title)

        self.art = self.prepare_plot()

        if figure_title is not None:
            self.ax.set_title(figure_title)

        if draw:
            self.draw()
            self.fig.canvas.draw()
            self.fig.show()

        self.fig.canvas.mpl_connect('key_press_event', self._press)
        print(self.help())

    def save_current_to_plot_stream(self, *args, **kwargs):
        suffix = kwargs.pop('name_suffix', '')
        suffix = "{}_{}{}".format(self.__class__.__name__, self.fig.number, suffix)
        kwargs['name_suffix'] = suffix
        save_to_plot_stream(self.fig, *args, **kwargs)

        return self

    @abstractmethod
    def draw(self):
        pass

    def safe_draw(self, new_state):
        # try to draw, revert to old state if draw returns false
        old_state = self._state
        self._state = new_state
        if self.draw() is False:
            if old_state != new_state:
                print("Draw failed, reverting to previous state")
                self._state = old_state
                self.draw()

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def prepare_plot(self):
        # In default implementation, nothing is prepared on plot
        pass

    def _press(self, event):
        return self.press(event.key)

    def press(self, key):
        if key not in self._keymaps.keys():
            return

        new_state = self._keymaps[key](self._state)

        # custom keymaps may not return a state
        if new_state is None:
            new_state = self._state

        self.safe_draw(new_state)

    def add_state_parameter(self, parameter: str, values: Union[List, Callable], increase=None, decrease=None,
                            initial_i: int = 0):
        """

        :param initial_i: at which position in values array to initialize the state variable
        :param parameter: which column in the _data frame to use
        :param increase: key to use for choosing next value
        :param decrease: key to use for choosing previous value
        :param values: list of possible values for state parameter or
                        callable to return list
        :return:
        """
        # store possible values, make list callable
        if not callable(values):
            def val_func():
                return values
        else:
            val_func = values

        # Make sure every parameter is only added once, every key only added once
        assert parameter not in self._state_values_lookup.keys()
        assert increase not in self._keymaps.keys()
        assert decrease not in self._keymaps.keys()

        self._state_values_lookup[parameter] = val_func

        # register keymaps
        if increase is not None:
            self._keymaps[increase] = lambda state: {k: (v + 1 if k == parameter else v) for k, v in state.items()}

        if decrease is not None:
            self._keymaps[decrease] = lambda state: {k: (v - 1 if k == parameter else v) for k, v in state.items()}

        self._parameter_tracker[parameter] = {"increase": increase, "decrease": decrease}
        self._state[parameter] = initial_i

    def remove_state_parameter(self, parameter: str, error_on_noexist: bool = True):
        exists = parameter in self._state_values_lookup.keys()

        if not exists:
            if error_on_noexist:
                raise KeyError("Parameter {} does not exist".format(parameter))
            else:
                return

        for d in ["increase", "decrease"]:
            key = self._parameter_tracker[parameter][d]
            if key is not None:
                self._keymaps.pop(key)
        self._parameter_tracker.pop(parameter)
        self._state_values_lookup.pop(parameter)

    def add_custom_keymap(self, key: str, callback: Callable):
        assert key not in self._keymaps.keys()
        self._keymaps[key] = lambda state: callback()

    def remove_custom_keymap(self, key: str):
        assert key in self._keymaps.keys()
        # these must be removed via state mps_parameters
        assert key not in self._parameter_tracker.keys()

        self._keymaps.pop(key)

    def link_keys(self, master: str, slaves: List[str]):
        assert all([s in self._keymaps.keys() for s in slaves])

        def press_all(state):
            for s in slaves:
                state = self._keymaps[s](state)

        self._keymaps[master] = press_all

    def update_state_by_value(self, update_values: dict, draw: bool = True):
        # use all keys from update values that exist as state values
        new_state = {}
        for k, v in update_values.items():
            if k in self._state_values_lookup.keys():
                index = np.where(v == self._state_values_lookup[k]())[0]
                if len(index) == 0:
                    index = np.where([val is v for val in self._state_values_lookup[k]()])[0]
                    if len(index) == 0:
                        print("Notice: {} could not be set to {}, ignoring state update."
                              "This could be due to nan or None in state values, please"
                              "do not use these values for anything other than missing data".format(k, v))
                        continue
                new_state[k] = index[0]

        # merge with existing fields of state that keep their value
        new_state = {**new_state, **{k: v for k, v in self._state.items() if k not in new_state.keys()}}

        if draw:
            self.safe_draw(new_state)
        else:
            self._state = new_state

    @property
    def state_vals(self):
        return self.get_state_vals()

    def get_state_vals(self, state: dict = None):
        if state is None:
            state = self._state

        state_vals = {}
        for col, val_i in state.items():
            possible_values = self._state_values_lookup[col]()
            if len(possible_values) == 0:
                state_vals[col] = np.nan
            else:
                state_vals[col] = possible_values[val_i % len(possible_values)]

        return state_vals

    def help(self):
        """
        Prints cheat sheet of keyboard shortcuts
        :return:
        """
        if self._parameter_tracker == {}:
            return ""
        doc = pd.DataFrame.from_dict(self._parameter_tracker, orient='index')
        doc["value_cycle"] = [self._state_values_lookup[c]() for c in doc.index]
        return "Use the following keyboard shorcuts to modify state parameters of {}: " \
               "\n{}".format(self.__class__.__name__, doc)

    def __repr__(self):
        return "{}\n{}".format(super().__repr__(), self.help())


class DataFrameFilterInteractivePlotting(InteractivePlotting):
    @abstractmethod
    def draw(self):
        pass

    def __new__(cls, **kwargs):
        obj = super().__new__(cls, **kwargs)
        obj._filter_columns = []
        return obj

    def __init__(self, **kwargs):

        data = kwargs.pop("data", pd.DataFrame())
        assert (isinstance(data, pd.DataFrame))

        self.__data_frame = data

        super().__init__(**kwargs)

        # now data frame is definitely added, so check filter keys
        invalid_columns = set(self._filter_columns) - set(self.data_frame.index.names)
        if len(invalid_columns) > 0 and invalid_columns is not {None} and len(self.data_frame.index > 0):
            raise KeyError("Invalid filter columns: ".format(invalid_columns))

    @property
    def data_frame(self):
        return self.__data_frame

    def add_state_parameter_filter(self, column: str, increase=None, decrease=None):

        if column in self._state_values_lookup.keys():
            # do not add a column twice, ignore request instead (assuming same column was added by parent/sibling
            return

        def possible_values(placeholder=None):
            if placeholder is None:
                placeholder = []
            if column not in self.data_frame.index.names:
                return placeholder
            level = self.data_frame.index.names.index(column)
            return self.data_frame.index.get_level_values(level).unique().values

        self.add_state_parameter(column, values=possible_values, increase=increase, decrease=decrease)
        self._filter_columns.append(column)

    def remove_state_parameter(self, parameter: str, error_on_noexist: bool = True):
        super().remove_state_parameter(parameter, error_on_noexist)
        if parameter in self._filter_columns:
            self._filter_columns.remove(parameter)
            self._state.pop(parameter, None)

    def get_results(self, state_values: dict = None, df: pd.DataFrame = None, drop_filter_keys=False) -> pd.DataFrame:

        if df is None:
            df = self.data_frame

        if len(df.index) == 0:
            # is completely empty dataframe
            return pd.DataFrame()

        if state_values is None:
            state_values = self.state_vals

        filter_state_values = {k: v for k, v in state_values.items() if k in self._filter_columns}

        if len(filter_state_values) == 0:
            results = df
        else:
            index = np.all([df.index.get_level_values(k) == v
                            for k, v in filter_state_values.items()], axis=0)
            results = df.iloc[index]

        if drop_filter_keys and results.index.nlevels > 1:
            results.index = results.index.droplevel([results.index.names.index(l) for l in filter_state_values.keys()])

        return results

    @staticmethod
    def filter_results(results_in: pd.DataFrame, index_level: Union[int, str], index_value):
        select = results_in.index.get_level_values(index_level) == index_value
        return results_in.iloc[select]


class AnalyzerFigure(DataFrameFilterInteractivePlotting, BatchExecutor, ABC):
    @property
    def data_frame(self):
        if self.parameters_set:
            # redirect data_frame requests to batcher dataframe
            return self.get_completed(True)
        else:
            return super().data_frame

    def run_all(self, *args, update_plot_interval=1, force_without_store=False, **kwargs):
        if not force_without_store and self._store is None:
            raise Exception("Warning: No store is added, set force_without_store=True "
                            "to continue without checkpoints to disk")

        if not self.parameters_set:
            print("Parameters have not been set yet, nothing will be executed.")

        last_stored_parameters = self.state_vals
        for i, (parameters, results) in enumerate(self.run(*args, **kwargs)):
            # update visualization of last evaluated_point
            if i % update_plot_interval == 0 and i > 0:

                try:
                    self.update_state_by_value(last_stored_parameters, draw=True)
                except (KeyError, ValueError, TypeError) as e:
                    warnings.warn(e.message)

            last_stored_parameters = parameters

        self.update_state_by_value(last_stored_parameters, draw=True)

    def evaluate_and_show(self, **kwargs):
        """
        Use analyzer to do a single evaluation. This row will then be added to the batcher classes
        dataframe and the figure will be redrawn
        :param kwargs: depend on analyzer implementation
        :return:
        """
        start = datetime.now()
        results = self.evaluate(**kwargs)
        if not isinstance(results, pd.DataFrame):
            raise NotImplementedError("For direct calling of evaluation method for visualization, the Analyzer"
                                      "may only return a DataFrame")

        results["executed"] = datetime.now()
        results["duration"] = datetime.now() - start

        # add kwargs to index
        for k, v in kwargs.items():
            results[k] = v
        results.set_index([k for k in kwargs.keys()], inplace=True, append=True)

        # add results to Batcher, get results with possibly added indices back
        results = self.append_data(results, add_indices=True)

        # display results
        # try to set as many state variables as possible to parameters known by evaluate_and_show method
        new_state_values = {s: v for s, v in kwargs.items() if s in self._state.keys()}
        self.update_state_by_value(new_state_values, draw=True)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()


def minmax(arr):
    return np.nanmin(arr), np.nanmax(arr)


def color_map(values, cmap='rainbow'):
    """
    Shortcut for creating a nice colorbar for parametric plotting
    :param values: common_parameters for colors
    :param cmap: what colormap to use
    :return:
    """
    values = np.array(values)
    if max(values) != 0:
        values = values /np.max(values)
    colors = plt.cm.rainbow(values)
    mappable = plt.cm.ScalarMappable(cmap=cmap)
    mappable.set_array(values)

    # tikz package has problem with small values (as in scientific notation),
    # since the human eye will not be able to notice these differences, set them to 0
    colors[colors < 1e-4] = 0
    return colors, mappable


def get_chapter():
    file_path = os.path.realpath(sys.argv[0])
    entries = file_path.split(os.sep)

    chapter = entries[-2]
    sub_chapter = entries[-1]

    find_number = re.compile("([0-9]+)_*")
    chapter = find_number.match(chapter).group(1)
    sub_chapter = find_number.match(sub_chapter).group(1)

    find_name = re.compile("[0-9]+_(.+)\.py")
    name = find_name.match(entries[-1]).group(1)
    name = name.replace("_", " ")

    return chapter, sub_chapter, name


def save_to_plot_stream(figs=None,
                        save_png: bool = True,
                        save_pdf: bool = True,
                        save_pgf: bool = False,
                        save_tex: bool = False,
                        name_suffix: str = None):
    plot_path = '/home/ben/masterphase/plot_stream'

    if figs is None:
        figs = plt.gcf()

    if not isinstance(figs, list):
        figs = [figs]

    for fig in figs:

        if name_suffix is None:
            name_suffix = fig.number
        else:
            name_suffix = "{}_".format(name_suffix)

        try:
            chapter, sub_chapter, _ = get_chapter()
        except AttributeError:
            # in interactive mode, chapter cannot be found
            chapter, sub_chapter = (None, None)

        plot_file_path = os.path.join(plot_path,
                                      "{}_{}_{}{}".format(chapter,
                                                          sub_chapter,
                                                          name_suffix,
                                                          datetime.now().strftime("%Y%m%d")))

        if save_png:
            fig.savefig("{}.png".format(plot_file_path))

        if save_pdf:
            fig.savefig("{}.pdf".format(plot_file_path))

        if save_pgf:
            plt.rcParams['pgf.rcfonts'] = False
            fig.savefig("{}.pgf".format(plot_file_path))

        if save_tex:
            matplotlib2tikz.save("{}.tex".format(plot_file_path),
                                 figureheight='\\figureheight',
                                 figurewidth='\\figurewidth')
