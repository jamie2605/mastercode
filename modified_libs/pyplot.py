from matplotlib.pyplot import *
from core.visualization import save_to_plot_stream

show_backup = show


def show(*args, save_pdf=True, save_png=True, save_pgf=True, **kwargs):

    figures = [figure(i) for i in get_fignums()]
    save_to_plot_stream(figures, save_pdf=save_pdf, save_png=save_png, save_pgf=save_pgf)

    show_backup(*args, **kwargs)

