from MPSPyLib import *
import subprocess
import multiprocessing
import time
# from helper.events import MPSResultHandler
import os
from datetime import datetime

# backup function to parallelize
ReadStaticObservablesSerial = ReadStaticObservables


# prepare simulation call for single file
def _run_simulation(counter, infile, base_command, Debug):
    command = [base_command] + [infile[0], infile[1]]
    if Debug:
        command[4] = '--log-file=' + str(counter)

    start = time.time()
    ret_val = subprocess.call(subprocess.list2cmdline(command), shell=True)
    if ret_val != 0:
        raise MPSFortLibError(ret_val)

    return counter, time.time() - start


def runMPS(infileList, RunDir=None, Debug=False, customAppl=None,
           parallel=None,
           save_simulation_times=None, progress=True):
    """
    Run the mps application with the given list of input files.

    **Arguments**

    infileList : list
        list of of the main files passed to fortran executable as
        command line argument.

    RunDir : str, optional
        Specifies path of the fortran-executable (local or global
        installation). This is available for default-simulation and
        custom application. (@Developers: Debugging with valgrind always
        works with a local installation.)
        `None` : if executable available within local folder, use local
        installation, otherwise assume global installation, default
        '' (empty string) : for global installation
        './' : local installation
        PATH+'/' : path to executable ending on slash `/`. Additional steps
        may be necessary when using this.

    Debug : boolean, optional
        Developers only:
        false : run usual simulation, default
        true : run debugging with valgrind using local installation

    customAppl : str, optional
        define custom executable. Global and local installation work
        as before. Custom applications cannot run with valgrind.
        default to `None` (running default executable)

    parallel : boolean or int
        if True: parallelize to all available cores, if int, use that number of
        processes
    """
    # Process custom application input
    if customAppl is None:
        customAppl = 'Execute_MPSMain'

    if RunDir is None:
        if os.path.isfile('./' + customAppl):
            # Local installation
            appname = './' + customAppl
        else:
            # Global installation
            appname = customAppl

    else:
        appname = RunDir + customAppl

    if Debug:
        cmdline = ['valgrind', '--tool=memcheck', '--leak-check=yes',
                   '--show-reachable=yes', '--log-file=None' + '.out', appname]
    else:
        cmdline = appname

    # execute commands
    if parallel is None or parallel is False:
        simulation_times = [_run_simulation(*enum, cmdline, Debug)[0] for enum in enumerate(infileList)]
    else:
        if parallel is True:
            n_processes = multiprocessing.cpu_count()
        else:
            n_processes = parallel

        # submit tasks to pool, gather result objects
        pool = multiprocessing.Pool(processes=n_processes)
        results = []
        for i, file in enumerate(infileList):
            results.append(pool.apply_async(_run_simulation, (i, file, cmdline, Debug)))

        # simply wait till all results can be gotten
        simulation_times = []
        for i, r in enumerate(results):
            simulation_times.append(r.get()[1])
            if progress:
                print("{} : Completed at least {}/{} simulations".format(datetime.now(), i+1, len(results)))

    np.save(os.path.join(os.path.dirname(infileList[0][0]), 'simulation_times.npy'), simulation_times)
    return simulation_times


def read_or_simulate(parameters, MainFiles=None, read_only=False, force_simulate=False, parallel=True):
    """
    Tries to read Observables, if not found, run with supplied common_parameters
    :param parameters:
    :param MainFiles:
    :param force_simulate:
    :param parallel:
    :return:
    """
    if MainFiles is None:
        assert read_only
    if read_only:
        assert not force_simulate
    simulation_times = None

    Outputs = None
    need_to_simulate = False
    # check if we can load from disk
    if not force_simulate:
        try:
            Outputs = ReadStaticObservables(parameters, parallel=parallel)
        except (IOError, ValueError, KeyError) as e:
            need_to_simulate = True
            if read_only:
                raise Exception("Could not read, remove read_only flag to perform simulation")

    # do simulation if we need to
    if force_simulate or need_to_simulate:
        simulation_times = runMPS(MainFiles, parallel=parallel)

    # if outputs not loaded yet, load them now
    if Outputs is None:
        Outputs = ReadStaticObservables(parameters, parallel=parallel)

    return Outputs


def ReadStaticObservables(parameters, suppress_errors=False, parallel=True):
    """
    Wrapper to parallelize ReadStaticObservables, especially useful if used with an SSD in the system
    :param parameters:
    :param suppress_errors:
    :param parallel:
    :return:
    """
    if parallel is False:
        return ReadStaticObservablesSerial(parameters, suppress_errors)
    
    elif parallel is True:
        n_processes = multiprocessing.cpu_count()
    else:
        n_processes = parallel
        
    # submit tasks to pool, gather result objects in correct order
    pool = multiprocessing.Pool(processes=n_processes)
    results = [None] * len(parameters)
    for i, parameter in enumerate(parameters):
        results[i] = pool.apply_async(ReadStaticObservablesSerial, ([parameter], suppress_errors))

    # wait till all results are loaded
    return [r.get()[0] for r in results]
