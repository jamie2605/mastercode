import time

import numpy as np
from qutip import *

from pycuda import gpuarray as gpuarray, autoinit
from skcuda import linalg


class GpuQobj(Qobj):

    """
    Behaves like Qobj for any operation if gpu=False.
    Uses GPU eigensolver for gpu = True
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def eigenstates(self, *args, gpu=False, progress=True, **kwargs):
        if gpu:
            assert "sparse" not in kwargs.keys() or not kwargs["sparse"]
            # GPU can only work with real values
            if np.sum(np.imag(self.data) > 0) > 0:
                raise ValueError("Eigenstates on GPU can only be found for quantum objects with real entries")

            # start gpu linear algebra library
            linalg.init()

            obj_gpu = gpuarray.to_gpu(np.real(self.data.todense()))
            start_gpu = time.time()
            psi, E = linalg.eig(obj_gpu, 'n', 'V', lib='cusolver')
            if progress:
                print("GPU solved Eigenstates in {}s (no transfer)".format(time.time()-start_gpu))
            psi = psi.get()
            # new dimension calculation (taken from cpu implementation)
            new_dims = [self.dims[0], [1] * len(self.dims[0])]
            return E.get(), [GpuQobj(p, dims=new_dims) for p in psi]

        else:
            return super(GpuQobj, self).eigenstates(*args, **kwargs)

    # def ground_state_configuration_probabilities(self, gpu=False, save_to=None) -> np.ndarray:
    #     """
    #     Construct ising hamiltonian and give configuration probabilities
    #     :param save_to: load/save probabilities in this directory
    #     :param gpu: Whether to use gpu for computation
    #     :return: configuration probabilites
    #     """
    #     # caching
    #     if save_to is not None:
    #         file_name = save_to
    #         full_path = os.path.join(save_to, file_name)
    #         if os.path.isfile(full_path):
    #             return np.load(full_path)
    #
    #     energy, psi = self.eigenstates(gpu=gpu)
    #
    #     # degenerate ground state treatment
    #     if energy[1] - energy[0] < 1e-10:
    #         print("degenerate ground state")
    #         ground = [psi[0], psi[1]]
    #     else:
    #         ground = [psi[0]]
    #
    #     # get probabilities from psi_0
    #     n = len(self.dims[0])
    #     probabilities = np.zeros(2 ** n)
    #     for psi_0 in ground:
    #         probabilities += np.array(np.real(psi_0._data.todense())).flatten()
    #
    #     probabilities = probabilities ** 2 / len(ground)
    #
    #     # save probabilities to file
    #     if save_to is not None:
    #         np.save(full_path, probabilities)
    #
    #     return probabilities