from analyzers.model_mixing import ModelMixingFigure
from core import Batcher
from features import FeatureSet
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

if __name__ == '__main__':
    feature_file = "/home/ben/masterphase/data/feature_sets/ising_quantum/mps/ml/long_range/{}.h5"
    store = "/home/ben/masterphase/data/analysis/2_TFI/9_model_mixing/large_sweep.h5"

    interactions = [(1, None), (2, -3), (np.inf, -3), (2, -6), (np.inf, -6)]

    # create_long_range_sets(feature_file_template, interactions)
    features = {interaction: FeatureSet(feature_file.format(interaction)) for interaction in interactions}

    analysis = ModelMixingFigure(feature_sets=features, store=store)

    if not analysis.parameters_set:
        parameters = Batcher.grid_search(use_train=interactions, use_test=interactions)
        parameters = pd.concat([parameters]*10)
        analysis.set_data(parameters)

    analysis.run_all()

    plt.show()
