import numpy as np
import pandas as pd
import modified_libs.pyplot as plt

import time

from core.analysis import Batcher
from analyzers.network_response_basics import ResponseFigure
from features import FeatureSet
from features.feature_factories_ml import TFISpinConfigurationExactDiagonalizationFactory
from physics.models_qutip import ising_hamiltonian
from visualization.features import Magnetization1D, TSNEView


def measure_eigenvalue_runtime(no_particles, h=0, gpu=True):
    start = time.time()
    ising_hamiltonian(no_particles, h, J=1).eigenstates(gpu=gpu)
    t = time.time() - start
    return t


def plot_runtime_dependence(sim_results: pd.DataFrame, group_by='h'):
    sim_results = sim_results.astype('float')

    if group_by == 'h':
        for h, hgroup in sim_results.groupby(level='h'):
            df = hgroup.reset_index(level='h', drop=True)

            # average over _data sets
            val = df.mean(level='n')
            err = df.std(level='n')

            val.plot(yerr=err, title="h={}".format(h), logy=True)
            plt.ylabel('runtime/s')
            plt.gcf().canvas.draw()
    else:
        for device in ['cpu', 'gpu']:
            fig = plt.figure()
            ax = fig.add_subplot(111, title=device)
            for h, hgroup in sim_results.groupby(level='h'):
                df = hgroup.reset_index(level='h', drop=True)

                val = df.mean(level='n')
                err = df.std(level='n')

                val[device].plot(ax=ax, yerr=err[device], logy=True, label=h)

            ax.set_ylabel('runtime/s')
            ax.legend(title='h')
            fig.canvas.draw()
        pass


def evaluate_runtime_dependence(system_sizes=np.arange(1, 13)):
    rep = np.arange(3)
    hs = np.linspace(0, 2, 5)

    b = Batcher(store='/home/ben/masterphase/_data/analysis/2_TFI/1_ED/runtimes.h5')

    if b.empty:
        b.set_data(Batcher.grid_search(N=system_sizes, repetition=rep, h=hs), result_columns=['gpu', 'cpu'])

    for parameters, results in b.run(save_interval=10):
        results.gpu = measure_eigenvalue_runtime(parameters['n'], parameters['h'], gpu=True)
        results.cpu = measure_eigenvalue_runtime(parameters['n'], parameters['h'], gpu=False)

    plot_runtime_dependence(b.get_completed(True), group_by='h')
    plot_runtime_dependence(b.get_completed(True), group_by='device')

    plt.show()


def create_feature_set(system_size=10):
    simulation = TFISpinConfigurationExactDiagonalizationFactory(system_size=system_size)
    data = simulation.create_feature_set()
    data.save('/home/ben/masterphase/data/features_sets/ising_quantum/exact/{}_particles.h5'.format(system_size))

    return data


if __name__ == '__main__':
    # evaluate_runtime_dependence()
    # _data = create_feature_set(system_size=2)
    data = FeatureSet('/home/ben/masterphase/data/feature_sets/ising_quantum/exact/10_particles.h5')

    response = ResponseFigure(feature_set=data,
                              threshold_column='h_x',
                              threshold=1,
                              train_values=(0, 2),
                              test_values=(0.5, 1.5))
    for i in range(10):
        response.evaluate_and_show()

    magnetization = Magnetization1D(data)
    tsne = TSNEView(data)

    plt.show()
