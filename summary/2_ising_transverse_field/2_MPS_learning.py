from numpy import linspace
import pandas as pd

from analyzers.asymmetric_training import AsymmetricTrainingFigure
from core.analysis import Batcher
from analyzers.network_response_basics import ResponseFigure
from features import FeatureSet
from features.feature_factories_ml import TFIMPSCorrelationFactory

import matplotlib.pyplot as plt

from visualization.features import Magnetization1DScalingTFI

if __name__ == '__main__':

    # Feature set generation
    regenerate = False
    feature_sets = {}

    for n in [20, 50, 100, 200, 350, 700]:
        feature_file = "/home/ben/masterphase/data/feature_sets/ising_quantum/mps/ml/{}particles100hx.h5".format(n)

        if regenerate:
            factory = TFIMPSCorrelationFactory(model_parameters={'system_size': n},
                                               h_x=linspace(0, 2, 200))
            feature_set = factory.create_feature_set()
            feature_set.save(feature_file)

        else:
            feature_set = FeatureSet(feature_file)

        feature_sets[n] = feature_set

    # Scaling of magnetization
    mag = Magnetization1DScalingTFI(feature_sets, feature_type='zz-correlation')
    mag.save_current_to_plot_stream()
    mag.update_state_by_value({"rescale": False}, draw=True)
    mag.save_current_to_plot_stream()

    # Example neuron response
    response = ResponseFigure(feature_set=feature_sets[700],
                              threshold_column='h_x',
                              threshold=1,
                              train_values=(0, 2),
                              test_values=(0.5, 1.5))
    for i in range(10):
        response.evaluate_and_show()
    response.save_current_to_plot_stream()

    # perform asymmetric training
    data = FeatureSet('/home/ben/masterphase/data/feature_sets/ising_quantum/mps/ml/700particles100hx.h5').subsample(10000)
    store = '/home/ben/masterphase/data/analysis/2_TFI/2_MPS/2.2_MPS_asymmetric_training.h5'
    analysis = AsymmetricTrainingFigure(feature_set=data,
                                        threshold=1,
                                        threshold_column='h_x',
                                        store=store
                                        )
    # only set parameters if store was non-empty
    if analysis.empty:
        parameters = Batcher.grid_search(test_begin=linspace(0.1, 0.9, 9), test_end=linspace(1.1, 1.9, 9))
        # repetitions
        parameters = pd.concat([parameters] * 10)
        analysis.set_data(parameters)

    analysis.run_all()

    plt.show()
