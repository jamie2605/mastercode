
# Feature set generation
from numpy import linspace
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from core.analysis import Batcher
from analyzers.long_range import LongRangeResponseFigure
from features import FeatureSet
from features.feature_factories_ml import TFIMPSCorrelationFactory
from visualization.features import Magnetization1D

if __name__ == '__main__':
    regenerate = False
    feature_sets = {}

    interactions = [(1, None), (2, -3), (np.inf, -3), (2, -6), (np.inf, -6)]

    for interaction in interactions:
        feature_file = "/home/ben/masterphase/data/feature_sets/ising_quantum/mps/ml/long_range/{}.h5".\
            format(interaction)

        if regenerate:
            factory = TFIMPSCorrelationFactory(model_parameters={'system_size': 700,
                                                                 'interaction_r': interaction[0],
                                                                 'interaction_exp': interaction[1]},
                                               h_x=linspace(0, 2, 200))
            feature_set = factory.create_feature_set()
            feature_set.save(feature_file)

        else:
            feature_set = FeatureSet(feature_file)

        feature_sets[interaction] = feature_set

    # magnetization for all feature sets
    mag = Magnetization1D([f for f in feature_sets.values()], feature_type='zz-correlation',
                          labels=[l for l in feature_sets.keys()])
    mag.save_current_to_plot_stream()

    data = {}
    for interaction, f in feature_sets.items():
        train, test = f.split_label_value(test_values={'h_x': (0.3, 1.7)})
        data[interaction] = {"test": test, "train": train}

    store = "/home/ben/masterphase/data/analysis/2_TFI/3_long_range/long_range_2.h5"
    fig = LongRangeResponseFigure(feature_sets=data, store=store)

    if not fig.parameters_set:
        parameters = Batcher.grid_search(interaction=interactions)
        parameters = pd.concat([parameters] * 10)
        fig.set_data(parameters)

    fig.run_all()
    for f in [fig, mag]:
        f.ax.set_title("")
        f.fig.set_size_inches(3.3, 2.3)
        f.fig.tight_layout()
        f.ax.set_ylim((-0.04,1.04))

    mag.ax.set_xlim((0,1.5))
    fig.ax.set_xlim(1.2, 1.5)
    plt.show()
