from analyzers.ml_vs_network import NoiseComparison
from tools.helper import combine_from_stores
import matplotlib.pyplot as plt

if __name__ == '__main__':

    stores = ['/home/ben/masterphase/data/analysis/2_TFI/4_noise/ml_small.h5',
              '/home/ben/masterphase/data/analysis/2_TFI/4_noise/network_science_zz_small.h5']

    data = combine_from_stores(stores)
    analysis = NoiseComparison(data=data, threshold=1, threshold_column="h_x")

    plt.show()
