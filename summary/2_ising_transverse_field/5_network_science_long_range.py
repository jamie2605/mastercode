from numpy import linspace

from features import FeatureSet
from features.feature_factories_networks import IsingNetworkRawFactory, IsingNetworkMeasuresFeatureFactory
import numpy as np

from tools.helper import dict_key_to_axis
from visualization.network_science import LongRangeNetworkMeasureFigure

import matplotlib.pyplot as plt

from datetime import datetime


def create_long_range_sets(file_template, interactions):
    for interaction in interactions:
        feature_file = file_template.format(interaction)
        factory = IsingNetworkMeasuresFeatureFactory(model_parameters={'system_size': 700,
                                                                       'interaction_r': interaction[0],
                                                                       'interaction_exp': interaction[1],
                                                                       'observe_mutual_info': True},
                                                     network_type='zz',
                                                     h_x=linspace(0, 1, 10))
        start = datetime.now()
        feature_set = factory.create_feature_set()
        end = datetime.now() - start
        feature_set.save(feature_file)


if __name__ == '__main__':

    feature_file_template = "/home/ben/masterphase/data/feature_sets/ising_quantum/mps/networks/long_range/foo{}-zz.h5"
    interactions = [(1, None), (2, -3), (np.inf, -3), (2, -6), (np.inf, -6)]

    create_long_range_sets(feature_file_template, interactions)
    features = {interaction: FeatureSet(feature_file_template.format(interaction)) for interaction in interactions}

    # convert to DataFrame
    features = dict_key_to_axis({k: f.unified for k, f in features.items()}, "interaction")

    measure_fig = LongRangeNetworkMeasureFigure(data=features)
    plt.show()
