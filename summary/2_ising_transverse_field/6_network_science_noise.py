from analyzers.noise_networks import NetworkMeasureNoiseFigure
from core import Batcher
from features import FeatureSet
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

if __name__ == '__main__':
    feature_file = "/home/ben/masterphase/data/feature_sets/ising_quantum/mps/networks/700_zz_raw.h5"
    store = "/home/ben/masterphase/data/analysis/2_TFI/4_noise/network_science_zz_small.h5"

    feature_set = FeatureSet(feature_file)
    analysis = NetworkMeasureNoiseFigure(feature_set=feature_set,
                                         store=store)

    if not analysis.parameters_set:
        parameters = Batcher.grid_search(relative=np.linspace(0, 1, 5), absolute=np.linspace(0, 1, 5))
        parameters = pd.concat([parameters]*5)
        analysis.set_data(parameters)

    analysis.run_all()
    plt.show()
