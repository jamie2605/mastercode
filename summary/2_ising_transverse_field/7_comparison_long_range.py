from analyzers.ml_vs_network import LongRangeComparison
from features import FeatureSet
import numpy as np
import pandas as pd

from tools.helper import dict_key_to_axis

if __name__ == '__main__':

    # load Network data
    feature_file_template = "/home/ben/masterphase/data/feature_sets/ising_quantum/mps/networks/long_range/{}-zz.h5"
    interactions = [(1, None), (2, -3), (np.inf, -3), (2, -6), (np.inf, -6)]

    network_measures = {interaction: FeatureSet(feature_file_template.format(interaction))
                        for interaction in interactions}
    network_measures = dict_key_to_axis({k: f.unified.apply(lambda x: x/np.max(x))
                                         for k, f in network_measures.items()}, "interaction")

    # load network training responses
    store = "/home/ben/masterphase/data/analysis/2_TFI/3_long_range/long_range_initial.h5"
    responses = pd.read_hdf(store, key='results')
    responses = responses[[c for c in responses.columns if c not in ["duration", "executed"]]]

    responses = responses.reorder_levels(network_measures.index.names)
    full_data = pd.concat([network_measures, responses], axis=1)

    # load zz-correlation feature sets for magnetization
    template = "/home/ben/masterphase/data/feature_sets/ising_quantum/mps/ml/long_range/{}.h5"
    feature_sets = {interaction: FeatureSet(template.format(interaction)) for interaction in interactions}

    fig = LongRangeComparison(data=full_data, feature_sets=feature_sets)

    pass