from features.feature_factories_networks import IsingNetworkRawFactory
import numpy as np


def create_feature_set(file, n=700, network_key='zz'):
    # create NN feature set
    factory = IsingNetworkRawFactory({"system_size": n, "observe_mutual_info": True},
                                     h_x=np.linspace(0, 2, 200), network_type=network_key)
    fs = factory.create_feature_set()
    fs.save(file)


if __name__ == '__main__':

    for network_type in ['zz', 'MI']:
        system_size = 700
        feature_file = "/home/ben/masterphase/data/feature_sets/ising_quantum/mps/networks/{}_{}_raw.h5"
        feature_file = feature_file.format(system_size, network_type)
        create_feature_set(feature_file, system_size, network_type)
