from core.analysis import Batcher
from analyzers.noise import MLNoiseFigure
from features import FeatureSet
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

if __name__ == '__main__':
    features = FeatureSet("/home/ben/masterphase/data/feature_sets/ising_quantum/mps/ml/700particles100hx.h5")
    store = "/home/ben/masterphase/data/analysis/2_TFI/4_noise/ml_small.h5"

    analysis = MLNoiseFigure(feature_set=features, store=store)

    if not analysis.parameters_set:
        parameters = Batcher.grid_search(relative=np.linspace(0, 1, 5), absolute=np.linspace(0, 1, 5))
        parameters = pd.concat([parameters]*5)
        analysis.set_data(parameters)

    analysis.run_all()
    analysis.fig.set_size_inches(3, 2)
    analysis.fig.tight_layout()

    plt.show()
