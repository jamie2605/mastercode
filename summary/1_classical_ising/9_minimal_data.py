from pandas import DataFrame

from analyzers.minimal_data import MinimalDataFigure
from core import Batcher
from features import FeatureSet
import numpy as np
import pandas as pd

if __name__ == '__main__':
    features = FeatureSet("ising_quantum/mps/ml/700particles100hx.h5")

    analysis = MinimalDataFigure(store='/home/ben/masterphase/data/analysis/2_TFI/9minimal_datalessdata.h5',
                                 feature_set=features,
                                 threshold=1,
                                 threshold_column='h_x')

    if analysis.empty:
        parameters = Batcher.grid_search(train_n=np.array([5, 50, 200, 500, 10000]))
        analysis.set_data(pd.concat([parameters]*10))

    analysis.run_all()

    analysis.ax.set_xlim((0.85, 1.15))
    analysis.fig.set_size_inches(4.5, 3.5)
    analysis.fig.tight_layout()

    pass