from features import FeatureSet
from features.feature_factories_ml import Classical2DIsingFactory, AlpsSpinDataParser, LukasSpinDataParser
from visualization.features import Magnetization1D
import numpy as np

if __name__ == '__main__':

    resample = False

    own_data_file = 'ising_classical/own.h5'
    lukas_file = 'ising_classical/lukas.h5'
    alps_file = 'ising_classical/alps_train.h5'

    if resample:
        own = Classical2DIsingFactory().create_feature_set().save(own_data_file)
        lukas = LukasSpinDataParser('/home/ben/masterphase/data/mc_lukas/16x16_60temp_1000each/').train.save(lukas_file)
        alps = AlpsSpinDataParser('/home/ben/masterphase/data/mc_alps/configs3/',
                                  subdirectories=False, skiprows=10).data.save(alps_file)
    else:
        own = FeatureSet(own_data_file)
        lukas = FeatureSet(lukas_file)
        alps = FeatureSet(alps_file)

    all_sets = [own, lukas, alps]

    temperatures = [np.round(s.ulabel, 1) for s in all_sets]
    temperatures = np.intersect1d(temperatures[0], np.intersect1d(temperatures[1], temperatures[2]))

    for features, select_t in zip(all_sets, [[1, 1.5, 2.12, 3.5],
                                             [1, 1.51, 2.12, 3.49],
                                             [1, 1.5, 2.11, 3.5]]):
        view = Magnetization1D(features)
        view.save_current_to_plot_stream()

        grouped = [(t, g[:1000, :]) for t, g in features.groupby('T') if np.round(t, 2) in select_t]
        temperatures, data = zip(*grouped)

        trajectories = Magnetization1D(list(data), labels=["{0:.2f}".format(t) for t in temperatures],
                                       mode='trajectory')
        trajectories.save_current_to_plot_stream()


    pass