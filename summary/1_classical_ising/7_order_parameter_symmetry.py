import pandas
import matplotlib.pyplot as plt

from core.analysis import Batcher
from analyzers.site_shuffling import DataShufflingFigure
from features import FeatureSet

if __name__ == '__main__':
    data = FeatureSet('ising_classical/alps_train.h5').subsample(10000)

    parameters = Batcher.grid_search(shuffle_mode=["no shuffle", "random site selection",
                                                   "random spin selection", "full"],
                                     select_sites=[20, 50, 100, 256])
    parameters = pandas.concat([parameters]*20)

    store = '/home/ben/masterphase/data/analysis/1_Classical_Ising/1.7_order_parameter_symmetry1.h5'
    analysis = DataShufflingFigure(feature_set=data,
                                   initial_data=parameters,
                                   store=store)
    analysis.run_all()

    plt.show()
