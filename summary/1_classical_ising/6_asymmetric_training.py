from analyzers.asymmetric_training import AsymmetricTrainingFigure
from core.analysis import Batcher
from features import FeatureSet
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


if __name__ == '__main__':
    data = FeatureSet('ising_classical/alps_train.h5').subsample(10000)

    store = '/home/ben/masterphase/data/analysis/1_Classical_Ising/1.6_asymmetric_training.h5'
    analysis = AsymmetricTrainingFigure(feature_set=data,
                                        threshold=2.269,
                                        store=store,
                                        )
    if analysis.empty:
        parameters = Batcher.grid_search(test_begin=np.linspace(1.1, 2.2, 12), test_end=np.linspace(2.3, 3.4, 12))
        # repetitions
        parameters = pd.concat([parameters]*20)

        analysis.set_data(parameters)

    analysis.run_all()
    plt.show()
