from features import FeatureSet
import pandas as pd
from analyzers.architecture import DenseNetworkFigure
from matplotlib import pyplot as plt

if __name__ == '__main__':
    data = FeatureSet('ising_classical/alps_train.h5').subsample(10000)

    # set up experiment parameters
    sweeps = {
        'neurons_single': ((2,), (10,), (100,), (1000,)),
        'multilayer_balanced': ((5, 5), (10, 10), (100, 100)),
        'multilayer_funnel': ((50, 10), (100, 10), (100, 20)),
        'deep': ((100, 50, 10), (200, 100, 50, 10), (500, 200, 100, 50, 10), (1000, 500, 200, 100, 50, 10)),
        'deep2': ((1000,), (800, 200), (500, 300, 200), (400, 300, 200, 100), (350, 250, 150, 100, 80, 50, 20)),
        'deep3': ((100,), (80, 20), (50, 30, 20), (40, 30, 20, 10)),
        'deep4': ((50,), (30, 20), (20, 15, 10, 5))
    }
    parameters = [{'sweep': sweep, 'num_neurons': neurons} for sweep, v in sweeps.items() for neurons in v]*20
    parameters = pd.DataFrame(parameters).set_index([k for k in parameters[0].keys()])

    store = '/home/ben/masterphase/data/analysis/1_Classical_Ising/1.4deep.h5'
    analysis = DenseNetworkFigure(feature_set=data, threshold=2.269,
                                  initial_data=None, store=store)

    analysis.run_all()
    plt.show()

