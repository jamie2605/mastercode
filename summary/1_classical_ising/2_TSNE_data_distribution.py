from features import FeatureSet
from visualization.features import TSNEView

if __name__ == '__main__':
    own = FeatureSet('ising_classical/own.h5')
    lukas = FeatureSet('ising_classical/lukas.h5')
    alps = FeatureSet('ising_classical/alps_train.h5')

    n = 3000
    for features, name in zip([own.subsample(n), lukas.subsample(n), alps.subsample(n)],
                              ['Own', 'Kades', 'Alps']):
        view = TSNEView(features, display_feature_names=False, pre_pca_dim=8,
                        figure_title="{} MCMC".format(name))
        view.save_current_to_plot_stream(name_suffix=name)
        pass

    pass