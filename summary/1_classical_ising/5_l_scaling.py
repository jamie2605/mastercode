import multiprocessing
from multiprocessing.pool import Pool

from core.analysis import Batcher
from analyzers.l_scaling import LScalingFigure
from features import FeatureSet
from features.feature_factories_ml import Classical2DIsingFactory
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from visualization.features import Magnetization1DScalingClassicalIsing


def get_feature_set(system_length, file_path):
    own = Classical2DIsingFactory(system_length=system_length, temperatures=np.linspace(1, 3.5, 50))
    feature_set = own.create_feature_set()
    feature_set.save(file_path)

    return feature_set


def get_all_feature_sets(system_lengths, regenerate=False, file_name='ising_classical/own_{}.h5'):
    if regenerate:
        with Pool(processes=multiprocessing.cpu_count()) as pool:
            res = {l: pool.apply_async(get_feature_set, (l, file_name.format(l))) for l in system_lengths}
            feature_sets = {l: r.get() for l, r in res.items()}
    else:
        feature_sets = {system_length: FeatureSet(file_name.format(system_length)) for system_length in system_lengths}

    return feature_sets


if __name__ == '__main__':
    lengths = [3, 6, 8, 12, 16]

    features = get_all_feature_sets(lengths)

    # split features into train, test data
    data = {}
    for length in lengths:
        train, test = features[length].split_label_value(train_values={'T': (1, 3.5)}, test_values={'T': (1.5, 3)})
        data[length ** 2] = {"train": train, "test": test}

    # prepare and perform analysis
    parameters = Batcher.grid_search(system_size=data.keys(), train_epochs=(1,))
    parameters = pd.concat([parameters] * 10)

    store = '/home/ben/masterphase/data/analysis/1_Classical_Ising/1.5l_scaling.h5'
    analysis = LScalingFigure(feature_sets=data,
                              store=store, # initial_data=parameters,
                              )

    analysis.run_all()

    magnetizations = Magnetization1DScalingClassicalIsing(features)
    plt.show()
