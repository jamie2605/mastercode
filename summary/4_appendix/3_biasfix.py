import numpy as np
import pandas as pd

from analyzers.network_response_basics import BiasLessFigure
from core import Batcher
from features import FeatureSet

if __name__ == '__main__':

    # Feature set generation
    regenerate = True

    n = 700
    feature_file = "ising_quantum/mps/ml/{}particles100hx.h5".format(n)

    feature_set = FeatureSet(feature_file)

    store = "/home/ben/masterphase/data/analysis/4_appendix/biasless.h5"
    analysis = BiasLessFigure(feature_set=feature_set, store=store)

    if analysis.empty:
        analysis.set_data(Batcher.grid_search(repetition=np.arange(10)))

    analysis.run_all()
    pass

