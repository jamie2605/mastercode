from analyzers.phase_detection_2d import Weak4PhaseDetection2D, Weak4PhaseDetection2DRGB, Weak4PhaseDetection2DEdges
from core import Batcher
from features import FeatureSet
from neural_networks import DenseModel

if __name__ == '__main__':
    features = FeatureSet("/home/ben/masterphase/data/feature_sets/xxz/mps/50spins.h5")
    num_neurons = (100,)
    nn = DenseModel(features.n_features, {'num_neurons': num_neurons,
                                          'num_classes': 4})

    store = "/home/ben/masterphase/data/analysis/3_XXZ/weakly_supervised{}_4moving.h5".format(num_neurons)

    kwargs = {"features": features, "neural_network": nn, "store": store}
    analyses = [Weak4PhaseDetection2D(**kwargs),
                Weak4PhaseDetection2DEdges(**kwargs, colored_edges=True),
                Weak4PhaseDetection2DEdges(**kwargs, colored_edges=False),
                Weak4PhaseDetection2DRGB(**kwargs)]

    for a, analysis in enumerate(analyses):
        patches = []
        for i in range(10):
            patches.append(((0.5, 1, 2-i/4, 2.5-i/4),
                            (0.5, 1, -2.5, -2),
                            (0.5, 1, -0.25, 0.25),
                            (5, 5.5, -0.5, 0)))

        for i in range(10):
            patches.append(((0.5, 1, 1+i/4, 1.5+i/4),
                            (0.5, 1, -2.5, -2),
                            (0.5, 1, -0.25+i/4, 0.25+i/4),
                            (5, 5.5, -0.5, 0)))

        for i in range(10):
            patches.append(((0.5, 1, 2, 2.5),
                            (0.5+i/2, 1+i/2, -2.5, -2),
                            (0.5, 1, -0.25, 0.25),
                            (5, 5.5, -0.5, 0)))

        for i in range(10):
            patches.append(((0.5, 1, 2, 2.5),
                            (0.5, 1, -2.5, -2),
                            (0.5, 1, -0.25, 0.25),
                            (5-i/2, 5.5-i/2, -0.5, 0)))

        if not analysis.parameters_set:

            parameters = Batcher.grid_search(train_epochs=[10],
                                             train_patches=patches
                                             )
            analysis.set_data(parameters)

        analysis.run_all()
        if a > 0:
            analysis.ax.set_ylabel("$\Delta$")
            analysis.ax.set_xlabel("$h_x$")

        for i, p in enumerate(patches):
            if i in [0, 4, 20, 30, 33]:
                analysis.save_current_to_plot_stream(name_suffix="{}".format(i))
            analysis.press("tab")


    pass
