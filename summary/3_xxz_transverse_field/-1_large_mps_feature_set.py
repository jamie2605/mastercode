from features.feature_factories_ml import XXZMPSCorrelationFactory
from visualization.features import CorrelationOrderings2D
import numpy as np


system_size = 50
mps_factory = XXZMPSCorrelationFactory(h_x=np.linspace(0, 6, 50),
                                       delta=np.linspace(-3, 3, 50),
                                       system_size=system_size)

feature_path = "/home/ben/masterphase/data/feature_sets/xxz/mps/{}spins.h5".format(system_size)

feature_set_mps = mps_factory.create_feature_set()
feature_set_mps.save(feature_path)
fig = CorrelationOrderings2D.from_outputs(mps_factory.read_outputs())
pass
