from features.feature_factories_ml import XXZMPSCorrelationFactory
import numpy as np

from visualization.features import CorrelationMatrices2D

if __name__ == '__main__':

    figures = {}
    for system_size in [10, 20, 30, 40, 50]:
        mps_factory = XXZMPSCorrelationFactory(h_x=np.linspace(0, 5, 40),
                                               delta=np.linspace(-2, 2, 20),
                                               system_size=system_size)

        outputs = mps_factory.read_outputs()
        figures[system_size] = CorrelationMatrices2D.from_outputs(outputs)

    pass
