from analyzers.phase_detection_2d import ConfusionPhaseDiagram2D
from features import FeatureSet
from visualization.features import SpinConfigurationOrderings2D

if __name__ == '__main__':

    feature_set = FeatureSet("/home/ben/masterphase/data/feature_sets/xxz/exact/8spins.h5")
    figure = SpinConfigurationOrderings2D(spin_configurations=feature_set)

    ConfusionPhaseDiagram2D()
    pass
