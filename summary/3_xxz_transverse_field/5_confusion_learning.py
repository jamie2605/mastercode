from analyzers.phase_detection_2d import ConfusionPhaseDiagram2D
from core import Batcher
from features import FeatureSet
import numpy as np
import pandas as pd
from matplotlib import rc

from neural_networks import DenseModel

if __name__ == '__main__':

    feature_set = FeatureSet("/home/ben/masterphase/data/feature_sets/xxz/mps/50spins.h5")
    store = "/home/ben/masterphase/data/analysis/3_XXZ/confusioncorrectmoppnormalization.h5"
    neural_network = DenseModel(feature_set.n_features, {'num_neurons': (100,)})

    analysis = ConfusionPhaseDiagram2D(store=store, features=feature_set,
                                       neural_network=neural_network)

    if not analysis.parameters_set:
        h_x = feature_set.labels["h_x"].unique()
        delta = feature_set.labels["delta"].unique()
        parameters = Batcher.grid_search(h_x=h_x[5:-5],
                                         delta=delta[5:-5],
                                         window_width=[0.5, 1.0, 2],
                                         scan_direction=['h_x', 'delta'])
        parameter = pd.concat([parameters]*10)
        analysis.set_data(parameters)

    analysis.run_all(update_plot_interval=10)
    analysis.ax.set_xlabel('$h_x$')
    analysis.ax.set_ylabel('$\Delta$')
    analysis.ax.set_title("")
    analysis.fig.set_size_inches(3.5, 2.5)
    analysis.fig.tight_layout()
    analysis.save_current_to_plot_stream()

    pass
