from features import FeatureSet
from features.feature_factories_ml import XXZSpinConfigurationExactDiagonalizationFactory, XXZMPSCorrelationFactory
from visualization.features import SpinConfigurationOrderings2D, CorrelationOrderings2D
import numpy as np

if __name__ == '__main__':

    figures = []
    regenerate = False

    for cloud_sigma, blockade_r, J in [(5, 0.8, 1), ([30, 30, 235], 5, 60000)]:
        model_parameters = {
            'system_size': 10,
            'ordered': False,
            'dimensionality': 3,
            'r_scaling': -6,
            'cloud_sigma': cloud_sigma,
            'blockade_r': blockade_r,
            'J': J

        }

        factory = XXZSpinConfigurationExactDiagonalizationFactory(h_x=np.linspace(0, 5, 40),
                                                                  delta=np.linspace(-2, 2, 20),
                                                                  **model_parameters)

        file_path = "/home/ben/masterphase/data/feature_sets/xxz/exact/{system_size}spins" \
                    "{ordered}_{cloud_sigma}_{blockade_r}_{J}.h5".format(**model_parameters)
        if regenerate:
            feature_set = factory.create_feature_set()
            feature_set.save(file_path)
        else:
            feature_set = FeatureSet(file_path)

        figures.append(SpinConfigurationOrderings2D(spin_configurations=feature_set))

    # MPS generation
    for system_size in [10, 20, 30, 40, 50]:
        mps_factory = XXZMPSCorrelationFactory(h_x=np.linspace(0, 5, 40),
                                               delta=np.linspace(-2, 2, 20),
                                               system_size=system_size)

        feature_path = "/home/ben/masterphase/data/feature_sets/xxz/mps/{}spins.h5".format(system_size)

        if regenerate:
            feature_set_mps = mps_factory.create_feature_set()
            feature_set_mps.save(feature_path)
        else:
            feature_set = FeatureSet(feature_path)

        figures.append(CorrelationOrderings2D.from_outputs(mps_factory.read_outputs()))
    pass
