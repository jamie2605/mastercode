import functools
import os
from math import floor, ceil
from typing import Union, List, Iterator, Dict
import numpy as np
import pandas

from pandas import DataFrame
import pandas as pd
import warnings

prepend_path = '/home/ben/masterphase/data/feature_sets/'


class FeatureSet:
    """
    class to store n_features with multidimensional label
    """

    def __init__(self, filepath: str = None,
                 features: DataFrame = None, labels: DataFrame = None,
                 meta: dict = None):
        """

        :param features: DataFrame with data being n_features
        :param labels: Dataframe, column header is label name, index must match n_features index
        :param filepath: path to load FeatureSet from
        """
        if filepath is not None:
            assert features is None and labels is None
            self.load(filepath)
            return

        assert (features.index.equals(labels.index))

        if not hasattr(features, 'name'):
            features.name = 'features'

        if meta is None:
            meta = dict()

        self._features = features
        self._labels = labels
        self.meta = meta

    def load(self, path):
        if not os.path.isabs(path):
            path = os.path.join(prepend_path, path)
        self._features = pd.read_hdf(path, 'features')
        self._labels = pd.read_hdf(path, 'labels')
        meta = pd.read_hdf(path, 'meta').to_dict()

        # unwrap if dataframe became 2D
        if '_content' in meta.keys():
            meta = meta['_content']

        self.meta = meta

        if not hasattr(self._features, 'name'):
            self._features.name = 'features'

    def save(self, path):
        if not os.path.isabs(path):
            path = os.path.join(prepend_path, path)
        self._features.to_hdf(path, 'features', mode='w')
        self._labels.to_hdf(path, 'labels')
        # gives performance warning which we ignore
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            # content is trick for easier re-conversion to dict
            pd.DataFrame.from_dict({'_content': self.meta}).to_hdf(path, 'meta')

        return self

    @property
    def features(self):
        return self._features

    @property
    def x(self):
        return self._features.values

    @property
    def labels(self):
        return self._labels

    @property
    def unified(self):
        """

        :return: unified data frame with features as columns as labels as indices
        """
        return pd.concat([self.features, self.labels], axis=1).set_index(list(self.labels.columns))

    @property
    def ulabel(self, values_1D=True):
        labels = self._labels.drop_duplicates()
        if values_1D and len(self.labels.columns) == 1:
            labels = labels.values.flatten()
        return labels

    def groupby(self, label, **kwargs) -> Iterator['FeatureSet']:
        """
        return iterator over feature set that yields Feature set for every value of the column
        :param label: the label name to group by
        :return:
        """
        for value, g in self.labels.groupby(label, **kwargs):
            yield value, self[g[label].index.values, :]

    def __len__(self):
        return self._features.__len__()

    def __repr__(self):
        if hasattr(self._features, 'name'):
            name = self._features.name
        else:
            name = ''
        template = "{} points x {} {}\n{} different label sets (labels: {})\nMeta info: {}"
        description = template.format(len(self), len(self._features.keys()), name,
                                      len(self.ulabel), str(self._labels.keys().values), self.meta)

        return description

    def __getitem__(self, item):
        """
        Returns a feature set using the format
        feature_set[slice of feature points, list of n_features to use, list of labels to use]
        :param item: tuple in the format specified above
        :return: a Feature set
        """
        if isinstance(item, pandas.Series):
            return FeatureSet(features=self._features.loc[item],
                              labels=self._labels.loc[item],
                              meta=self.meta)

        points_i = item[0]
        features = item[1]
        if len(item) > 2:
            labels = item[2]
        else:
            labels = self._labels.keys()

        if isinstance(features, (slice, int, np.ndarray)) or isinstance(features[0], int):
            feature_i = features
        else:
            feature_i = [self._features.columns.get_loc(f) for f in features]
        if isinstance(labels, (slice, int, np.ndarray)) or isinstance(labels[0], int):
            if isinstance(labels, int):
                label_i = [labels]
            else:
                label_i = labels
        else:
            label_i = [self._labels.columns.get_loc(l) for l in labels]

        return FeatureSet(features=self._features.iloc[points_i, feature_i].reset_index(drop=True),
                          labels=self._labels.iloc[points_i, label_i].reset_index(drop=True),
                          meta=self.meta)

    def concat(self, feature_set):
        if isinstance(feature_set, FeatureSet):
            feature_set = [feature_set]

        return FeatureSet(
            features=pd.concat([self._features] + [f._features for f in feature_set]).reset_index(drop=True),
            labels=pd.concat([self._labels] + [f._labels for f in feature_set]).reset_index(drop=True),
            meta=self.meta)

    def split_by_proportion(self, train_p: float):
        """
        :param train_p: What proportion of data to use for training
        :return: tuple (train, test) of feature sets
        """
        select = np.arange(len(self))
        np.random.shuffle(select)
        last_train_i = int(train_p * len(select))

        return self[select[:last_train_i], :], self[select[last_train_i:], :]

    def split_by_homogeneous_proportion(self, train_n: int):
        """
        select train data such that the number of samples per temperature is equal
        :param train_n:
        :return:
        """
        assert train_n < len(self)

        n_label = len(self.ulabel)
        per_label = floor(train_n / n_label)

        # split remaining label values evenly
        remainder = train_n - per_label * n_label
        t_add = self.ulabel[np.array(np.linspace(0, n_label-1, remainder), dtype=int)]

        def sample(df):
            n_sample = per_label + int(df.iloc[0, 0] in t_add)
            return df.sample(n_sample, replace=True)

        select_train = self.labels.groupby(self.labels.columns[0]).apply(sample)
        select_train = select_train.set_index(select_train.index.droplevel(0)).index

        select_test = ~self.labels.index.isin(select_train)

        return self[select_train, :], self[select_test, :]




    def split_train_test_by_train_label(self, train_values: Union[DataFrame, dict]):
        """

        :param train_values: DataFrame describing training points
        :return: tuple (train, test) of feature sets for training and testing
        """
        assert (all(train_values.keys() == self._labels.keys()))

        if isinstance(train_values, dict):
            raise NotImplementedError
            # train_values = Batcher.grid_search(**train_values)

        select_train = ~self.labels[self._labels.isin(train_values)].isnull().any(axis=1)
        return self[select_train, :], self[~select_train, :]

    def split_inner_hyper_rectangle(self, **edges):
        """
        Return train and test set by specifying edges of hyper rectangle in parameter space
        the edges of the hyper rectangle are the train points (unless edge with is zero,
        then only test points along this line)

        :param edges: column=int, float or 2 tuple of these specifying the
        :return: FeaturesSet train matching all points in train_values
                 FeatureSet test matching all points between train_values
        """
        select_patch = self.select_patch(**edges)

        # select window edge as training points
        select_edge = np.zeros(select_patch.shape, dtype=bool)
        scan_window_labels = self.labels.iloc[select_patch]
        for k in scan_window_labels.columns:
            upper = np.max(scan_window_labels[k])
            lower = np.min(scan_window_labels[k])

            if upper != lower:
                # add new edges to selected training points
                select_edge = select_edge | ((self.labels[k] == upper) | (self.labels[k] == lower)).values
        select_edge = select_patch & select_edge

        select_train = select_edge
        select_test = select_patch & np.logical_not(select_train)

        # make sure train window was not too narrow and datapoints exist
        assert sum(select_train) > 0 and sum(select_test) > 0

        return self[select_train, :], self[select_test, :]

    def select_patch(self, **edges):
        """
        Return train and test set by specifying edges of hyper rectangle in parameter space
        the edges of the hyper rectangle are the train points (unless edge with is zero,
        then only test points along this line)

        :param edges: column=int, float or 2 tuple of these specifying the
        :return: FeaturesSet train matching all points in train_values
                 FeatureSet test matching all points between train_values
        """

        # make sure all labels exist and their values are specified
        assert set(self.labels.columns) == set(edges.keys())
        for k in edges.keys():
            if isinstance(edges[k], int) or isinstance(edges[k], float):
                edges[k] = (edges[k], edges[k])
            else:
                assert isinstance(edges[k], tuple) and len(edges[k]) == 2
        select_patch = np.zeros((len(self.labels), len(edges.keys())), dtype=bool)
        for i, k in enumerate(edges.keys()):
            vals = self.labels[k].values
            select_patch[:, i] = np.logical_and(vals >= edges[k][0], vals <= edges[k][1])
        select_patch = np.all(select_patch, axis=1)

        assert sum(select_patch) > 0

        return select_patch

    def split_label_value(self,
                          train_values: dict = None,
                          test_values: dict = None,
                          exclude_test_from_train: bool = True,
                          exclude_train_from_test: bool = False):
        """
        Select based on label values
        :param train_values: dict with label names as keys, Tuple (min, max) as values
        :param test_values: dict with label names as keys, Tuple (min, max) as values
        :param exclude_test_from_train:
        :param exclude_train_from_test:
        :return:
        """
        assert not (exclude_test_from_train and exclude_train_from_test)

        assert not (train_values is None and test_values is None)
        if train_values is not None and test_values is not None:
            assert train_values.keys() == test_values.keys()

        if train_values is not None:
            assert all([k in self.labels.keys() for k in train_values.keys()])
        elif test_values is not None:
            assert all([k in self.labels.keys() for k in test_values.keys()])

        select = []
        for cond in [train_values, test_values]:
            if cond is None:
                select.append(pd.Series(data=True, index=self.labels.index))
            else:
                conditions = [self.labels[k].between(v[0], v[1]) for k, v in cond.items()]
                select.append(functools.reduce(lambda x, y: x & y, conditions))

        select_train, select_test = select

        if exclude_test_from_train:
            select_train.loc[select_test] = False

        if exclude_train_from_test:
            select_test.loc[select_train] = False

        return self[select_train], self[select_test]

    @property
    def n_features(self):
        return len(self._features.keys())

    def binary_label(self, threshold, column: str = None):
        return np.argmax(self.binary_single_2d_label(threshold, column), axis=1)

    def binary_single_2d_label(self, threshold, column: str =None):
        # bring labels in format that tensorflow likes

        if column is None:
            assert len(self._labels.columns) == 1
            column = self._labels.columns[0]

        phase = self._labels[column] > threshold

        labels = np.zeros((len(self._labels), 2))
        labels[phase == 0, 0] = 1
        labels[phase == 1, 1] = 1

        return labels

    def evaluate_along_label(self, y_pred: np.ndarray, threshold, column: str = None):
        if column is None:
            assert len(self._labels.columns) == 1
            column = self._labels.columns[0]

        if np.ndim(y_pred) > 1:
            y_pred = np.argmax(y_pred, axis=1)

        y_true = self.binary_label(threshold, column)

        results = {'accuracy': y_pred == y_true,
                   'y_pred': y_pred}
        correct = pd.DataFrame(results, index=self.labels[column])
        accuracies = correct[["accuracy"]].groupby(level=0).mean()
        accuracies["std"] = correct["y_pred"].groupby(level=0).std()

        return accuracies

    def subsample(self, sample_size, axis=0) -> 'FeatureSet':
        """
        Return a Feature set with only some datapoints selected
        :param axis: 0 too subsample feature points, 1 to select random features
        :param sample_size: if > 1: number of samples, if < 1 proportion of data
        :return: subsampled FeatureSet
        """
        assert axis in [0, 1]
        if axis == 0:
            n = len(self)
        else:
            n = self.n_features

        assert sample_size > 0
        if sample_size <= 1:
            sample_size = int(n * sample_size)

        assert isinstance(sample_size, int)
        assert sample_size <= n

        i = np.random.choice(np.arange(n), size=sample_size, replace=False)

        if axis == 0:
            return self[i, :]
        else:
            return self[:, self.features.columns[i]]

    def shuffle(self, mode=1) -> 'FeatureSet':
        """
        Shuffle data points, features, or both
        :param mode:    0: shuffle points,
                        1: shuffle features,
                        'row-wise': shuffle sites of each measurement individually
                        'all': shuffle flattened array of features
        :return: shuffled FeatureSet
        """
        assert mode in [0, 1, 'row-wise', 'full']
        if mode in [0, 1]:
            return self.subsample(1, axis=mode)
        elif mode == 'row-wise':
            # apply with random shuffle is too slow, use permutations
            d = self.n_features
            new_order = np.vstack([np.random.permutation(np.arange(i*d, (i+1)*d)) for i in range(len(self))])
            new_features = self.features.values.flatten()[new_order]
            new_features = pd.DataFrame(new_features, index=self.labels.index)
            return FeatureSet(features=new_features,
                              labels=self.labels,
                              meta=self.meta)
        elif mode == 'full':
            if len(self.labels.columns) > 1:
                raise NotImplementedError

            groups = []
            labels = []
            for label, g in self.unified.groupby(level=0):
                group = g.values
                np.random.shuffle(group.flat)
                groups.append(group)
                labels.append(self.labels.loc[self.labels.iloc[:, 0] == label].values[:, 0])
            groups = np.vstack(groups)
            labels = np.hstack(labels).transpose()
            return FeatureSet(features=pd.DataFrame(groups, columns=self.features.columns),
                              labels=pd.DataFrame(labels, columns=self.labels.columns),
                              meta=self.meta)
        else:
            raise ValueError("Value {} is invalid for parameter mode".format(mode))

    def add_noise(self, add_relative_gaussian: float = None,
                  add_absolute_gaussian: float = None):

        features = self.features.values.copy()

        if add_relative_gaussian is not None:
            features += features * np.random.normal(scale=add_relative_gaussian, size=features.shape)

        if add_absolute_gaussian is not None:
            features += np.random.normal(scale=add_absolute_gaussian, size=features.shape)

        meta = {**self.meta, "noise": {"relative": add_relative_gaussian,
                                       "absolute": add_absolute_gaussian}}

        features = pd.DataFrame(features, columns=self.features.columns, index=self.features.index)

        return FeatureSet(features=features, labels=self.labels, meta=meta)


# noinspection PyTypeChecker
def concatenate(feature_sets: List[FeatureSet]) -> FeatureSet:
    return FeatureSet(features=pd.concat([fs.features for fs in feature_sets]),
                      labels=pd.concat([fs.features for fs in feature_sets]),
                      meta=feature_sets[0].meta)
