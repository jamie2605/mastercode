import csv
import os
import re
from datetime import datetime
from abc import ABC, abstractmethod, ABCMeta
from typing import List, Tuple
import multiprocessing

import numpy as np
import pandas as pd

from modified_libs import mps, qutip

from physics import models_qutip
from physics.models_mps import TFIMPSModel, XXZMPSModel, MPSModel
from tools.ClassicalIsingSimulator import Classical2DIsingSimulator
from tools.helper import bitwise_boolean
from .feature_set import FeatureSet


class FeatureSetFactory(ABC):
    def __init__(self, common_parameters: dict,
                 sweep_parameters: pd.DataFrame,
                 meta: dict = None):
        """
        Class to streamline simulation to find groundstates and fetch_sampling_results feature sets from them
        :param common_parameters: fixed model sweeps
        :param sweep_parameters: sweeped model sweeps (these become the labels)
        :param meta: Additional information about the model
        """

        self.common_parameters = common_parameters
        self.sweep_parameters = sweep_parameters

        if meta is None:
            meta = {}
        self.meta = meta

    @property
    @abstractmethod
    def feature_name(self):
        # :param feature_name: general name for feature names (microscopic spin configuration, ...)
        pass

    @property
    @abstractmethod
    def feature_names(self):
        # :param feature_names: list of names for each feature (such [\sigma^x_1, ...])
        pass

    @property
    def label_names(self):
        return self.sweep_parameters.keys()

    def create_feature_set(self):
        return self._simulate_all()

    def _simulate_all(self):
        all_parameters = [{**self.common_parameters, **p.to_dict()} for i, p in self.sweep_parameters.iterrows()]
        fs = self.simulate(all_parameters)
        fs.meta = {**self.meta, 'common_parameters': self.common_parameters}
        fs.features.name = self.feature_name

        # if self.cache is not None:
        #     fs = fs.concat(self.cache)

        return fs

    @abstractmethod
    def simulate(self, parameters: List[dict]) -> FeatureSet:
        pass


class Classical2DIsingFactory(FeatureSetFactory):
    def __init__(self, system_length=16, temperatures=np.linspace(1, 3.5, 25),
                 n_points=1000, burn_in=500000, skip_samples=1000):
        self.system_length = system_length
        self.n_points = n_points
        meta = {"model": "Classical Ising model",
                "method": "MCMC (own)"}

        common_parameters = {'J': 1, 'burn_in': burn_in, 'skip_samples': skip_samples}
        sweep_parameters = pd.DataFrame.from_dict({'T': temperatures})

        super().__init__(common_parameters, sweep_parameters, meta)

    @property
    def feature_name(self):
        return 'Classical spins'

    @property
    def feature_names(self):
        return ['S_{{{}, {}}}'.format(i, j) for i in range(self.system_length) for j in range(self.system_length)]

    def simulate(self, parameters: List[dict]) -> FeatureSet:

        n_total = len(parameters) * self.n_points
        spin_configs = np.zeros((n_total, self.system_length ** 2))
        labels = np.zeros(n_total)

        for i, p in enumerate(parameters):
            print("Simulating for {}".format(p))
            sim = Classical2DIsingSimulator(system_size=self.system_length, temperature=p['T'], interaction=p['J'])

            # burn in
            print("Burning in")
            sim.step(p['burn_in'])

            rows = slice(i * self.n_points, (i+1) * self.n_points)
            print("sampling")
            spin_configs[rows, :] = np.array([l.flatten() for l in sim.sample(self.n_points, p['skip_samples'])])
            labels[rows] = p['T']

        return FeatureSet(features=pd.DataFrame(spin_configs, columns=self.feature_names),
                          labels=pd.DataFrame.from_dict({'T': labels}))


class ExactDiagonalizationFeatureFactory(FeatureSetFactory):
    def __init__(self, *args, **kwargs):
        self.n_points = kwargs.pop("n_points", 1000)
        super().__init__(*args, **kwargs)

    def diagonalize(self, all_parameters):
        pool = multiprocessing.Pool(multiprocessing.cpu_count())
        results = []
        for i, p in enumerate(all_parameters):
            print("{} Diagonalizing {}/{} on GPU ({})".format(datetime.now(), i + 1,
                                                              len(all_parameters),
                                                              self.sweep_parameters.iloc[i].to_dict()))
            ham = self.hamiltonian(p)
            ground_state = self.groundstate(ham, gpu=True)

            results.append(pool.apply_async(self.sample_single, (ground_state, self.n_points)))
        return results

    @staticmethod
    def groundstate(hamiltonian, gpu=False) -> List[qutip.GpuQobj]:
        E, psi = hamiltonian.eigenstates(gpu=gpu, progress=True)
        if E[1] - E[0] < 1e-10:
            # degenerate ground state
            print("degenerate ground state, returning first 2 eigenstates as groundstates")
            ground = [psi[0], psi[1]]
        else:
            ground = [psi[0]]
        return ground

    def simulate(self, all_parameters):
        print("Diagonalizing on GPU, sampling on CPU in parallel")
        ground_states_async = self.diagonalize(all_parameters)

        print("Finishing sampling, fetching results")
        configs = self.fetch_sampling_results(ground_states_async)

        print("Converting data to feature set")
        features = pd.DataFrame(configs.reshape((-1, self.n_features)),
                                columns=self.feature_names)
        labels = pd.DataFrame(self.sweep_parameters.as_matrix().repeat(self.n_points, 0), columns=self.label_names)

        return FeatureSet(features=features, labels=labels)

    def fetch_sampling_results(self, results):
        """
        Sample on CPU while GPU diagonalizes
        :param results: result objects with diagonalized hamiltonians
        :return:
        """
        configs = np.zeros((len(results),
                            self.n_points,
                            self.n_features))

        remaining = {i: r for i, r in enumerate(results)}
        while True:
            completed_now = []
            for i, r in remaining.items():
                if r.ready:
                    configs[i, ...] = r.get()
                    completed_now.append(i)

            if len(completed_now) > 0:
                for i in completed_now:
                    remaining.pop(i)
                print("{} Completed sampling for {}/{}".format(datetime.now(),
                                                               len(results) - len(remaining),
                                                               len(results)))
            if all([r.ready for r in remaining.values()]):
                break

        return configs

    @abstractmethod
    def sample_single(self, ground, n_points) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def n_features(self):
        pass

    @abstractmethod
    def hamiltonian(self, parameters) -> qutip.GpuQobj:
        pass


class TFIExactDiagonalizationFactory(ExactDiagonalizationFeatureFactory):
    def __init__(self, *args, **kwargs):
        kwargs["meta"] = {"model": "Ising model with transverse field",
                          "method": "exact diagonalization"}
        super().__init__(*args, **kwargs)

    @abstractmethod
    def sample_single(self, ground, n_points) -> np.ndarray:
        pass

    def hamiltonian(self, parameters):
        return models_qutip.ising_hamiltonian(parameters['system_size'], parameters['h_x'], parameters['J'])


class TFISpinConfigurationExactDiagonalizationFactory(TFIExactDiagonalizationFactory):
    def __init__(self, system_size=10, h_x=np.linspace(0.1, 2.5, 25), n_points=1000):
        """
        Sample microscopic spin configurations \sigma_z  of the transverse field ising model
        (ordered, NN)
        based on exact diagonalization of the ground state

        :param system_size: number of spins in chain
        :param h_x: values for transverse field
        :param n_points: number of points to sample per h_x value
        """
        common_parameters = {'system_size': system_size,
                             'J': 1}
        sweep_parameters = pd.DataFrame(h_x, columns=['h_x'])
        super().__init__(sweep_parameters=sweep_parameters, common_parameters=common_parameters, n_points=n_points)

    @property
    def feature_name(self):
        return "Spin in z direction"

    @property
    def feature_names(self):
        return ["s^{}_{}".format("z", i) for i in range(self.n_features)]

    @property
    def n_features(self):
        return self.common_parameters["system_size"]

    def sample_single(self, ground, n_points):
        hilbert_dim = ground[0].data.get_shape()[0]
        system_size = len(ground[0].dims[0])
        configs = np.zeros((n_points, self.n_features))
        probabilities = np.zeros(hilbert_dim)

        # treat degenerate ground states
        for psi_0 in ground:
            probabilities += np.array(np.real(psi_0.data.todense())).flatten() ** 2

        probabilities = probabilities / len(ground)

        for i, config_i in enumerate(np.random.choice(hilbert_dim, size=n_points, p=probabilities)):
            configs[i, :] = [1 if b else -1 for b in bitwise_boolean(int(config_i), system_size)]

        return configs


class XXZExactDiagonalizationFactory(ExactDiagonalizationFeatureFactory, metaclass=ABCMeta):
    def __init__(self, n_points: int = 1000,
                 h_x=np.linspace(0, 5, 5), delta=np.linspace(-2, 2, 5), **model_parameters):

        model_parameters["system_size"] = model_parameters.pop("system_size", 10)
        model_parameters["J"] = model_parameters.pop("J", 1)
        model_parameters["dimensionality"] = model_parameters.pop("dimensionality", 3)
        model_parameters["r_scaling"] = model_parameters.pop("r_scaling", -3)

        model_parameters["ordered"] = model_parameters.pop("ordered", False)
        if model_parameters["ordered"]:
            assert "rscaling" not in model_parameters.keys()
            assert "cloud_sigma" not in model_parameters.keys()
        else:
            model_parameters["r_scaling"] = model_parameters.pop("r_scaling", -3)
            model_parameters["cloud_sigma"] = model_parameters.pop("cloud_sigma", -3)

        common_parameters = model_parameters
        data = np.vstack([d.flatten() for d in np.meshgrid(h_x, delta)]).transpose()
        sweep_parameters = pd.DataFrame(data, columns=['h_x', 'delta'])

        meta = {"model": "XXZ with transverse field",
                "method": "exact diagonalization"}

        super().__init__(sweep_parameters=sweep_parameters,
                         common_parameters=common_parameters,
                         n_points=n_points, meta=meta)

    def hamiltonian(self, parameters):
        return models_qutip.xxz_hamiltonian(parameters)


class XXZSpinConfigurationExactDiagonalizationFactory(XXZExactDiagonalizationFactory):
    """
    Sample microscopic spin configurations \sigma_z  of the XXZ model with a transverse field
    (ordered or unordered, NN or longrange interactions)
    based on exact diagonalization of the ground state
    """

    @property
    def feature_name(self):
        return "Spin degrees of freedom"

    @property
    def feature_names(self):
        return ["s^{}_{}".format(d, i) for i in range(int(self.n_features / 3)) for d in ["x", "y", "z"]]

    def sample_single(self, ground, n_points):
        ground = ground[0]
        n = len(ground.dims[0])
        hilbert_dim = 2 ** n
        configs = np.zeros((n_points, self.n_features))
        probabilities = np.zeros((3, hilbert_dim))
        for i in range(hilbert_dim):
            probabilities[:, i] = [np.abs((psi.dag() * ground).data.todense()[0, 0]) ** 2
                                   for psi in models_qutip.states(i, n)]
        for dim in range(3):
            for i, config_i in enumerate(np.random.choice(hilbert_dim, size=n_points, p=probabilities[dim, :])):
                configs[i, dim::3] = [1 if b else -1 for b in bitwise_boolean(int(config_i), n)]

        return configs

    @property
    def n_features(self):
        return self.common_parameters["system_size"] * 3


class MPSFeatureSetFactory(FeatureSetFactory, MPSModel, metaclass=ABCMeta):
    def __init__(self, model_parameters: dict, sweep_parameters: pd.DataFrame):
        common_parameters, self.mps_parameters, self.main_files = self.setup_model(model_parameters, sweep_parameters)

        super().__init__(common_parameters, pd.DataFrame(sweep_parameters))

        self.meta = {**self.meta,
                     "model_parameters": model_parameters,
                     "method": "MPS"}

    def setup_model(self, model_parameters, sweep_parameters):
        ham, operator, observable = self.model_definition(**model_parameters)
        common_parameters, running_mps_parameters = self.get_parameters(observable, model_parameters, sweep_parameters)
        mps_parameters = [{**common_parameters, **p} for p in running_mps_parameters]
        main_files = mps.WriteFiles(mps_parameters, operator, ham, PostProcess=False)

        return common_parameters, mps_parameters, main_files


class MPSCorrelationFeatureSetFactory(MPSFeatureSetFactory, metaclass=ABCMeta):
    @property
    def feature_name(self):
        return 'Correlation matrices'

    @property
    @abstractmethod
    def correlation_keys(self):
        pass

    def simulate(self, all_parameters) -> FeatureSet:
        dimensionality = len(self.correlation_keys)

        def reorder_nth(length: int, nth: int =3):
            assert (length / nth).is_integer()
            order = []
            for i in range(nth):
                order += list(range(i, length, nth))
            return order

        print("Preparing model, simulating if necessary, reading output")
        outputs = self.read_outputs()

        print("Creating feature set")
        n = self.mps_parameters[0]['L']
        cutoff = int(n / 2)
        features = np.hstack([get_all_feature_vectors([o[key] for o in outputs], cutoff)
                              for key in self.correlation_keys])
        features = features[:, reorder_nth(dimensionality * cutoff, cutoff)]
        features = pd.DataFrame(features, columns=self.feature_names)

        labels = self.sweep_parameters.iloc[np.repeat(self.sweep_parameters.index.values, cutoff)]
        labels.reset_index(drop=True, inplace=True)

        return FeatureSet(features=features, labels=labels)

    def read_outputs(self):
        outputs = mps.read_or_simulate(self.mps_parameters, self.main_files, parallel=9)
        return outputs


class XXZMPSCorrelationFactory(MPSCorrelationFeatureSetFactory, XXZMPSModel):
    def __init__(self, system_size=50,
                 h_x=np.linspace(0, 5, 22),
                 delta=np.linspace(-2.5, 2.5, 25)):
        """
        Create FeatureSet based on xx, yy, zz-correlations of the ordered XXZ-model with transverse field
        generated by a MPS Simulation
        If MPS Simulation output is not found, an MPS simulation will be launched

        :param system_size: number of spins
        :param h_x:
        :param delta:
        """

        sweep_parameters = pd.MultiIndex.from_product([h_x, delta], names=['h_x', 'delta'])
        sweep_parameters = pd.DataFrame(index=sweep_parameters).reset_index()

        super().__init__(model_parameters={'system_size': system_size}, sweep_parameters=sweep_parameters)

        self.meta["model"] = "Ordered XXZ with transverse field"

    @property
    def correlation_keys(self):
        return ['xx', 'yy', 'zz']

    @property
    def feature_names(self):
        system_size = self.mps_parameters[0]['L']
        cutoff = int(system_size / 2)
        return ["f^{}_{{{}}}".format(xi, i) for i in range(cutoff) for xi in ['x', 'y', 'z']]


class TFIMPSCorrelationFactory(MPSCorrelationFeatureSetFactory, TFIMPSModel):
    def __init__(self, model_parameters: dict, h_x=np.linspace(0, 2, 22)):
        """
        Create FeatureSet based on zz-correlations of the transverse-field ising model
        generated by a MPS Simulation
        If MPS Simulation output is not found, an MPS simulation will be launched

        :param model_parameters: See models_mps.ising_model_1d for keys + key system_size
        :param h_x: list of transverse field values to simulate
        """
        sweep_parameters = pd.DataFrame(h_x, columns=['h_x'])

        model_parameters["system_size"] = model_parameters.pop("system_size", 700)

        super().__init__(model_parameters=model_parameters, sweep_parameters=sweep_parameters)

        self.meta["model"] = "Ising with transverse field"

    @property
    def correlation_keys(self):
        return ['zz']

    @property
    def feature_names(self):
        system_size = self.mps_parameters[0]['L']
        cutoff = int(system_size / 2)
        return ["f^{}_{{{}}}".format('z', i) for i in range(cutoff)]


class SpinDataParser:
    """
    parse Spin Data obtained by ALPS/Lukas MC algorithm and parse to create a FeatureSet
    """

    def __init__(self, directory, subdirectories=True, skiprows=1):
        if subdirectories:
            self.test = self.load_data(os.path.join(directory, 'test'), skiprows=skiprows)
            self.train = self.load_data(os.path.join(directory, 'train'), skiprows=skiprows)
        else:
            self.data = self.load_data(directory, skiprows=skiprows)

    def load_data(self, directory, skiprows=1) -> FeatureSet:
        files = os.listdir(directory)
        pattern = re.compile('config_([0-9.]+).dat')

        # only use matched files
        files = [f for f in files if pattern.search(f) is not None]

        n = len(files)

        data = []
        temperatures = []

        # check that directories contain files
        assert len(files) > 0

        files = sorted(files)

        # test-open first file to check number of entries
        with open(os.path.join(directory, files[0])) as csvfile:
            read = csv.reader(csvfile, delimiter=' ')
            firstline = read.__next__()

        if firstline[-1] == '':
            firstline.pop()

        system_size = len(firstline) - 1
        system_length = int(system_size ** 0.5)

        for file, i in zip(files, range(n)):
            current_file = pd.read_csv(os.path.join(directory, file),
                                       names=range(system_size), sep=" ",
                                       dtype=int, usecols=range(1, system_size + 1),
                                       skiprows=lambda x: x % skiprows != 0)
            data.append(current_file)

            # assuming only files of the correct format exist within the directory
            beta = float(pattern.search(file).group(1))

            temperature = 1 / beta
            n_measurements = len(current_file)
            temperatures.append(temperature * np.ones(n_measurements))

            print("{}: Lines: {}".format(temperature, len(current_file)))

        data = pd.concat(data)
        data.columns = ['s^z_{{{},{}}}'.format(i, j) for i in range(system_length) for j in range(system_length)]
        data.reset_index(drop=True, inplace=True)
        temperatures = np.hstack(temperatures)

        return FeatureSet(features=pd.DataFrame(data),
                          labels=pd.DataFrame(temperatures, columns=['T']),
                          meta=self.meta)

    @property
    @abstractmethod
    def meta(self):
        pass


class LukasSpinDataParser(SpinDataParser):
    @property
    def meta(self):
        return {"origin": "Spin Data Sampled by MC library of Lukas Kades"}


class AlpsSpinDataParser(SpinDataParser):
    @property
    def meta(self):
        return {'origin': "Spin Data Sampled by ALPS library"}


'''
Helper functions for feature set generation
'''


def get_feature_vectors(correlation_matrix, cutoff=10):
    """
    return correlation functions up to a certain cutoff
    :param correlation_matrix:
    :param cutoff:
    :return:
    """
    system_size = np.shape(correlation_matrix)[0]

    return np.array([correlation_matrix.diagonal(r)[:system_size - cutoff] for r in range(1, cutoff + 1)]).transpose()


def get_all_feature_vectors(correlation_matrices, cutoff=10):
    return np.vstack([get_feature_vectors(z, cutoff) for z in correlation_matrices])
