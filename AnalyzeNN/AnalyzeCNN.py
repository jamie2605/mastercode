import matplotlib.pyplot as plt
import numpy as np
from NN.TensorFlowModels import ConvNN
import time


class AnalyzeCNN:
    def __init__(self):
        self.ui = AnalyzeCNN.create_window()

        self.ui["figure"].canvas.mpl_connect('button_press_event', self.click_callback)
        self.ui["figure"].draw()

        self._input_config = np.zeros((16,16))
        self.cnn = ConvNN(16)

        self.response_history = []

    def train(self, *args, **kwargs):
        self.cnn.train(*args, **kwargs)
        weights = self.cnn._sess.run(self.cnn.weights["W_conv1"])

        # normalize filters to total maximum
        m = np.max(np.abs(weights))
        for i in range(np.shape(weights)[-1]):
            self.ui["filter_im"][i].set_array(weights[:, :, 0, i]/m)

        self.predict()

    def set_config(self, im, draw=True, sleep=0):
        if len(np.shape(im)) == 1:
            im = im.reshape((16,16))
        self._input_config = im
        self.ui["input_image"].set_array(im)
        self.predict()
        if draw:
            self.ui["figure"].canvas.draw()
            time.sleep(sleep)

    def set_random_config(self):
        self.set_config(np.random.rand(*np.shape(self._input_config)))

    def predict(self):
        # use current spin config to predict on CNN model and train accordingly
        test = np.array([np.reshape(self._input_config, (-1, 256))])

        # run NN prediction
        y_pred = self.cnn.predict_classes(test)

        # save result
        self.response_history.append(y_pred[0])

        # update UI accordingly
        self.ui["response0"].set_height(y_pred[0][0])
        self.ui["response1"].set_height(y_pred[0][1])

        response = self.response_history
        n = len(response)
        self.ui["history_response0"].set_offsets([(i, r[0]) for i, r in zip(range(n), response)])
        self.ui["history_response1"].set_offsets([(i, r[1]) for i, r in zip(range(n), response)])

        self.rescale_responses()

        # view convolutions with weights and weights in last layer before summation
        h_conv = self.cnn.get_convolution(np.reshape(self._input_config, (1, -1)))
        w = self.cnn._sess.run(self.cnn.weights["W2"])
        b = self.cnn._sess.run(self.cnn.weights["b2"])
        h = np.reshape(h_conv, -1)
        ferro = h*w[:, 0]
        para = h*w[:, 1]

        for i in range(np.shape(h_conv)[-1]):
            self.ui["conv_im"][i].set_array(h_conv[0, :, :, i])

            self.ui["last_im_ferro"][i].set_array(np.reshape(ferro[i::4], (16,16)))
            self.ui["last_im_para"][i].set_array(np.reshape(para[i::4], (16, 16)))

    def rescale_responses(self):
        max = np.max(self.response_history)
        min = np.min(self.response_history)
        max = np.max((max, -min))

        self.ui["bar_ax"].set_ylim((-max, max))
        self.ui["history_ax"].set_ylim((-max,max))
        self.ui["history_ax"].set_xlim((0,len(self.response_history)))

    @staticmethod
    def create_window(size=(10,5), no_filters=4):
        ui = dict()
        #plt.rcParams['toolbar'] = 'None'
        ui["figure"] = plt.figure(figsize=size)
        ui["figure"].canvas.set_window_title("CNN Analyzer")

        def hide_axes(ax):
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)

        # positional variables
        input_size = (0, 0.55, 0.2, 0.4)

        # input spin configuration
        ax = ui["figure"].add_axes(input_size, title="Input Spin configuration")
        hide_axes(ax)
        ui["input_image"] = ax.imshow(np.empty((16,16))*np.nan, vmin=-1, vmax=1)
        ui["input_axes"] = ax

        # filters and convolutions
        ui["filter_im"] = []
        ui["conv_im"] = []
        ui["last_im_ferro"] = []
        ui["last_im_para"] = []

        for i in range(no_filters):
            filter_position = (input_size[0]+input_size[2]+0.05+0.1*i, 0.75, 0.1, 0.2)
            ax_f = ui["figure"].add_axes(filter_position)
            hide_axes(ax_f)
            ui["filter_im"].append(ax_f.imshow(np.empty((3,3))*np.nan, vmin=-1, vmax=1))


            conv_position = (input_size[0]+input_size[2]+0.05+0.1*i, 0.55, 0.1, 0.2)
            ax_c = ui["figure"].add_axes(conv_position)
            hide_axes(ax_c)
            ui["conv_im"].append(ax_c.imshow(np.empty((16,16))*np.nan, vmin=-1, vmax=1))

            ferro_position = (input_size[0]+input_size[2]+0.05+0.1*i, 0.35, 0.1, 0.2)
            ax = ui["figure"].add_axes(ferro_position)
            hide_axes(ax)
            ui["last_im_ferro"].append(ax.imshow(np.empty((16,16))*np.nan, vmin=-0.05, vmax=0.05))

            para_position = (input_size[0] + input_size[2] + 0.05 + 0.1 * i, 0.15, 0.1, 0.2)
            ax = ui["figure"].add_axes(para_position)
            hide_axes(ax)
            ui["last_im_para"].append(ax.imshow(np.empty((16, 16)) * np.nan, vmin=-0.05, vmax=0.05))

        # current response output
        bar_position = (input_size[0]+0.05, 0.05, 0.1, 0.4)
        ax = ui["figure"].add_axes(bar_position, title="Neuron Response")
        ax.get_xaxis().set_visible(False)
        ui["bar_ax"] = ax
        ui["response0"] = ax.bar(-0.5, np.nan, 1)[0]
        ui["response1"] = ax.bar(0.5, np.nan, 1)[0]
        ax.set_ylim([-1, 1])


        # history of responses
        history_position = (filter_position[0]+filter_position[2]+0.05,
                            bar_position[1], 0.2, 0.8)
        ax = ui["figure"].add_axes(history_position, title="Response history")
        ax.get_xaxis().set_visible(False)
        ax.set_ylim([-1, 1])
        ui["history_ax"] = ax
        ui["history_response0"] = ax.scatter(range(30), np.zeros(30))
        ui["history_response1"] = ax.scatter(range(30), np.zeros(30))

        return ui

    def click_callback(self, e):
        if e.inaxes is self.ui["input_axes"]:
            # compensate indexing inconsistency in matplotlib
            pos = (int(round(e.ydata)),int(round(e.xdata)))
            im = np.array(self.ui["input_image"].get_array())
            im[pos] = -im[pos]
            self.set_config(im)


if __name__ == "__main__":
    # patterns
    def checkerboard(L=16):
        im = np.ones(L**2)
        ind = np.arange(L**2)
        # flip odd rows
        im[np.logical_and(ind % (2*L) < L, ind % 2 == 0)] = -1
        # flip even rows
        im[np.logical_and(ind % (2*L) >= L, ind % 2 == 1)] = -1

        return np.reshape(im, (L,L))

    data = np.load("foo.npz")
    a = AnalyzeCNN()
    a.train(data["train_config"], data["train_labels"],
            learning_rate=5e-5, epochs=4,
            valid_data=data["test_config"],
            valid_labels=data["test_labels"], progress_update=1)

    # quick alias to _data
    t = data["test_temp"]
    config = data["test_config"]
    indices = np.argsort(t)
    t = t[indices]
    config = config[indices, :]

    a.set_config(config[1000])

    pass
