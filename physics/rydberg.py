import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import coo_matrix
from scipy.spatial.ckdtree import cKDTree
from scipy.spatial.distance import squareform, pdist
import pandas as pd
import param

class Parameter(param.Parameterized):
    @classmethod
    def get_ParameterSet_from_instance(cls, instance):
        keys = cls.params().keys()
        return parameters.ParameterSet({key: instance.__getattribute__(key) for key in keys if key is not 'name'})

    def get_ParameterSet_from_class(self, ParameterClass):
        keys = ParameterClass.params().keys()
        return parameters.ParameterSet({key: self.__getattribute__(key) for key in keys if key is not 'name'})

    @classmethod
    def from_df(cls, ryd_df):
        dictionary = ryd_df.to_dict()['value']
        return cls(**dictionary)

    def create_df(self):
        parameter_set = self.get_ParameterSet_from_class(self)
        ryd_df = pd.DataFrame.from_dict(parameter_set, orient='index')
        ryd_df.columns = ['value']
        return ryd_df


class AtomCloud(Parameter):
    """docstring for GsDistribution"""

    def __init__(self, pos=np.array([]), **params):
        super(AtomCloud, self).__init__(**params)
        self.pos = np.array(pos)
        self.N = len(pos)

    def __len__(self):
        return self.N

    def get_dist(self, ignore_warning=False):
        """Gets self.dist from self.pos"""
        if self.N > 100000 and not ignore_warning:
            raise ValueError('Calculating the distance matrix for more then 100000 atoms typically exceed memory.'
                             'Try get_sparse_dist or set ignore_warnings to True.')

        pos = np.array(self.pos)
        if pos.size == 0:
            return np.array([])
        dist = pdist(pos, 'euclidean')
        return squareform(dist)

    @staticmethod
    def axis_rotation_matrix(rotation_direction, angle):
        d = np.array(rotation_direction, dtype=np.float64)
        d /= np.linalg.norm(d)
        angle *= 2 * np.pi / 360

        eye = np.eye(3, dtype=np.float64)
        ddt = np.outer(d, d)
        skew = np.array([[0, d[2], -d[1]],
                         [-d[2], 0, d[0]],
                         [d[1], -d[0], 0]], dtype=np.float64)
        rot = ddt + np.cos(angle) * (eye - ddt) + np.sin(angle) * skew
        return rot

    def rotate_ground_state_distribution(self, rotation_direction, angle):
        rot = self.axis_rotation_matrix(rotation_direction, angle)
        for i, pos_i in enumerate(self.pos):
            pos_i = rot.dot(pos_i)
            self.pos[i] = pos_i

    def get_sparse_dist(self, cut_off):
        tree = cKDTree(self.pos)
        sparse_dist = tree.sparse_distance_matrix(tree, cut_off)
        return tree, sparse_dist

    @staticmethod
    def get_ijV(sparse_dist):
        coo_dist = coo_matrix(sparse_dist)
        coo_dist.eliminate_zeros()
        i, j = coo_dist.nonzero()
        V = coo_dist.data
        return i, j, V

    def plt_positions(self, figsize=(16, 16)):
        pos = self.pos
        fig = plt.figure(figsize=figsize)
        ax = fig.gca(projection='3d')
        ax.set_aspect('equal')
        X = pos[:, 0]
        Y = pos[:, 1]
        Z = pos[:, 2]
        ax.scatter(X, Y, Z)


class Lattice(AtomCloud):
    """Create uniform cloud of randomly distributed atoms in a box with given blockade radius."""

    Nx = param.Integer(default=5, doc='Number of atoms in x-direction')
    Ny = param.Integer(default=5, doc='Number of atoms in y-direction')
    Nz = param.Integer(default=5, doc='Number of atoms in z-direction')
    distance = param.Number(default=5, doc='Mean nearest neighbor distance')
    epsilon = param.Number(default=0, doc='Mean nearest neighbor distance')

    def __init__(self, **params):
        super().__init__(**params)
        self.N = int(self.Nx * self.Ny * self.Nz)
        self.pos = self.excite()

    def excite(self):
        Nx = self.Nx
        Ny = self.Ny
        Nz = self.Nz
        distance = self.distance
        epsilon = self.epsilon
        N = self.N
        X = np.linspace(0, Nx * distance, Nx)
        Y = np.linspace(0, Nx * distance, Ny)
        Z = np.linspace(0, Nx * distance, Nz)
        pos = np.concatenate(np.meshgrid(X, Y, Z))
        pos = pos.reshape((3, N)).T + epsilon * np.random.rand(N, 3)

        return pos

    def get_int_matrix_disorder(self, disorder):
        int_matrix = self.get_int_matrix()
        N = self.N
        int_matrix_disorder = disorder * (np.random.rand(N, N) - 0.5)
        int_matrix += int_matrix_disorder
        return int_matrix


class Simple_Blockade(AtomCloud):
    N_atoms = param.Integer(default=int(1.3 * 10 ** 5), doc='Number of ground state atoms')
    R_c = param.Number(default=5.2, doc='Blockade radius')
    iteration = param.Integer(default=1000, doc='Number of iterations before break')
    sig_x = param.Number(default=235, doc='Size of ground state distribution in x direction')
    sig_y = param.Number(default=35, doc='Size of ground state distribution in y direction')
    sig_z = param.Number(default=35, doc='Size of ground state distribution in z direction')

    def __init__(self, **params):
        super().__init__(**params)
        sig_x = self.sig_x
        sig_y = self.sig_y
        sig_z = self.sig_z
        N_atoms = self.N_atoms
        R_c = self.R_c
        iteration = self.iteration
        i = 0
        pos = np.random.uniform((0, 0, 0), (sig_x, sig_y, sig_z), (1, 3))
        while len(pos) < N_atoms and i < iteration:
            i += 1
            pos_i = np.random.uniform((0, 0, 0), (sig_x, sig_y, sig_z), (1, 3))
            if np.all(np.linalg.norm(pos_i - pos, axis=1) > R_c):
                pos = np.concatenate([pos, pos_i])
        self.pos = np.array(pos)
        self.N = len(pos)


if __name__ == '__main__':
    l = Simple_Blockade()
    pass