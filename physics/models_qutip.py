from qutip import mcsolve, mesolve, sigmax, sigmaz, tensor, basis
import numpy as np
import qutip as qt

from scipy.spatial.distance import pdist, squareform
from modified_libs.qutip import GpuQobj

from tools.helper import bitwise_boolean


def ising_hamiltonian(system_size, h_x, J=1) -> GpuQobj:
    J = J * np.ones(system_size)

    s_z = tensor_op(system_size, sigmaz())
    s_x = tensor_op(system_size, sigmax())

    # construct the hamiltonian
    H = 0

    # energy splitting terms
    for n in range(system_size):
        H += - h_x * s_x[n]

    # interaction terms
    for n in range(system_size - 1):
        H += - J[n] * s_z[n] * s_z[n + 1]

    return GpuQobj(H)


def xxz_hamiltonian(parameters: dict) -> GpuQobj:
    """
    Wrapper to build quantum object for ordered or unordered XXZ hamiltonian
    :param parameters: model sweeps as dict
    :return:
    """
    # alias
    system_size = parameters['system_size']
    dim = parameters['dimensionality']
    r_scaling = parameters['r_scaling']
    delta = parameters['delta']
    h_x = parameters['h_x']
    J = parameters['J']

    # define hamiltonian
    if parameters['ordered']:
        assert int(system_size ** 1 / dim) ** dim == system_size
        l = int(system_size ** 1 / dim)
        coords = np.meshgrid(*[np.arange(l) for _ in range(dim)])
        coords = np.vstack([c.flatten() for c in coords]).transpose()
        r = squareform(pdist(coords))

        if r_scaling is None:
            # NN neighbor
            r[r > 1] = 0
            H = _xxz_hamiltonian(interaction=J * r,
                                 transverse_field=h_x * np.ones(system_size),
                                 z_coupling=delta)
        else:
            r[r != 0] = r[r != 0] ** r_scaling
            H = _xxz_hamiltonian(interaction=np.triu(J * r, 1),
                                 transverse_field=h_x * np.ones(system_size),
                                 z_coupling=delta)
    else:
        if 'r' in parameters.keys():
            r = parameters['r']
        else:
            cloud_sigma = parameters.pop('cloud_sigma', 0.5)
            assert isinstance(cloud_sigma, float) or isinstance(cloud_sigma, int) or len(cloud_sigma) == dim
            r = np.random.multivariate_normal(np.zeros(dim), cloud_sigma * np.eye(dim),
                                              size=system_size)
        H = _unordered_xxz_hamiltonian(r,
                                       interaction=J,
                                       h=h_x * np.ones(system_size),
                                       delta=delta,
                                       r_scaling=r_scaling,
                                       blockade_r=parameters.pop("blockade_r", 0))
    return H


def _unordered_xxz_hamiltonian(r: np.ndarray, interaction: float, h: np.ndarray, delta: float = 0,
                               r_scaling: float = -6, blockade_r: float = 0):
    """
    defines quantum obj for xxz hamiltonian of n particles in unordered atom cloud

    .. math:: H = \sum_{ij} J r^{s} (\sigma^x_i\sigma^x_j +\sigma^y_i\sigma^y_j + \Delta \sigma^z_i\sigma^z_j) + \Omega_i\sigma^x_i
    :param r_scaling: scaling of interaction with distance
    :param interaction: interaction constant J_ij
    :param r: nxd matrix, describing n coordinates of atoms
    :param delta: .. math:: \Delta
    :param h: .. math:: \Omega_i
    :return: qutip object of XXZ model
    """
    n = len(h)
    assert len(r) == n
    assert r_scaling is not None
    distances = squareform(pdist(r))
    # only count pairs once
    distances = np.triu(distances)
    distances[distances < blockade_r] = 0
    distances[distances > 0] = (distances[distances > 0] ** r_scaling).flatten()
    interaction = interaction * distances

    return _xxz_hamiltonian(interaction, delta, h)


def _xxz_hamiltonian(interaction: np.ndarray, z_coupling: float, transverse_field: np.ndarray) -> GpuQobj:
    """
    defines quantum obj for xxz hamiltonian of n particles

    .. math:: H = \sum_{ij} J_{ij} (\sigma^x_i\sigma^x_j +\sigma^y_i\sigma^y_j + \Delta \sigma^z_i\sigma^z_j) -h_x\sum_i\sigma^x_i
    :param interaction: .. math:: J_{ij}
    :param z_coupling: .. math:: \Delta
    :param transverse_field: .. math:: h_x
    :return: qutip object of XXZ model
    """
    n = len(transverse_field)
    assert all([s == n for s in np.shape(z_coupling)])
    assert all([s == n for s in np.shape(interaction)])

    s_x = tensor_op(n, qt.sigmax())
    s_y = tensor_op(n, qt.sigmay())
    s_z = tensor_op(n, qt.sigmaz())

    H = 0
    for i in range(n):
        H -= transverse_field[i] * s_x[i]
        for j in range(n):
            H += interaction[i, j] * (s_x[i] * s_x[j] + s_y[i] * s_y[j] + z_coupling * s_z[i] * s_z[j])

    return GpuQobj(H)


def tensor_op(N, op=qt.sigmax()):
    return [qt.tensor([op if m == n else qt.qeye(2) for m in range(N)]) for n in range(N)]


def psi0(N):
    # initial state, first spin in state |1>, the rest in state |0>
    psi_list = [basis(2, 1)]

    for n in range(N-1):
        psi_list.append(basis(2, 0))
    return tensor(psi_list)


def integrate(N, h, J, psi0, tlist, solver, opts=None):
    H = ising_hamiltonian(N, h, J)

    # collapse operators
    c_op_list = []

    # evolve and calculate expectation values
    if solver == "me":
        result = mesolve(H, psi0, tlist, c_op_list, tensor_op(N, sigmax()))
    elif solver == "mc":
        ntraj = 250
        result = mcsolve(H, psi0, tlist, c_op_list, tensor_op(N, sigmax()), ntraj, options=opts)
    else:
        raise Exception('Solver unknown')

    return result


z_plus = qt.basis(2, 0)
z_minus = qt.basis(2, 1)

y_plus = 2 ** -0.5 * (z_plus + 1j * z_minus)
y_minus = 2 ** -0.5 * (z_plus - 1j * z_minus)

x_plus = 2 ** -0.5 * (z_plus + z_minus)
x_minus = 2 ** -0.5 * (z_plus - z_minus)

plus = (x_plus, y_plus, z_plus)
minus = (x_minus, y_minus, z_minus)


def projection_ops_bloch(state_no: int, n_particle: int):
    """
    return 3 projection operators, one for each dimension, for a given state number
    :param state_no: spin configuration in z direction, interpreted as binary number, as integer
    :param n_particle: number of spins
    :return: projection operators in x, y, z direction
    """
    return [qt.tensor([p * p.dag() if z_up else m * m.dag() for z_up in
                       bitwise_boolean(state_no, n_particle)]) for p, m in zip(plus, minus)]


def states(state_no: int, n_particle: int):
    """
    same as projection_op_bloch, but without bra
    :param state_no: ...
    :param n_particle: ...
    :return:
    """
    return [qt.tensor([p if z_up else m for z_up in
                       bitwise_boolean(state_no, n_particle)]) for p, m in zip(plus, minus)]


if __name__ == '__main__':
    a = qt.sigmay()
