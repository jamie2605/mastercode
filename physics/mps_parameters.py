import os
import modified_libs.mps as mps
import physics.models_mps as models
import numpy as np


def get_parameters_ch17(myObservables, L=20, prepend_file=""):
    myConv = mps.MPSConvParam(max_bond_dimension=20, max_num_sweeps=6,
                              local_tol=1E-14)

    # Specify constants and parameter lists
    J = 1.0
    glist = np.linspace(0.01, 1.99, 201)
    parameters = []

    for g in glist:
        parameters.append({
            'simtype': 'Finite',
            # Directories
            'job_ID': 'Ising_Statics',
            'unique_ID': 'g_' + str(g),
            'Write_Directory': '{}L={}_TMP/'.format(prepend_file, L),
            'Output_Directory': '{}L={}_OUTPUTS/'.format(prepend_file, L),
            # System size and Hamiltonian common_parameters
            'L': L,
            'J': J,
            'g': g,
            # ObservablesConvergence common_parameters
            'verbose': 0,
            'MPSObservables': myObservables,
            'MPSConvergenceParamets': myConv,
            'logfile': True,
            'Discrete_generators': ['gen'],
            'Discrete_quantum_numbers': [0]
        })
    return parameters


def get_output_ch17(r, exp, L=700, read_only=False, directory=""):
    H, Operators, Observables = models.ising_model_1d(700,
                                                      observe_transverse_mag=True,
                                                      observe_density_matrix=False,
                                                      observe_zz_corr=True,
                                                      interaction_r=r,
                                                      interaction_exp=exp)
    prepend_file = os.path.join(directory, "r{}exp{}_".format(r, exp))
    parameters = get_parameters_ch17(Observables, L=L, prepend_file=prepend_file)

    if read_only:
        MainFiles = None
    else:
        MainFiles = mps.WriteFiles(parameters, Operators, H, PostProcess=False)
    Outputs = mps.read_or_simulate(parameters, MainFiles, parallel=12, read_only=read_only)

    return Outputs, parameters