from abc import ABC, ABCMeta, abstractmethod

import MPSPyLib as mps
import numpy as np

from modified_libs import mps


class MPSModel(ABC, metaclass=ABCMeta):

    @staticmethod
    @abstractmethod
    def get_parameters(observables, model_parameters, sweep_parameters):
        pass

    @staticmethod
    @abstractmethod
    def model_definition(model_parameters):
        pass


class TFIMPSModel(MPSModel):

    @staticmethod
    def model_definition(system_size, interaction_r=1, interaction_exp=-3,
                         observe_transverse_mag: bool = False,
                         observe_density_matrix: bool = False,
                         observe_mutual_info: bool = False,
                         observe_zz_corr: bool = True):

        # Build spin operators for spin-1/2 system
        operators = mps.BuildSpinOperators(0.5)
        operators['sigmax'] = 2 * operators['sz']
        operators['sigmaz'] = -(operators['splus'] + operators['sminus'])
        operators['gen'] = np.array([[0, 0], [0, 1.]])

        # Define Hamiltonian of transverse Ising model
        hamiltonian = mps.MPO(operators)
        # add transverse field
        hamiltonian.AddMPOTerm('site', 'sigmax', hparam='h_x', weight=-1.0)

        # Add interaction terms
        if interaction_r == 1:
            hamiltonian.AddMPOTerm('bond', ['sigmaz', 'sigmaz'],
                                   hparam='J', weight=-1.0)

        elif interaction_r is np.inf:
            hamiltonian.AddMPOTerm('InfiniteFunction', ['sigmaz', 'sigmaz'],
                                   hparam='J', weight=-1.0,
                                   func=lambda x: x ** interaction_exp, L=system_size, tol=1e-9)
        else:
            f = []
            for ii in range(interaction_r):  # the sum goes over all i and j separated by interaction_r
                f.append(1.0 * (ii + 1.0) ** interaction_exp)
            hamiltonian.AddMPOTerm('FiniteFunction', ['sigmaz', 'sigmaz'], f=f,
                                   hparam='J', weight=-1.0)

        # Observables
        my_observables = mps.Observables(operators)
        if observe_zz_corr:
            my_observables.AddObservable('corr', ['sigmaz', 'sigmaz'], 'zz')
        if observe_transverse_mag:
            my_observables.AddObservable('site', 'sigmax', 'sigmax')
        if observe_density_matrix:
            my_observables.AddObservable('DensityMatrix_i', [])
            my_observables.AddObservable('DensityMatrix_ij', [])
        if observe_mutual_info:
            my_observables.AddObservable('MI', None)

        return hamiltonian, operators, my_observables

    @staticmethod
    def get_parameters(observables, model_parameters, sweep_parameters):
        system_size = model_parameters['system_size']

        my_conv = mps.MPSConvParam(max_bond_dimension=20, max_num_sweeps=6,
                                   local_tol=1E-14)

        interaction_r = None if 'interaction_r' not in model_parameters.keys() else model_parameters["interaction_r"]
        interaction_exp = None if 'interaction_exp' not in model_parameters.keys() else model_parameters["interaction_exp"]


        common_parameters = {
            'simtype': 'Finite',
            # Directories
            'job_ID': 'TFI',
            'Write_Directory': 'TFI_corr_L={}_{}{}_TMP/'.format(system_size, interaction_r, interaction_exp),
            'Output_Directory': 'TFI_corr_L={}_{}{}_OUTPUTS/'.format(system_size, interaction_r, interaction_exp),
            # System size and Hamiltonian common_parameters
            'L': system_size,
            'J': 1.0,
            # ObservablesConvergence common_parameters
            'verbose': 0,
            'MPSObservables': observables,
            'MPSConvergenceParameters': my_conv,
            'logfile': True,
            'Discrete_generators': ['gen'],
            'Discrete_quantum_numbers': [0]
        }

        parameters = []
        for h_x in sweep_parameters['h_x']:
            parameters.append({
                'unique_ID': 'h_x_' + str(h_x),
                'h_x': h_x
            })
        return common_parameters, parameters


class XXZMPSModel(MPSModel):
    @staticmethod
    def model_definition(system_size,
                         observe_corr: bool = True,
                         observe_mag: bool = True):
        """
        returns xxz model in MPS framework, same as helper.physics.xxz_hamiltonian
        :param observe_mag: define observable magnetization in all directions
        :param observe_corr: define observable correlation in all directions
        :param sweeps: model sweeps
        :return: hamiltonian, operators and correlations for use in mps model simulation
        """
        # Build spin operators for spin-1/2 system
        operators = mps.BuildSpinOperators(0.5)
        operators['sigmaz'] = 2 * operators['sz']
        operators['sigmay'] = 1j * (operators['sminus'] - operators['splus'])
        operators['sigmax'] = -(operators['splus'] + operators['sminus'])
        # operators['gen'] = np.array([[1., 0], [0, 0]])

        # Define Hamiltonian of nearest neighbor xxz-model
        hamiltonian = mps.MPO(operators)
        hamiltonian.AddMPOTerm('bond', ['sigmax', 'sigmax'])
        hamiltonian.AddMPOTerm('bond', ['sigmay', 'sigmay'])
        hamiltonian.AddMPOTerm('bond', ['sigmaz', 'sigmaz'], hparam='delta')

        hamiltonian.AddMPOTerm('site', 'sigmax', hparam='h_x', weight=-1.0)

        # Observables
        observables = mps.Observables(operators)
        if observe_corr:
            observables.AddObservable('corr', ['sigmax', 'sigmax'], 'xx')
            observables.AddObservable('corr', ['sigmay', 'sigmay'], 'yy')
            observables.AddObservable('corr', ['sigmaz', 'sigmaz'], 'zz')
        if observe_mag:
            observables.AddObservable('site', 'sigmax', 'sigmax')
            observables.AddObservable('site', 'sigmay', 'sigmay')
            observables.AddObservable('site', 'sigmaz', 'sigmaz')

        return hamiltonian, operators, observables

    @staticmethod
    def get_parameters(observables, model_parameters, sweep_parameters):
        my_conv = mps.MPSConvParam(max_bond_dimension=20, max_num_sweeps=6,
                                   local_tol=1E-14)

        system_size = model_parameters['system_size']

        common_parameters = {
            'simtype': 'Finite',
            # Directories
            'job_ID': 'XXZ',
            'Write_Directory': 'XXZ_corr_L={}_TMP/'.format(system_size),
            'Output_Directory': 'XXZ_corr_L={}_OUTPUTS/'.format(system_size),
            # System size and Hamiltonian common_parameters
            'L': system_size,
            # ObservablesConvergence common_parameters
            'verbose': 0,
            'MPSObservables': observables,
            'MPSConvergenceParameters': my_conv,
            'logfile': True
        }

        parameters = []

        for i, row in sweep_parameters.iterrows():
            parameters.append({
                'unique_ID': 'hx_{}_delta_{}'.format(row['h_x'], row['delta']),
                'delta': row['delta'],
                'h_x': row['h_x'],
            })

        return common_parameters, parameters
