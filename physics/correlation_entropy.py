import numpy as np
from tools.helper import array2d_to_r
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def rho_2site(correlation):
    return 1/4*np.diag((1+correlation, 1-correlation, 1-correlation, 1+correlation))


def rho_site(magnetization):
    return 1/2*np.diag((1+magnetization, 1-magnetization))


def entropy(rho):
    return -1/np.log(2)*np.trace(rho*np.nan_to_num(np.log(rho)))


def _correlation_entropy(correlation_ij, mag_i, mag_j):
    return entropy(rho_site(mag_i))+entropy(rho_site(mag_j))-entropy(rho_2site(correlation_ij))


correlation_entropy = np.vectorize(_correlation_entropy)


def calculate_correlation(configs, l=24):
    # calculcate $\langle \sigma_i\sigma_j \rangle$ over all configurations, return as site pairs
    correlation = np.zeros((l ** 2, l ** 2))
    for c in configs:
        si, sj = np.meshgrid(c, c)
        correlation += si * sj

    return correlation * 1 / len(configs)


def calculate_correlation_entropy(configs, l=24):
    # print("Calculating magnetization, correlation")
    mag = np.mean(configs, axis=0)
    correlation = calculate_correlation(configs, l)

    # print("Calculating pairwise correlation entropy")
    mag_i, mag_j = np.meshgrid(mag, mag)
    corr_entropy = correlation_entropy(correlation, mag_i, mag_j)

    # print("Reparametrizing")
    return corr_entropy, array2d_to_r(corr_entropy)


def plot_correlation_entropy(corr_entropies, i, l=24):
    # plot raw correlation entropy for this temperature
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    xs, ys = np.meshgrid(np.arange(l ** 2), np.arange(l ** 2))
    ax.plot_surface(xs, ys, corr_entropies[i, ...])
    ax.set_ylabel('site_i')
    ax.set_xlabel('site_j')
    ax.set_zlabel('correlation_entropy')
    plt.show()


def plot_correlation_entropy_r(corr_entropy_r, temps):
    # calculate intermediate plot up to current temperature
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    r, t = np.meshgrid(range(np.shape(corr_entropy_r)[1]), temps)
    ax.plot_surface(r, t, corr_entropy_r)
    ax.set_ylabel('temperature')
    ax.set_xlabel('r')
    ax.set_zlabel('correlation_entropy')
    plt.show()


# border cases
if __name__ == "__main__":
    # all spin up
    print(correlation_entropy(1, 1, 1))
    # all spin down
    print(correlation_entropy(1, -1, -1))

    # half of the configurations are completely spin up, other half down
    print(correlation_entropy(1, 0, 0))

    # paramagnetic state
    print(correlation_entropy(0, 0, 0))
