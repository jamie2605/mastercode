from typing import Dict, Tuple

from analyzers.noise import MLNoiseFigure
from analyzers.noise_networks import NetworkMeasureNoiseFigure

from features import FeatureSet
from visualization.features import Magnetization1D
from analyzers.network_response_basics import ResponseFigureBase
from visualization.network_science import NetworkMeasureFigure
import pandas as pd


class LongRangeComparison(ResponseFigureBase, NetworkMeasureFigure):
    def     __init__(self, data: pd.DataFrame, feature_sets: Dict[Tuple[int, float], FeatureSet]):
        self.feature_sets = feature_sets
        self.add_state_parameter_filter("interaction", "tab", "ctrl+tab")
        self.add_state_parameter("display_magnetization", values=[True, False], increase="m")

        super().__init__(data=data)

        self.update_state_by_value({'display_threshold': False}, draw=True)

    def draw(self):
        self.ax.cla()
        data = self.get_results(drop_filter_keys=True)
        state = self.state_vals

        if len(data) > 0:
            # calculate magnetization, if required
            if state["display_magnetization"]:
                magnetizations = Magnetization1D.calculate_magnetizations(self.feature_sets[state["interaction"]],
                                                                          feature_type='zz-correlation')
                magnetizations = pd.DataFrame(magnetizations, columns=["h_x", "magnetization"]).set_index("h_x")
                magnetizations["magnetization"] = magnetizations["magnetization"] ** 2
                data = pd.concat([data, magnetizations], axis=1)

            # select columns to view
            select = []
            for c in data.columns:
                if "display_{}".format(c) in state.keys() and state["display_{}".format(c)]:
                    select.append(c)
            if state["display_responses"]:
                select += ["r0", "r1"]

            data = data[select]
            data.plot(ax=self.ax, markersize=3, marker='o', linestyle='-', alpha=1)
            self.ax.set_title(state["interaction"])

            self.ax.set_xlim((0.8, 1.5))

    def evaluate(self, *args, **kwargs):
        raise NotImplementedError("Cannot train in this combined view")


class NoiseComparison(MLNoiseFigure, NetworkMeasureNoiseFigure):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def evaluate(self, relative: float = None, absolute: float = None):
        return NotImplementedError("This combined Figure is view-only")

    def draw(self):
        results = self.get_results(drop_filter_keys=True)
        self.ax.cla()

        if len(results) > 0:
            self.add_network_measures(results)
            self.add_responses_plot(results)

            state = self.state_vals
            self.ax.set_title(
                "relative noise: {:.2f} absolute noise: {:.2f}".format(state["relative"], state["absolute"]))