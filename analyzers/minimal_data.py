from math import ceil
from typing import Tuple

from analyzers.network_response_basics import ResponseFigureBase
import numpy as np

from core import Analyzer
from features import FeatureSet
from neural_networks import DenseModel


class MinimalDataAnalyzer(Analyzer):
    def __init__(self, feature_set: FeatureSet, threshold: float = 2.269,
                 threshold_column: str = 'T', **kwargs):
        self.feature_set = feature_set
        self.threshold = threshold
        self.threshold_column = threshold_column

        super().__init__(**kwargs)

    def evaluate(self, train_n: int,
                 num_neurons: Tuple[int] = (100, ),
                 train_epochs_times_n: float = 200,
                 batch_size: int = None):

        train, test = self.feature_set.split_by_homogeneous_proportion(train_n)
        train_y = train.binary_single_2d_label(self.threshold, self.threshold_column)
        test_y = test.binary_single_2d_label(self.threshold, self.threshold_column)
        nn = DenseModel(train.n_features, model_parameters={'num_neurons': num_neurons})
        nn.train(train.x, train_y, epochs=ceil(train_epochs_times_n/train_n)+5, batch_size=batch_size, validation_data=(test.x, test_y))

        results = nn.evaluate_along_label_f(test, self.threshold, self.threshold_column)

        if results["r0"].min() > results["r1"].max() or results["r1"].min() > results["r0"].max():
            raise ValueError("Did not converge, responses did not cross")
        if nn.model.evaluate(test.x, test_y)[1] < 0.90:
            raise ValueError("Did not reach required accuracy across test set")

        return results


class MinimalDataFigure(ResponseFigureBase, MinimalDataAnalyzer):
    def draw(self):
        results = self.get_results()
        self.ax.cla()

        if len(results) > 0:
            self.add_responses_plot(results, x_axis=self.threshold_column, group_along=['train_n'])