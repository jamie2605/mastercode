from typing import Tuple

from analyzers.network_response_basics import ResponseAnalyzerFixedFeatureSet, ResponseFigureBase
from features import FeatureSet
from neural_networks import DenseModel


class ResponseNoiseAnalyzer(ResponseAnalyzerFixedFeatureSet):
    def __init__(self, feature_set: FeatureSet=None, threshold: float = 1,
                 threshold_column: str = 'h_x',
                 train_values: Tuple[float, float] = (0, 2),
                 test_values: Tuple[float, float] = (0.3, 1.7), **kwargs):
        # set default arguments of init in different way to better suit quantum Ising by default

        super().__init__(feature_set=feature_set, threshold=threshold,
                         threshold_column=threshold_column,
                         train_values=train_values,
                         test_values=test_values, **kwargs)

    def evaluate(self, relative: float = None, absolute: float = None):
        train = self.train_data.add_noise(relative, absolute)
        test = self.test_data.add_noise(relative, absolute)

        nn = DenseModel(train.n_features)

        nn.train(train.x, self.train_labels, validation_data=(test.x, self.test_labels))

        return nn.evaluate_along_label_f(test, self.threshold, self.threshold_column)


class MLNoiseFigure(ResponseFigureBase, ResponseNoiseAnalyzer):
    def __init__(self, **kwargs):
        self.add_state_parameter_filter("relative", "up", "down")
        self.add_state_parameter_filter("absolute", "ctrl+up", "ctrl+down")
        super().__init__(**kwargs)

    def draw(self):
        super().draw()
        state = self.state_vals
        self.ax.set_title("relative noise: {:.1f} absolute noise: {:.1f}".format(state["relative"], state["absolute"]))
        self.ax.set_xlim([0.85, 1.15])
