from abc import ABCMeta
from typing import Tuple, List, Dict

import matplotlib
import numpy as np
from pandas import DataFrame

from core.analysis import Analyzer
from features import FeatureSet
from neural_networks import DenseModel
from tools.helper import find_crossing
from core.visualization import AnalyzerFigure, color_map


class ResponseAnalyzerBase(Analyzer, metaclass=ABCMeta):
    def __new__(cls, *args, **kwargs):
        obj = super().__new__(cls)
        obj.threshold = None
        obj.threshold_column = None
        return obj


class NetworkResponseAnalyzerFixed(ResponseAnalyzerBase):
    def __init__(self, train_data: FeatureSet, test_data: FeatureSet,
                 threshold: float=1, threshold_column: str = 'h_x', **kwargs):
        """
        Wrapper to train neural network and evaluate_and_show output. Designed for usage with Network Analysis class
        :param train_data:
        :param test_data:
        """
        self.threshold_column = threshold_column
        self.threshold = threshold
        self.train_data = train_data
        self.test_data = test_data
        self.y_train = self.train_data.binary_single_2d_label(threshold, threshold_column)
        self.y_test = self.test_data.binary_single_2d_label(threshold, threshold_column)

        super().__init__(**kwargs)

    def evaluate(self):
        nn = DenseModel(self.train_data.n_features, {'num_neurons': (100,)})
        nn.train(self.train_data.x, self.y_train, validation_data=(self.test_data.x, self.y_test))
        return nn.evaluate_along_label_f(self.test_data, self.threshold, self.threshold_column)


class ResponseAnalyzerFixedFeatureSet(ResponseAnalyzerBase, metaclass=ABCMeta):
    def __init__(self, feature_set: FeatureSet=None, threshold: float=2.269,
                 threshold_column: str = 'T',
                 train_values: Tuple[float, float] = (1, 3.5),
                 test_values: Tuple[float, float] = (1.5, 3), **kwargs):
        """
        Play with different neuron numbers for a dense neural network model
        :param feature_set:
        :param threshold: label value to split feature set into 2 phases
        :param threshold_column: which label column to use for split
        :param train_values: selection for train_values by label value
        :param test_values: selection for test values by label value
        :param kwargs:
        """
        self.threshold = threshold
        self.threshold_column = threshold_column

        if feature_set is not None:
            self.train_data, \
            self.test_data = feature_set.split_label_value(train_values={threshold_column: train_values},
                                                           test_values={threshold_column: test_values})
            self.train_labels = self.train_data.binary_single_2d_label(threshold, threshold_column)
            self.test_labels = self.test_data.binary_single_2d_label(threshold, threshold_column)

        super().__init__(**kwargs)

    def evaluate(self):
        nn = DenseModel(self.train_data.n_features, {"num_neurons": (100,)})
        nn.train(self.train_data.x, self.train_labels, validation_data=(self.test_data.x, self.test_labels))
        return nn.evaluate_along_label_f(self.test_data, self.threshold, self.threshold_column)


class VariableFeatureSetAnalyzer(Analyzer):
    def __init__(self, feature_sets: Dict[int, Dict[str, FeatureSet]], threshold: float=2.269,
                 threshold_column: str = 'T', **kwargs):
        """
        Evaluate the response of a neuron output to different feature sets specified by a parameter
        :param feature_sets: Dict of Dict of feature sets (train, test) with some parameter as key
                            {system_size1: {train : fs, test: fs}, system_size2: {...} ...}
        :param threshold: where to set label threshold in data
        :param kwargs:
        """
        assert all([set(f.keys()) == {"train", "test"} for f in feature_sets.values()])
        self.feature_sets = feature_sets
        self.threshold = threshold
        self.threshold_column = threshold_column

        super().__init__(**kwargs)

    def evaluate(self, feature_parameter: int, num_neurons: Tuple[int] = (50, 10),
                 train_epochs: int = 5,
                 batch_size: int = None):
        if feature_parameter not in self.feature_sets.keys():
            raise KeyError("Feature set for system size {} not present".format(feature_parameter))

        train_x = self.feature_sets[feature_parameter]["train"].x
        test_x = self.feature_sets[feature_parameter]["test"].x
        train_y = self.feature_sets[feature_parameter]["train"].binary_single_2d_label(self.threshold,
                                                                                       self.threshold_column)
        test_y = self.feature_sets[feature_parameter]["test"].binary_single_2d_label(self.threshold,
                                                                                     self.threshold_column)

        n_features = self.feature_sets[feature_parameter]["train"].n_features
        nn = DenseModel(n_features, model_parameters={'num_neurons': num_neurons})
        nn.train(train_x, train_y, epochs=train_epochs, batch_size=batch_size, validation_data=(test_x, test_y))

        return nn.evaluate_along_label_f(self.feature_sets[feature_parameter]["test"],
                                         self.threshold, self.threshold_column)


class ResponseFigureBase(AnalyzerFigure, ResponseAnalyzerBase, metaclass=ABCMeta):
    def __init__(self, **kwargs):
        self.add_state_parameter("display_threshold", values=[True, False], increase="t")
        self.add_state_parameter("display_responses", values=[True, False], increase="e")
        self.add_state_parameter("display_crossing", values=[True, False], increase="c")
        self.add_state_parameter("display_accuracy", values=[False, True], increase="a")
        self.add_state_parameter("display_std", values=[False, True], increase="d")

        super().__init__(**kwargs)

    def draw(self):
        results = self.get_results()
        self.ax.cla()

        if len(results) > 0:
            self.add_responses_plot(results)

    def add_responses_plot(self, results: DataFrame, x_axis: str = None, group_along: List[str] = None,
                           display_legend: bool = True):
        """

        :param display_legend: add legend to plot
        :param x_axis: index level to use for x Axis
        :param results: Data Frame of results, must have columns
        :param group_along: levels to group data by, All other levels are marginalized over
        :return:
        """
        if x_axis is None:
            x_axis = self.threshold_column
        # Input validation
        assert x_axis in results.index.names
        if group_along is not None:
            assert all([g in results.index.names for g in group_along])

        # check that all requested columns are present in results. If None requested, use all columns found in results
        s = self.state_vals
        cols = []
        for c in ["r0", "r1", "accuracy", "std"]:
            if c in ["r0", "r1"]:
                if s["display_responses"]:
                    cols.append(c)
            elif s["display_{}".format(c)]:
                cols.append(c)

        if len(results) == 0 or len(cols) == 0:
            return

        plot_data = results[cols]

        if group_along is None:
            values = plot_data.groupby(level=x_axis).mean()
            errors = plot_data.groupby(level=x_axis).std()

            values.plot(yerr=errors, ax=self.ax, marker='o',
                        linestyle='', markersize=5, legend=display_legend)

            if s["display_responses"] and s["display_crossing"]:
                crossing = find_crossing(values["r0"].values, values["r1"].values,
                                         values.index.get_level_values(x_axis))
                self.ax.axvline(crossing, label='crossing', linestyle=':')
        else:
            traces = np.arange(len(plot_data.groupby(level=group_along)))
            c, m = color_map(traces)

            handles = []
            labels = []
            for trace_i, (group_values, group) in enumerate(plot_data.groupby(level=group_along)):
                artists = set(self.ax.get_children())
                values = group.groupby(level=x_axis).mean()
                errors = group.groupby(level=x_axis).std()

                # use different markers for default_labels, colors for groups
                for column, marker in zip(cols, [m for i, m in enumerate(['<', '>', 'o', '+', 's']) if i < len(cols)]):
                    values[column].plot(yerr=errors[column], ax=self.ax,
                                        linestyle='', markersize=5, marker=marker, c=c[trace_i],
                                        label='foo')

                    if s["display_responses"] and s["display_crossing"]:
                        crossing = find_crossing(values["r0"].values, values["r1"].values,
                                                 values.index.get_level_values(x_axis))
                        self.ax.axvline(crossing, c=c[trace_i], linestyle=':')

                new_artists = set(self.ax.get_children())-artists
                for a in new_artists:
                    if isinstance(a, matplotlib.collections.LineCollection):
                        handles.append(a)
                        break
                labels.append(group_values)

            if display_legend:
                self.ax.legend(handles, labels)

        if s["display_threshold"]:
            self.ax.axvline(self.threshold_scaled, c='k', label='threshold', linestyle=':')

        self.ax.relim()
        self.ax.autoscale()

    @property
    def threshold_scaled(self):
        return self.threshold


class ResponseFigure(ResponseFigureBase, ResponseAnalyzerFixedFeatureSet):
    pass


class BiasLessAnalyzer(Analyzer):
    def __init__(self, feature_set: FeatureSet, train_p: float = 0.2, **kwargs):
        """

        :param feature_set: feature set to use for training and testing
        :param train_p: proportion of feature set to use for training
        :param kwargs:
        """

        self.train, self.test = feature_set.split_by_proportion(train_p=train_p)
        self.threshold = kwargs.pop("threshold", 1)
        self.threshold_column = kwargs.pop("threshold_column", "h_x")

        super().__init__(**kwargs)

    def evaluate(self, train_epochs: int = 2, batch_size: int = None):
        nn = DenseModel(self.train.n_features, model_parameters={'num_neurons': (100,)})

        train_y = self.train.binary_single_2d_label(self.threshold, self.threshold_column)
        test_y = self.test.binary_single_2d_label(self.threshold, self.threshold_column)
        nn.train(self.train.x, train_y, epochs=train_epochs, batch_size=batch_size,
                 validation_data=(self.test.x, test_y))

        return nn.evaluate_along_label_f(self.test, self.threshold, self.threshold_column)


class BiasLessFigure(ResponseFigureBase, BiasLessAnalyzer):
    pass
