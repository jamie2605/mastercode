from core import Analyzer, AnalyzerFigure
from features import FeatureSet
from tools.helper import calculate_network_measures_f
import numpy as np


class NetworkMeasureNoiseAnalyzer(Analyzer):
    def __init__(self, feature_set: FeatureSet = None, **kwargs):
        """

        :param feature_set: Feature set with adjacency matrix as features
        """
        self.feature_set = feature_set
        super().__init__(**kwargs)

    def evaluate(self, relative: float = None, absolute: float = None):
        features = self.feature_set.add_noise(relative, absolute)

        return calculate_network_measures_f(features).unified


class NetworkMeasureNoiseFigure(AnalyzerFigure, NetworkMeasureNoiseAnalyzer):
    def __init__(self, **kwargs):
        self.network_measures = ["clustering", "density", "disparity", "pearson"]

        for i, n in enumerate(self.network_measures):
            self.add_state_parameter("display_{}".format(n), values=[True, False], increase=str(i))

        self.add_state_parameter_filter("relative", "up", "down")
        self.add_state_parameter_filter("absolute", "ctrl+up", "ctrl+down")

        super().__init__(**kwargs)

    def draw(self):
        results = self.get_results(drop_filter_keys=True)

        self.ax.cla()
        if len(results) > 0:
            self.add_network_measures(results)
            state = self.state_vals
            self.ax.set_title(
                "relative noise: {:.2f} absolute noise: {:.2f}".format(state["relative"], state["absolute"]))

    def add_network_measures(self, results):
        # select columns
        state = self.state_vals
        results = results[[m for m in self.network_measures if state["display_{}".format(m)]]]
        # normalize
        results = results.apply(lambda x: x / np.max(np.abs(x)))
        # calculate errors
        values = results.groupby(level=0).mean()
        errors = results.groupby(level=0).std()
        values.plot(yerr=errors, ax=self.ax)
