from typing import Union, Tuple, Dict, List

from matplotlib import pyplot as plt
from pandas import DataFrame
import pandas as pd
import numpy as np

from abc import abstractmethod

# from NN.BaseNN import TensorFlowNN
from core import AnalyzerFigure, minmax
from core.analysis import Analyzer
from neural_networks.BaseNN import BaseNN
from tools.helper import build_matrix
from visualization.features import Orderings2DBase


class ConfusionDetector2D(Analyzer):
    def __init__(self, **kwargs):
        self.nn = kwargs.pop("neural_network")
        assert isinstance(self.nn, BaseNN)

        self.data = kwargs.pop("features")

        super().__init__(**kwargs)

    def evaluate(self, h_x: float, delta: float,
                 scan_direction: str = 'delta',
                 window_width: float = 0.5,
                 train_epochs: int = 10) -> dict:
        """

        :param train_epochs:
        :param h_x: first coordinate in phase diagram
        :param delta: second coordinate in phase diagram
        :param scan_direction: in which axis to orient scan window
        :param window_width: width of scan window, endpoints are test points
        :return:
        """

        assert all([k in self.data.labels.columns for k in ["h_x", "delta"]])
        assert scan_direction in self.data.labels.columns
        parameter_point = {"h_x": h_x, "delta": delta}

        # select train window
        window = parameter_point.copy()
        window[scan_direction] = (window[scan_direction] - window_width / 2,
                                  window[scan_direction] + window_width / 2)
        train, test = self.data.split_inner_hyper_rectangle(**window)

        # train
        self.nn.restart_session()
        self.nn.train_f(train, valid_data=None, epochs=train_epochs,
                        column=scan_direction, threshold=parameter_point[scan_direction])

        confidences = self.nn.predict(test.x)
        results = self.nn.evaluate_along_label_f(test, parameter_point[scan_direction], scan_direction)

        # window width check, calculate measure of phase prediction
        window_width_test = test.labels[scan_direction].max() - test.labels[scan_direction].min()
        if window_width_test < 1e-13:
            print("Window width too small, test window has size of zero")
            mopp = np.nan
        else:
            avg_confidence = pd.DataFrame({"confidence": confidences[:, 0]},
                                          index=test.labels[scan_direction].values).groupby(level=0).mean()
            mopp = 2* np.abs(0.5-avg_confidence).mean()

        return {"predict": np.mean(confidences, axis=0),
                "predict_std": np.std(confidences, axis=0),
                "pseudo-accuracy_along_label": results["accuracy"].values,
                "r0": results["r0"].values,
                "r1": results["r1"].values,
                "measure_of_phase_prediction": mopp}


class ConfusionPhaseDiagram2D(ConfusionDetector2D, AnalyzerFigure):
    """
    Supervise Training of networks for a 2D phase diagram
    """

    def __init__(self, **kwargs):

        self.add_state_parameter_filter("scan_direction", "tab")
        self.add_state_parameter_filter("window_width", "up", "down")
        self.add_state_parameter("vmax_factor", np.linspace(0, 1, 10), "ctrl+up", "ctrl+down", initial_i=9)

        self.add_state_parameter("overlay_phase_diagram", [True, False], "o")

        super().__init__(**kwargs)

    def draw(self):
        results = self.get_results()
        state_vals = self.state_vals

        # extents = {k: results.index.levels[results.index.names.index(k)].astype(float).values
        #       for k in ["threshold_h", "threshold_delta"]}
        # extents = {k: (min(v), max(v)) for k, v in extents.items()}
        # self.art["im"].set_extent(extents["threshold_h"] + extents["threshold_delta"])

        template = "scanning in {}-direction, window size {}"
        self.ax.set_title(template.format(state_vals["scan_direction"], state_vals["window_width"]))

        if len(results) < 2:
            self.art["im"].set_data(np.full((2, 2), np.nan))
            return False

        vals = results["measure_of_phase_prediction"]
        coordinates = [a for a in zip(
            *[results.index.get_level_values(c).astype(float)
              for c in ["h_x", "delta"]])]
        image, bins, extent = build_matrix(coordinates, vals.values)

        self.art["im"].set_data(np.flipud(image))
        self.art["im"].set_extent((extent[0], extent[1], extent[3], extent[2]))

        if "vmax_factor" in state_vals.keys():
            vmax = state_vals["vmax_factor"]
        else:
            vmax = 1
        self.art["im"].set_clim((np.nanmin(image), np.nanmax(image) * vmax))

        overlay = state_vals["overlay_phase_diagram"]
        for o in self.art["overlay"]:
            o.set_visible(overlay)

    def prepare_plot(self):
        self.ax.set_xlabel("h_x")
        self.ax.set_ylabel("delta")
        art = dict(im=plt.imshow(np.full((2, 2), np.nan)))

        art["cb"] = self.fig.colorbar(art["im"], label="P")
        art["overlay"] = Orderings2DBase.overlay_phase_diagram(self.ax)

        return art


class WeakPhaseDetector2D(Analyzer):
    def __init__(self, **kwargs):
        self.nn = kwargs.pop("neural_network")
        assert isinstance(self.nn, BaseNN) or isinstance(self.nn, dict)

        self.data = kwargs.pop("features")

        super().__init__(**kwargs)

    def evaluate(self, train_patches: Tuple[Tuple],
                 train_epochs: int = 10, batch_size: int = None) -> pd.DataFrame:

        """
        Train model by training on some patches in phase diagram, test on rest
        :param train_epochs:
        :param batch_size:
        :param train_patches: variables train_patch_<n>=tuple (h_x min, h_x max, delta min, delta max)
        :return:
        """
        batch_size = None if batch_size in [np.nan, None] else int(batch_size)

        labels = np.zeros((len(self.data), len(train_patches)), dtype=bool)
        for i, patch in enumerate(train_patches):
            labels[:, i] = self.data.select_patch(h_x=(patch[0], patch[1]), delta=(patch[2], patch[3]))

        train_i = np.any(labels, axis=1)
        train_labels = labels[train_i, :]
        train_set = self.data[train_i, :]
        test_set = self.data[np.logical_not(train_i), :]

        # train
        self.nn.restart_session()
        self.nn.train(train_set.x, train_labels, epochs=int(train_epochs), batch_size=batch_size)

        confidences = self.nn.predict(test_set.x)

        label_index = test_set.labels.set_index([c for c in test_set.labels.columns]).index
        confidences = pd.DataFrame(confidences, index=label_index)
        levels = list(range(confidences.index.nlevels))
        confidences_grouped = confidences.groupby(level=levels)

        return pd.merge(confidences_grouped.mean(), confidences_grouped.std(),
                        left_index=True, right_index=True, suffixes=('', '_err'))


class Weak4PhaseDetection2D(AnalyzerFigure, WeakPhaseDetector2D):
    def __init__(self, **kwargs):
        kwargs["subplot_args"] = kwargs.pop("subplot_args", {"nrows": 2, "ncols": 2, "sharex": True, "sharey": True})
        self.add_colorbar = kwargs.pop("add_colorbar", False)
        self.add_state_parameter_filter("train_patches", "tab", "ctrl+tab")
        self.add_state_parameter_filter("train_epochs")
        self.add_state_parameter('overlay_phase_diagram', values=[True, False], increase="o")

        super().__init__(**kwargs)

    def prepare_plot(self):
        images = [ax.imshow(np.zeros((2, 2))) for ax in self.ax.flatten()]
        art = {"im": images}

        if self.add_colorbar:
            self.fig.subplots_adjust(right=0.8)
            art["cb_ax"] = self.fig.add_axes([0.85, 0.15, 0.05, 0.7])
            art["cb"] = self.fig.colorbar(art["im"][0], cax=art["cb_ax"])

        art['overlay'] = [Orderings2DBase.overlay_phase_diagram(ax) for ax in self.ax.flatten()]

        return art

    def draw(self):
        results = self.get_results(drop_filter_keys=True)
        state_vals = self.state_vals

        if len(results) > 0:
            images, extents = self.get_images(results, self.data.labels.columns[0])

            for i, (image, extent) in enumerate(zip(images, extents)):
                self.art["im"][i].set_data(image)
                self.art["im"][i].set_extent(extent)
                self.art["im"][i].set_clim(minmax(image))

            self.decorate()
            self.overlay()

    @staticmethod
    def get_images(results: pd.DataFrame, unstacking: str = 'h_x') -> List[np.ndarray]:
        images = []
        extents = []
        # marginalize over index
        results = results.groupby(level=list(results.index.names)).mean()

        for i in range(4):
            image = results[str(i)].unstack(level=unstacking)
            images.append(np.flipud(image.values))
            extents.append((image.columns.min(), image.columns.max(), image.index.min(), image.index.max()))

        return images, extents

    def decorate(self):
        for row, axl in enumerate(self.ax):
            for col, ax in enumerate(axl):
                if row == 1:
                    ax.set_xlabel(self.data.labels.columns[0])
                if col == 0:
                    ax.set_ylabel(self.data.labels.columns[1])
        self.overlay()

    def overlay(self):
        visible_phase_diagram = self.state_vals["overlay_phase_diagram"]
        for ax in self.art["overlay"]:
            for o in ax:
                o.set_visible(visible_phase_diagram)


class Weak4PhaseDetection2DRGB(Weak4PhaseDetection2D):
    def __init__(self, **kwargs):
        kwargs["subplot_args"] = {}
        super().__init__(**kwargs)

    def prepare_plot(self):
        art = {"im": self.ax.imshow(np.zeros((2, 2))),
               'overlay': Orderings2DBase.overlay_phase_diagram(self.ax, color='white')}
        return art

    def draw(self):
        results = self.get_results(drop_filter_keys=True)
        state_vals = self.state_vals

        if len(results) > 0:
            images, extents = self.get_images(results, self.data.labels.columns[0])

            rgb_image = np.moveaxis(np.array(images), 0, 2)[..., :3]
            rgb_image[np.isnan(rgb_image)] = 1
            self.art["im"].set_data(rgb_image)
            self.art["im"].set_extent(extents[0])


    def overlay(self):
        visible_phase_diagram = self.state_vals["overlay_phase_diagram"]
        for o in self.ax:
            o.set_visible(visible_phase_diagram)


class Weak4PhaseDetection2DEdges(Weak4PhaseDetection2D):
    def __init__(self, **kwargs):
        self.colored_edges = kwargs.pop("colored_edges", False)
        kwargs["subplot_args"] = {}
        super().__init__(**kwargs)

    def prepare_plot(self):
        art = {"im": self.ax.imshow(np.ones((2, 2))),
               'overlay': Orderings2DBase.overlay_phase_diagram(self.ax, color='k')}
        return art

    def draw(self):
        results = self.get_results(drop_filter_keys=True)
        state_vals = self.state_vals

        if len(results) > 0:
            images, extents = self.get_images(results, self.data.labels.columns[0])

            edges = []
            for image in images:
                grad = np.array(np.gradient(image))
                edges.append(np.hypot(grad[0, ...], grad[1, ...]))

            if self.colored_edges:
                rgb_image = np.moveaxis(np.array(edges), 0, 2)[..., :3]
                rgb_image[np.isnan(rgb_image)] = 1
                self.art["im"].set_data(rgb_image)
            else:
                avg_gradient = np.mean(np.array(edges), axis=0)
                self.art["im"].set_data(avg_gradient)
                self.art["im"].set_clim(minmax(avg_gradient))

            self.art["im"].set_extent(extents[0])

    def overlay(self):
        visible_phase_diagram = self.state_vals["overlay_phase_diagram"]
        for o in self.ax:
            o.set_visible(visible_phase_diagram)