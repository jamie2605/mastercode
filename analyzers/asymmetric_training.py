from pandas import DataFrame
import numpy as np
import matplotlib.pyplot as plt

from core.analysis import Analyzer
from features import FeatureSet
from neural_networks import DenseModel
from tools.helper import find_crossing
from core.visualization import AnalyzerFigure


class AsymmetricTrainingAnalyzer(Analyzer):
    def __init__(self, feature_set: FeatureSet, threshold: float=2.269, threshold_column: str='T'):
        self.feature_set = feature_set
        self.threshold = threshold
        self.threshold_column = threshold_column

    def evaluate(self, test_begin: float, test_end: float):

        train, test = self.feature_set.split_label_value(test_values={self.threshold_column: (test_begin, test_end)})
        train_y = train.binary_single_2d_label(self.threshold)
        test_y = test.binary_single_2d_label(self.threshold)

        # reinitialize a new neural network
        nn = DenseModel(self.feature_set.n_features, model_parameters={'num_neurons': (100, 10)})
        nn.train(train.x, train_y, validation_data=(test.x, test_y))

        results = nn.evaluate_along_label_f(test, self.threshold)

        crossing = find_crossing(results["r0"].values,
                                 results["r1"].values,
                                 results.index.get_level_values(0).values)

        return {"crossing": crossing}


class AsymmetricTrainingFigure(AnalyzerFigure, AsymmetricTrainingAnalyzer):
    def __init__(self, **kwargs):
        self.add_state_parameter("rescale", values=[False, True], increase="r")
        super().__init__(**kwargs)

    def prepare_plot(self):
        art = {}
        cb = plt.colorbar(self.ax.imshow(np.empty((2, 2))))
        art["colorbar_ax"] = cb.ax
        return art

    def draw(self):
        results = self.get_results()
        self.ax.cla()
        self.art["colorbar_ax"].cla()

        if len(results) > 0:
            self.add_image(results)

    def add_image(self, results: DataFrame,
                  x_axis: str = 'test_begin', y_axis: str = 'test_end',
                  c_axis: str = 'crossing'):

        results = self.join_with_full_results(c_axis, results, x_axis, y_axis)

        # rescale
        if self.state_vals["rescale"] and hasattr(self, "threshold"):
            threshold = self.threshold

            index_columns = results.index.names
            results.reset_index(inplace=True)
            results[[c_axis]] -= threshold
            results.set_index(index_columns, inplace=True)
            rescale = True
        else:
            rescale = False

        image = results[c_axis].unstack(level=x_axis)
        image.sort_index(inplace=True)
        image.sort_index(axis=1, inplace=True)

        self._draw_image(image, rescale, x_axis, y_axis)

    def _draw_image(self, image, rescale, x_axis, y_axis):
        # calculate correct extent, max is for border case of only one value per axis
        # assuming linear spacing in parameters
        x = image.columns
        x_spacing = x[min(len(x) - 1, 1)] - x[0]
        y = image.index
        y_spacing = y[min(len(y) - 1, 1)] - y[0]
        extent = (x[0] - x_spacing, x[-1] + x_spacing,
                  y[0] - y_spacing, y[-1] + y_spacing)
        vmin = np.nanmin(image.values)
        vmax = np.nanmax(image.values)
        cmap = 'viridis'
        if rescale:
            vmax = max(-vmin, vmax)
            vmin = min(vmin, -vmax)
            cmap = 'Spectral'
        # decoration

        im = self.ax.imshow(np.flipud(image.values), extent=extent,
                            vmin=vmax, vmax=vmin,
                            cmap=cmap)

        self.ax.set_xlabel('Begin of test data interval ($h_{begin}$)')
        self.ax.set_ylabel('End of test data interval ($h_{end}$)')

        if rescale:
            label = 'Bias in predicted critical point ($h_{pred} - h_c$)'
        else:
            self.ax.set_xlabel(x_axis)
            self.ax.set_ylabel(y_axis)
            label = 'predicted critical point'
        plt.colorbar(im, cax=self.art["colorbar_ax"], label=label)

    def join_with_full_results(self, c_axis, results, x_axis, y_axis):
        # select results
        results = results[[c_axis]]
        results = results.groupby(level=[x_axis, y_axis]).mean()
        # join with full parameter space (including uncompleted results) for still image during execution
        index = self.get_results(df=self._data_frame_int).index
        index.droplevel(level=[l for l in index.names if l not in [x_axis, y_axis]])
        index_names = index.names.copy()
        index = index.unique()
        index.names = index_names
        param_space = DataFrame(index=index)
        results = param_space.join(results)
        return results
