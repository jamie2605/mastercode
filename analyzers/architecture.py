from typing import Tuple

from pandas import DataFrame

from analyzers.network_response_basics import ResponseAnalyzerFixedFeatureSet, ResponseFigureBase
from neural_networks import DenseModel


class DenseNetworkArchitectureAnalyzer(ResponseAnalyzerFixedFeatureSet):
    def evaluate(self,
                 num_neurons: Tuple[int] = None,
                 train_epochs: int = 5,
                 batch_size: int = None) -> DataFrame:
        """

        :param batch_size:
        :param train_epochs:
        :param num_neurons: number of neurons to use in each layer
        :return: accuracy, std and responses along label axis as DataFrame
        """
        test = self.test_data
        train = self.train_data
        y_train = self.train_labels
        y_test = self.test_labels

        nn = DenseModel(self.train_data.n_features, model_parameters={'num_neurons': num_neurons})
        nn.train(train.x, y_train, epochs=train_epochs, batch_size=batch_size,
                 validation_data=(test.x, y_test))

        return nn.evaluate_along_label_f(test, self.threshold)


class DenseNetworkFigure(ResponseFigureBase, DenseNetworkArchitectureAnalyzer):
    def __init__(self, **kwargs):
        self.add_state_parameter_filter('sweep', 'tab')
        super().__init__(**kwargs)

    def draw(self):
        results = self.get_results()
        self.ax.cla()

        if len(results) > 0:
            self.add_responses_plot(results, group_along=['num_neurons'])
