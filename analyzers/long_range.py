from typing import Dict, Tuple

from analyzers.network_response_basics import ResponseFigureBase, VariableFeatureSetAnalyzer


class InteractionAnalyzer(VariableFeatureSetAnalyzer):
    def evaluate(self, interaction: Tuple[int, float], num_neurons: Tuple[int] = (50, 10),
                 train_epochs: int = 5,
                 batch_size: int = None):
        return super().evaluate(interaction, num_neurons, train_epochs, batch_size)


class LongRangeResponseFigure(ResponseFigureBase, InteractionAnalyzer):
    def __init__(self, **kwargs):
        self.add_state_parameter_filter("interaction", "tab", "ctrl+tab")
        kwargs["threshold_column"] = 'h_x'
        kwargs["threshold"] = 1.0
        super().__init__(**kwargs)

        self.update_state_by_value({'display_threshold': False}, draw=True)

    def draw(self):
        super().draw()

        self.ax.set_title(self.state_vals["interaction"])
