from typing import Tuple, Any, Dict

from analyzers.network_response_basics import NetworkResponseAnalyzerFixed, ResponseFigureBase, ResponseAnalyzerBase
from features import FeatureSet
from neural_networks import DenseModel


class ManualModelMixingFigure(NetworkResponseAnalyzerFixed, ResponseFigureBase):
    pass


class ModelMixingAnalyzer(ResponseAnalyzerBase):
    def __init__(self, feature_sets: Dict[Any, FeatureSet],
                 test_interval: Tuple[float, float] = (0.3, 1.7),
                 threshold: float=1, threshold_column: str = 'h_x', **kwargs):
        """

        :param feature_sets: Dict of Feature sets, each belonging to a model identified by key
        :param test_interval: where to split into train/test data
        :param threshold: where to split label
        :param threshold_column: which label column to use for split
        :param kwargs:
        """
        self.threshold_column = threshold_column
        self.threshold = threshold

        data = {key: feature_set.split_label_value(test_values={self.threshold_column: test_interval})
                for key, feature_set in feature_sets.items()}

        self.train = {k: d[0] for k, d in data.items()}
        self.test = {k: d[1] for k, d in data.items()}
        self.y_train = {k: f.binary_single_2d_label(threshold, threshold_column) for k, f in self.train.items()}
        self.y_test = {k: f.binary_single_2d_label(threshold, threshold_column) for k, f in self.test.items()}

        super().__init__(**kwargs)

    def evaluate(self, use_train: str, use_test: str):
        train = self.train[use_train]
        test = self.test[use_test]
        y_train = self.y_train[use_train]
        y_test = self.y_test[use_test]

        nn = DenseModel(train.n_features, {'num_neurons': (100,)})
        nn.train(train.x, y_train, validation_data=(test.x, y_test), verbose=0)
        return nn.evaluate_along_label_f(test, self.threshold, self.threshold_column)


class ModelMixingFigure(ModelMixingAnalyzer, ResponseFigureBase):
    def __init__(self, **kwargs):

        self.add_state_parameter_filter('use_test', "ctrl+up", "ctrl+down")

        self.filter_by_train = False
        self.add_custom_keymap('g', self.toggle_grouping)

        super().__init__(**kwargs)

    def toggle_grouping(self):
        """
        toggle which axis to filter and which to group by
        :return:
        """
        self.filter_by_train = not self.filter_by_train

        if self.filter_by_train:
            self.add_state_parameter_filter('use_train', "up", "down")
        else:
            self.remove_state_parameter('use_train')

    def draw(self):
        results = self.get_results()
        self.ax.cla()

        if len(results) > 0:
            if self.filter_by_train:
                self.add_responses_plot(results)
            else:
                self.add_responses_plot(results, group_along=['use_train'])

        s = self.state_vals

        if self.filter_by_train:
            self.ax.set_title("train on {}, test on {}".format(s["use_train"], s["use_test"]))
        else:
            self.ax.set_title("test on {}".format(s["use_test"]))

        self.ax.set_xlim((0.85, 1.15))
