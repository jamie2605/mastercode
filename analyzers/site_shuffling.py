from typing import Union

from pandas import DataFrame
import numpy as np

from analyzers.network_response_basics import ResponseAnalyzerFixedFeatureSet, ResponseFigureBase
from neural_networks import DenseModel


class SiteShufflingAnalyzer(ResponseAnalyzerFixedFeatureSet):
    def evaluate(self,
                 shuffle_mode: Union[None, int, str],
                 select_sites: int = None) -> DataFrame:
        """

        :param shuffle_mode: how to shuffle FeatureSet (See FeatureSet.shuffle)
        :param select_sites: train and test only on first select_sites, If None: use all sites
        :return:
        """
        if select_sites == 0:
            select_sites = self.train_data.n_features
        assert select_sites <= self.train_data.n_features
        select_sites = int(select_sites)

        if shuffle_mode == 'no shuffle':
            test = self.test_data[:, :select_sites]
            train = self.train_data[:, :select_sites]
        elif shuffle_mode == "random site selection":
            # same selection for train and test
            sites = np.random.choice(np.arange(self.train_data.n_features, dtype=int), size=select_sites)
            test = self.test_data[:, sites]
            train = self.train_data[:, sites]
        elif shuffle_mode == "random spin selection":
            test = self.test_data.shuffle('row-wise')[:, :select_sites]
            train = self.train_data.shuffle('row-wise')[:, :select_sites]
        elif shuffle_mode == "full":
            test = self.test_data.shuffle('full')[:, :select_sites]
            train = self.train_data.shuffle('full')[:, :select_sites]
        else:
            raise KeyError("Unknown shuffle mode {}.".format(shuffle_mode))

        y_train = train.binary_single_2d_label(self.threshold, self.threshold_column)
        y_test = test.binary_single_2d_label(self.threshold, self.threshold_column)

        nn = DenseModel(train.n_features, model_parameters={'num_neurons': (100,)})
        nn.train(train.x, y_train, validation_data=(test.x, y_test))

        return nn.evaluate_along_label_f(test, self.threshold, self.threshold_column)


class DataShufflingFigure(ResponseFigureBase, SiteShufflingAnalyzer):
    def __init__(self, **kwargs):

        self.group_along = 'select_sites'
        self.filter_by = 'shuffle_mode'
        self.toggle_mode()

        self.add_custom_keymap('m', self.toggle_mode)

        super().__init__(**kwargs)

    def toggle_mode(self):
        """
        toggle which axis to filter and which to group by
        :return:
        """
        self.remove_state_parameter(self.filter_by, error_on_noexist=False)
        self.filter_by, self.group_along = (self.group_along, self.filter_by)
        self.add_state_parameter_filter(self.filter_by, "tab", "tab+shift")

    def draw(self):
        results = self.get_results()
        self.ax.cla()

        if len(results) > 0:
            self.add_responses_plot(results, group_along=[self.group_along])

        self.ax.set_title("{}: {}".format(self.filter_by, self.state_vals[self.filter_by]))
