from typing import Tuple

from analyzers.network_response_basics import ResponseFigureBase, VariableFeatureSetAnalyzer
import numpy as np


class LScalingAnalyzer(VariableFeatureSetAnalyzer):
    def evaluate(self, system_size: int, num_neurons: Tuple[int] = (50, 10),
                 train_epochs: int = 5,
                 batch_size: int = None):
        return super().evaluate(system_size, num_neurons, train_epochs, batch_size)


class LScalingFigure(ResponseFigureBase, LScalingAnalyzer):
    def __init__(self, **kwargs):
        """
        From a dict of features sets for different feature sizes, train a simple dense neural network
        and plot the output over label or over a scaled label
        :param kwargs: See VariableFeatureSetAnalyzer, ResponseFigureBase and Batcher __init__
        """
        self.add_state_parameter("rescale", values=[True, False], increase="r")
        super().__init__(**kwargs)

    def draw(self):
        results = self.get_results()
        self.ax.cla()

        if len(results) > 0:
            if self.state_vals["rescale"]:
                # rescale index
                temperature = results.index.get_level_values(self.threshold_column)
                system_sizes = results.index.get_level_values('system_size')

                results["tL"] = (temperature - self.threshold) * np.sqrt(system_sizes)**(1/1)
                results.set_index('tL', append=True, inplace=True)

                self.add_responses_plot(results, x_axis='tL', group_along=['system_size'])
            else:
                self.add_responses_plot(results, x_axis=self.threshold_column, group_along=['system_size'])

    @property
    def threshold_scaled(self):
        if self.state_vals["rescale"]:
            return 0
        else:
            return super().threshold_scaled
