\documentclass[main.tex]{subfiles}
\begin{document}
\chapter{Machine learning for many-body physics}

Finding structure in nature has always been at the heart of science. By closely observing the behavior of a system, patterns can be found and rules can be formulated to describe these patterns. Over time, science has not only developed very elaborate models about many phenomena in nature, but also acquired a large toolbox of methods to effectively improve these models even further.

This toolbox consists of many parts, advanced theories may require advanced mathematics, observing the most delicate phenomena requires the most delicate measurement instruments and a variety of tools were established for recording, analyzing, visualizing and evaluating the always-growing amount of measurement data.

%There has always been a feedback cycle between science, mathematics and technology, with new discoveries in science enabling new technologies which in turn may make new experiments feasible, yielding to results and models that may only be described by new fields of mathematics, creating new discoveries to restart the cycle. Without optics, telescopes would not have been invented and astronomy would not exist. Without the discovery of semiconductors, CCD cameras would not exist and without knowing about stimulated emission, lasers would not be possible. Most modern quantum experiments rely on all 3 of the above technologies.

The progress in computer technology has made it much cheaper to generate measurement data by automating experiments. Additionally, computers speed up the analysis of this data, using models that scientists have developed beforehand and are usually very experiment-specific. Performing such an analysis is limited in the way that for the most part, it can only ever determine whether the data agrees with the patterns the scientist was looking for when developing the model. The model is essentially an explicit transformation function of the points in the high-dimensional measurement space onto some 1, 2 or 3-dimensional space to plot. 

This method is very successful at testing hypotheses, a process fundamentally important for the scientific method. However, to aid scientists formulate new hypotheses, it would be interesting to have a more general method of finding structure in the more and more complicated measurement data acquired in modern experiments. The scientific toolbox of physics is not yet well equipped for such a task.

As we look into the technology sector, many similar problems of finding structure in data have recently been solved by employing tools from the domain of machine learning. Due to the large interest and money invested by technology companies into this area, there are very mature libraries for machine learning available and it therefore was only a question of time when the machine learning was applied to scientific data. 

\vspace{5mm}

Machine learning as a whole has seen both tremendous progress and widespread attention from both the scientific community, the technology sector as well as from the general public. Providing an overview over this large, quickly-growing research field is well beyond the scope of this thesis. Therefore, only a very selected outline of the machine learning terminology and tools used in this thesis will now be given. For more details, refer to the standard literature such as \cite{Bishop2006, Barber2012, Murphy2012}

Mathematically, the task of machine learning can be defined as the following optimization problem: Given a set of measurements that are arranged in a Matrix $X$ of dimensionality $N\times D$ with $N$ being the number of measurement sets and $D$ being the number of different quantities - \emph{features} - measured in each set, as well as a response $y$, machine learning is the task of finding the best mapping function $f$ such that $$ X \xrightarrow{ f } y$$
$f$ is a model-dependent function that usually contains many optimization parameters. The distance between $f(X)$ and $y$ is quantified by a \textit{loss function}. A familiar example of a loss function in the context of regression tasks is the residual sum of squares.

In a \textit{supervised learning} setup, both $X$ and $y$ are known for some measurement sets. Once the loss is optimized on a set of \textit{training data}, the model can predict $y$ on previously unseen data $X$. Using a set of \textit{test data} ($X_{train}$, $y_{train}$), $f(X_{train})$ is evaluated and the accuracy is measured by comparing the prediction to the known, true $y_{train}$. If $y$ is element of some finite set, this task is called \textit{classification} and $y$ is known as \textit{label} in this context. The label indicates to which \emph{class} $y$ is predicted to belong to.

In an \textit{unsupervised learning} setup, $y$ is not known. Instead, the task is as general as finding structure in the high-dimensional vector space spanned by the training set. Unsupervised learning is often used for dimensionality reduction, visualization or finding clusters.\footnote{These tasks have a large overlap with each other}. We will see an example of unsupervised learning for visualization in Section \ref{chap:classical:results} by use of the t-SNE algorithm as developed by \cite{VanDerMaaten2008}.

\begin{figure}[h!]
	\centering
	\includegraphics{figures/nn}
	\caption{Typical graphical representation of a fully connected neural network with an input layer of 5 neurons, 1 hidden layer with 3 neurons and an output layer of 2 neurons}
	\label{fig:nn}
\end{figure}

An example of a well-known supervised model is the family of neural network models. Modern forms of these models such as convolutional neural networks (CNN) or recurrent neural networks (RNN) have proven to be very successful in image recognition, natural language processing, speech recognition, speech synthesis\footnote{See \cite{VandenOord2016} for the impressive voice synthesis of WaveNet} or various artificial intelligence tasks such as the very famous AlphaGo project described in \cite{Silver2016} which was used to beat world champions in Go, a game that is considered very difficult for artificial intelligence. For a detailed overview of the developments of neural networks and the achievements resulting from each of these developments, see \cite{Schmidhuber2014}. We will only introduce fully connected, feed-forward neural networks for reasons explained in Section \ref{chap:classical:results}. Such a network consists of many \textit{perceptrons}, represented by circles in Figure \ref{fig:nn}. Each neuron has arbitrary many inputs and one output. A weighted sum over the input signal vector $X_i$ is passed to a non-linear activation function $\phi(\sum_iX_iw_i+b)$ before passing the result on to the next layer of neurons. During training, the weights $w$ and the bias $b$ of each neuron is adjusted by \textit{backpropagation}, which essentially is gradient descent: The network is initialized with random weights and evaluated using training data. The difference between the outputted $y$ and $y_{train}$ is quantified using the loss function and the network weights are adjusted according to the loss function's gradient (multiplied by a \textit{learning rate} $\tau$). This process is repeated until the network has converged.



\vspace{5mm}

This thesis focuses on the topic of \emph{machine learning for quantum systems}, i.e. applying machine learning to a quantum system to find structure within its behavior. We clearly contrast this approach to the domain of quantum machine learning. There, theoretic approaches are investigated to run a machine learning algorithm on a quantum system by encoding the optimization goal into the Hamiltonian of a quantum system for a speed-up in computation time. For a recent review of advances in that field, see \cite{Schuld2015}.

Many-body quantum systems with their exponential complexity in particle number for simulation and exponential size of the Hilbert space are seen as an application where machine learning can excel. Currently, there are two main developments in the field of machine learning for quantum systems. The first approach is to use machine learning to directly encode the wave function into the optimization goal of the machine learning algorithm, using the variational principle to minimize the loss function and find the ground state. This approach is described in \cite{Carleo2016a}. We will instead follow an approach introduced by \cite{Carrasquilla2017}, which is a purely-observable based method to find phase transitions of a many-body system. 

While details of the method can be found in the cited paper, an overview over the approach is now presented. Chapter \ref{chap:classical:results} reproduces results from the cited paper, later chapters utilize the same general methodology on different systems. 

A neural network is used to distinguish between 2 phases of matter. It is trained on Monte-Carlo sampled observables - such as microscopic spin configurations - along with a binary label $y$ indicating which phase a given set of observables belongs to. The network is evaluated new sets of Monte-Carlo sampled data and the response of the 2 output neurons recorded. Each neuron response represents the confidence of observing the corresponding phase.

It was also found in \cite{Carrasquilla2017} that the neuron response curves scale with the critical exponent $\nu$ in system length, at least for the classical Ising model.

\vspace{5mm}
The thesis is structured as follows: We first apply the observable-based approach to classical 2D Ising model in Chapter \ref{chap:classical} before entering the quantum domain in form of the Ising chain in Chapter \ref{chap:tfi}. Here, we will also compare the approach to an alternative method of finding structure in data automatically as provided by network science and introduced by \cite{Valdez2017}. Finally, we analyze the XXZ-model in Chapter \ref{chap:xxz}, which is no longer analytically solvable and explore its 2-dimensional phase diagram in both an unsupervised method as described in \cite{Nieuwenburg2017} as well as a newly introduced weakly supervised method.

\end{document}