\documentclass[main.tex]{subfiles}
\begin{document}
	%\begin{appendix}
	\appendix
		\chapter*{Appendices}
		\addcontentsline{toc}{chapter}{Appendices}
		
		\renewcommand{\thesection}{\Alph{section}}
		\renewcommand\thefigure{\thesection.\arabic{figure}}
		\renewcommand\theequation{\thesection.\arabic{figure}}
		
		\section{Phase detection by alternative machine learning methods}
		\label{app:othermethods}
		
		While most current efforts of introducing machine learning to aid understanding of quantum systems focus on neural networks, albeit in many variations from vanilla fully-connected feed-forward networks over convolutional variants and restricted Boltzmann machines to variational autoencoders, this only encompasses a very small domain of machine learning.
		
		Neural networks have proven to be a very powerful tool for many real-world applications and generally perform well on a variety of classification and regression problems, especially if an abundance of computation time and training data is available.
		
		Some current works of machine learning on many body systems have included other standard machine learning analyses, including both supervised methods such as support vector machines employed in \cite{Ponte2017} as well as unsupervised methods such as PCA in \cite{Hu2017}) models. With many of the recent models available in modern libraries such as sklearn by \cite{sklearn} providing consistent APIs across all models, very little effort is required to perform model selection. At the very least, knowledge about the limitations of each tested model are learned.
		
		To generally evaluate the performance of these models, we use 2 new measures of performance since neuron response is a model-specific quantity to neural networks. They are accuracy and standard deviation over the test set's predicted label. Accuracy is a measure that can only be evaluated in a supervised setup with training labels over the whole test set, which may not always be the case for this application. Standard deviation over the test set is always available and intuitively will peak at the phase boundary as the network is most confused at that location. For a perfect classifier however, it will be flat zero. We will see that we are not in that limiting case.
		
		We will not discuss each model individually but rather present the results as a whole. Figure \ref{fig:app:classicalmlnot} displays the results of classifiers that are not able to distinguish the two classes. They are all linear models, meaning the two classes ferro- and paramagnetic are not linearly separable, which is to no surprise.
		Figure \ref{fig:app:classicalmlworking} displays results of classifiers that are able to distinguish the two phases. The best model interestingly is SVC, which is the same as the support vector machine implemented in LinearSVC, but with the radial-basis functions as kernel function.  Apparently, within the space resulting from this transformation, the two classes are  linearly separable. It is the only model offering a precision in the prediction of the critical temperature similar to neural networks. We are aware that there is work on using support vector machines and the kernel trick on this problem in \cite{Ponte2017} as means for a more interpretable machine learning. See also appendix \ref{app:whatislearned} for more information on the topic of interpretable machine learning.
		
		To test other models or parameterizations, we refer to the notebook generating these plots which is contained in the supplied code (See appendix \ref{app:Code}, navigate to \texttt{summary/} \texttt{1\_classical\_ising/}\texttt{3\_classical\_ML.ipynb}).
		
		%\todo{add figures from notebook file}
		
		\begin{center}
			\begin{figure}[h!]
				\makebox[\textwidth]{\makebox[0.95\paperwidth]{
						\begin{subfigure}[b]{0.4\paperwidth}
							\includepdf{\textwidth}{QuadraticDiscriminantAnalysis}
						\end{subfigure}
						\begin{subfigure}[b]{0.4\paperwidth}
							\includepdf{\textwidth}{LinearSVC}
						\end{subfigure}
				}}
				
				
				\makebox[\textwidth]{\makebox[0.95\paperwidth]{
						\begin{subfigure}[b]{0.4\paperwidth}
							\includepdf{\textwidth}{AdaBoostClassifier}
						\end{subfigure}
						\begin{subfigure}[b]{0.4\paperwidth}
							\includepdf{\textwidth}{GaussianNB}
						\end{subfigure}	
				}}
				\caption{Accuracy and standard deviation of the predicted label (std) vs. temperature T for machine learning models failing to distinguish the 2 phases of the classical 2D Ising model when trained on spin configurations. Subplot titles are class names as in the sklearn library (\cite{sklearn}), refer to their documentation for details on models and parameters. The vertical black line indicates the critical temperature in the thermodynamic limit.}
				\label{fig:app:classicalmlnot}
				
			\end{figure}
		\end{center}
	
		\begin{center}
			\begin{figure}[h!]
				\makebox[\textwidth]{\makebox[0.95\paperwidth]{
						\begin{subfigure}[b]{0.4\paperwidth}
							\includepdf{\textwidth}{KNeighborsClassifier}
						\end{subfigure}
						\begin{subfigure}[b]{0.4\paperwidth}
							\includepdf{\textwidth}{SVC}
						\end{subfigure}
				}}
				
				
				\makebox[\textwidth]{\makebox[0.95\paperwidth]{
						\begin{subfigure}[b]{0.4\paperwidth}
							\includepdf{\textwidth}{DecisionTreeClassifier}
						\end{subfigure}
						\begin{subfigure}[b]{0.4\paperwidth}
							\includepdf{\textwidth}{RandomForestClassifier}
						\end{subfigure}	
				}}
				
				\caption{Accuracy and standard deviation of the predicted label (std) vs. temperature T for machine learning models that are successful at the task of classification of the 2 phases of the classical 2D Ising model when trained on spin configurations. Subplot titles are class names as in the sklearn library (\cite{sklearn}), refer to their documentation for details on models and parameters. The vertical black line indicates the critical temperature in the thermodynamic limit.}
				\label{fig:app:classicalmlworking}
			\end{figure}
		\end{center}
	
		\setcounter{figure}{0}
		\clearpage
		\section{Phase detection by convergence time}
		\label{app:runtimedetection}
		
		When running the two-site variational ground state search of the Matrix Product states, it was noted that the convergence time varied greatly and had a strong dependence on the Hamiltonian's parameters. To investigate, the convergence times were recorded.\footnote{Wall time, not CPU time} These times were plotted in Figure \ref{fig:app:runtime}. 
		
		In case of the quantum Ising model, there is a sharp peak at the critical value of $h_c = 1$. The peak width may be larger than the neuron crossing regime of the machine learning method, but no bias could be observed with this method.
		
		For the XXZ model in the second pane, the relation between convergence time and phase boundary is slightly different. Rather than peaking at phase borders, the different phases of the system seem to have different typical convergence time. Interestingly, the z-neel-ordered phase of high $\Delta$ seems to be very costly, while the z-ordered phase of high negative $\Delta$ is very cheap to simulate. On top of that, it is almost indistinguishable to the phase of high $h_x$. This phase border is the most easily distinguishable one by the machine learning method. Contrarily, the phase boundary of highest contrast for this method i.e. between low and high positive $\Delta$-values is most difficult to identify using the introduced machine learning method.
		
		\begin{figure}[h]
			\centering
			\begin{subfigure}{0.7\textwidth}
				\includegraphics[width=\textwidth, trim=2cm 0 0 0]{\figpath{15_1_20180413-150744-181427_edit.pdf}}
				\caption{Quantum Ising chain of 200 particles}
			\end{subfigure}
			\begin{subfigure}{0.8\textwidth}
				\includegraphics[width=\textwidth]{\figpath{21_1_20180622-102453-661446.pdf}}
				\caption{XXZ-model of 700 particles}
			\end{subfigure}
			\caption{Time until convergence of the MPS algorithm for different sets of coupling strengths}
			\label{fig:app:runtime}
		\end{figure}
	
		\clearpage
		\setcounter{figure}{0}
		\section{Phase detection without bias}
		\label{app:biasfix}
		It has been noted in Section \ref{chap:tfi:results} that training deep within the phases of a quantum systems introduces a small bias that will likely persist even for a very large system size and only depends on the nature of the phase transition itself. In the case of the quantum Ising model, the neural network is not able to assign realizations of the system with a small, but nonzero magnetization the label "ferromagnetic" when only being trained on magnetizations very close to 0 and 1. The interpolation fails, or more precisely: it produces undesirable results.
		
		We have seen in Section \ref{chap:tfi:noise} that adding noise reduces this bias as a larger range of observed magnetizations is present during training. We have also noted that this setup is unfavorable from a machine learning perspective. Within the region of interest, the feature space should be sampled as evenly as possible, which is what we are about to perform.
		
		
		
		It is now assumed that a labeled set of training data exists with training points across the entire domain. Once again, this training set could also be obtained iteratively: First we train the neural network as before, deep within the phases and obtain a biased guess of the critical point. The gap between the 2 phases is then successively decreased, resulting in a bias decrease.
		
		
		
		The results are given in \ref{fig:app:biasfix}. The bias can be reduced below the resolution of the magnetic field axis and the network response is very close to a perfect classifier.
		
		\begin{figure}[bht]
			\centering
			\includepdf{0.8\textwidth}{4_3_BiasLessFigure_1_20181024}
			\caption{Phase detection when sampling training data across the entire parameter space. Error bars are smaller than depicted plot points. Once again r0 and r1 refer to the response of the ferromagnetic and paramagnetic neuron, respectively. The blue line indicate neuron crossing and the black line indicates the critical field value as predicted by theory.}
			\label{fig:app:biasfix}
		\end{figure}
	\newpage
		
		\paragraph{Minimal training data} Within this context of bias-less learning of the phase transition, we also want to answer the question of how many training points are needed for the network to be able to predict the phase transition. We sampled a small number of training points homogeneously across the entire domain of magnetic field values and recorded the responses in \ref{fig:app:limited}.
		
		\begin{figure}[bthp]
			\centering

			\includepdf{0.8\textwidth}{1_9_MinimalDataFigure_1_20181024}
			\caption{Phase detection when sampling training data across the entire parameter space. The response of the ferromagnetic and paramagnetic neuron are denoted by triangles pointing to the left and right, respectively. Color indicates number of training points - correlation functions - used during training. The vertical colored lines correspond to the crossings of the corresponding neuron responses, the black line to the critical field value as predicted by theory.}
			\label{fig:app:limited}
		\end{figure}
	
		We observe
		 that even with as little as 5 training points the network is successful at distinguishing the phases, at the cost of a lower probability for a network to converge during training. Additionally, the contrast in response between the 2 neurons is decreasing for fewer training points. It seems that the additional training points are mostly used to identify the structure of the paramagnetic phase. 500 training points appear to be a good number of training points if high contrast in the paramagnetic phase is needed. The bias in predicted location of the critical point is still within the resolution of the magnetic field axis, even for just 5 training points.
		
		\clearpage
		\setcounter{figure}{0}
		\section{Entropy estimation with gzip}
		\label{app:entropy}
		
		\cite{Onsager1944} has calculated the entropy $S$ of the 2D Ising model on an infinite lattice with only nearest neighbor interactions to be
		\begin{equation}
		 S \propto \int^{2\pi}_0 d\theta_1 \int^{2\pi}_0 d\theta_2 \frac{\sinh(2 \beta J) - (\cos(\theta_1)-\cos(\theta_2))}{\cosh(2\beta J)^2-\sinh(2\beta J)(\cos(\theta_1)-\cos(\theta_2))}
		\label{eq:entropy}
		\end{equation}
		with inverse temperature $\beta$.
		
		From a computer science perspective, entropy can be interpreted as amount of information a given message contains. One measure to gauge this is the Kolmogorov complexity, also known as algorithmic entropy. For a given message, it is defined by the length of the shortest program that can produce the message. While in practice it will be almost impossible to manually design such a program, let alone prove it to be minimal, an empiric method to estimate Kolmogorov complexity on a given message lies in lossless compression algorithms such as Huffman coding or more modern variants such as LZ77 introduced in \cite{Ziv1977}. These aim at finding a minimal representation of a given piece of data which should then be roughly proportional to the Kolmogorov complexity.
		
		\begin{figure}[ht]
			\centering
			\includegraphics[width=0.82\textwidth]{\figpath{1_1_entropy_file_size.png}}
			\caption{Thermodynamic entropy and file size of the spin configurations when compressed vs. temperature for the classical 2D Ising model. The spin configurations were sampled on a finite 16x16 model, the entropy is for a 2D Ising model on an infinite lattice. The vertical blue line marks the critical temperature $T_c$ in the thermodynamic limit.}
			\label{fig:entropy}
		\end{figure}
	
		To investigate if there is a correlation between thermodynamic entropy and information content of the sampled spin configurations, the compression library gzip\footnote{\url{www.gzip.org}} was used and for each temperature, the corresponding spin configurations sampled from the classical Ising models were compressed. The file size of these files was plotted against temperature in Figure \ref{fig:entropy}, together with the thermodynamic entropy obtained by numerically integrating equation \ref{eq:entropy}. Please note that the y-axis is scaled arbitrarily as both quantities are only relative measures of entropy.
		
		While the two quantities are not directly proportional to each other, there definitely is a very visible correlation between the 2 measures. Some deviation can probably be attributed to gzip not finding the minimal representation, but most of it is probably due to the fact that the spin configurations were sampled of a finite 16x16 lattice while the analytic expression for the thermodynamic entropy is only valid for an infinite lattice. If a larger lattice were to be sampled, the curves will become even more similar.
		
		\clearpage
		\setcounter{figure}{0}
		\section{What does the neural network learn?}
		\label{app:whatislearned}
		
		Making sense of how any machine learning model distinguishes classes from each other is an active area of research. For some simpler models the explicit form of the decision function is enough to manually infer what is learned. For linear classifiers\footnote{The decision function of a linear classifier has the features only as linear arguments, weighted by weights $w$ is $y=f(\sum_iw_ix_i)$} for example, the decision function is given by a simple hyperplane, once the kernel trick is involved, this plane may be warped, but the explicit form of the decision function remains simple. Unfortunately these linear models all assume that the feature space - transformed by kernel trick or not - is linearly separable.
		
		As soon as that is no longer the case, it becomes much more difficult to interpret the decision function of a trained model. While for neural networks, the network function is very simple to express in principle, it generally contains too many parameters to make sense in a manual evaluation. In the context of image recognition and convolutional neural networks, the filter weights are often examined and found to contain simple shapes in higher layers and more complicated structures in lower layers. 
		
		One approach applicable to any machine learning model is a variational search for the most typical member of a class. A random training vector $x$ is varied to maximize the response $y$ for a given class. Once this maximization completes, the input vector should be a class representative, describing how the network actually generalizes this class. Unfortunately, this is not always the case. This method has also been attempted on the image recognition task and when starting from an image of random noise, the maximized image may also look like noise to the human eye, but the network is certain to see a dog in this second image (\cite{Nguyen2015}). For this application, constraining the variations in the input image to have image-like features and statistics makes the class-representative search possible, but it is not quite clear how this translates to other applications.
		
		As a sidenote and for the readers enjoyment we recommend the results of a successful image maximization and refer to the "Dreaming neural networks" approach by Google\footnote{\url{https://ai.googleblog.com/2015/06/inceptionism-going-deeper-into-neural.html}}. There are 2 directions to where this approach is headed for image recognition: art and security. The former because the network can use the image of a dog as initial guess for a tree and let the network design a dog tree or use a photo as input and raw a painting in the style of an artist\footnote{\url{http://deepdreamgenerator.com}}. The latter because neural networks are also used in security-critical applications such as driving cars. There is an emerging field of machine learning dealing with the possibilities and implications of adversarial manipulation of machine learning systems. In \cite{Sabour2015}, images are subtly manipulated to be misclassified to a class of the intruder's choice.  While this method was limited to a white-box setting, i.e. the intruder has direct access to the in- and output of the machine learning algorithm, there also are approaches without this assumption, \cite{Eykholt2017} for example deals with the possibility of manipulating street signs in the physical world, disguised as graffiti, to purposely force a misclassification for an autonomous car, resulting in ignored/added stop signs or speed limits. All this work shows that at least for image classification, the neural network interprets images in a very different way than humans do, making interpretability even more challenging.
		
		Coming back to the problem of classifying phases, what research has been performed to find out what the network learns?
		
		It turns out there have been multiple attempts of doing so, and they fall into 3 categories. 
		
		The first approach is the weight investigation. A convolutional neural network is trained and the filter weights are analyzed for patterns. Such an investigation has been carried out in \cite{Mills2018} for the classical Ising model, and neither there nor in an own attempt did such an analysis succeed and produce meaningful patterns. Since convolutional neural network layers are always followed by at least one more fully connected hidden layer and we have shown in Section \ref{chap:classical:results} that a single hidden layer is enough to classify the phases, it is unlikely for the neural network to use the filter weights in an interpretable way.
		
		The second approach can be found in \cite{Wetzel2017b}, also on the classical Ising model. The network architecture is strongly constrained, allowing for a very short, explicit formulation of the network function, which then turned out to be the magnetization. It is unclear how this approach is generalizable to a larger class of systems/order parameters.
		
		The third approach is described in \cite{Wetzel2017} and employs variational autoencoders. These are neural network architectures with a bottleneck in the middle called latent representation, i.e. a layer with very few neurons compared to the input layer. The input and output layer contain the same number of neurons. The hidden layers between input layer and bottleneck are called encoder, the other hidden layers decoder. The network weights are optimized such that the output of the network is equal to the input. Therefore, the encoder must find an efficient representation of the input data to pass through the bottleneck to convey the maximum amount of information to the decoder. When using an autoencoder with only one neuron for the latent representation for phase classification on the 2D classical Ising model, the one-dimensional latent parameter is proportional to magnetization. On top of that, the encoded output layer is uniform of the magnetization value for the ferromagnetic model and chessboard-like for the antiferromagnetic Ising model. (See figure 6 of \cite{Wetzel2017}).
		
		Overall we conclude that interpretable machine learning is still an open question and topic of current research in the machine learning community and is not solved in general. Once again, there is a trade off between quality/robustness of the method results and domain knowledge. It is possible to restrict to simple models which are more easily interpretable and to tune these models with domain knowledge to still provide acceptable results at the expense of loss of generality of models and methods.
		
		\clearpage
		\setcounter{figure}{0}
		\section{Using the supplied code}
		\label{app:Code}
		
		The code used for all analyses of this thesis is available at \url{https://bitbucket.org/jamie2605/mastercode/}, free to use with attribution. The following is a short guide on how to use the code.
		
		To reproduce the plots of this master thesis and see sample usage of all the classes implemented for this project, navigate to the \texttt{summary} folder. The subfolders correspond to the chapters of this thesis and contain notebook-like short scripts, encompassing every analysis performed in this thesis. The code should work on any Python 3.7+ with numpy, matplotlib, pandas, TensorFlow, keras, sklearn, OSMPS, qutip and skcuda installed. For Monte-Carlo Markov Chain simulations with external libraries such as ALPS \cite{Bauer2011} or the library by Lukas Kades\footnote{Theoretical physics institute Heidelberg, l.kades@thphys.uni-heidelberg.de}, these need to be adapted and compiled.
		
		Since exact diagonalization, Monte-Carlo Markov Chain and Matrix Product State simulations, as well as training neural networks are computationally intensive tasks, a 3 step process was devised to keep experimentation productive. The most demanding tasks where offloaded to the GPU\footnote{Such as exact diagonalization or training neural networks} or parallelized\footnote{Usually simple task parallelism for the different Hamiltonian's parameters, which allowed for significant, near-linear speedup in most cases, allowing to sweep parameter spaces 6 times larger at essentially no additional time}, such as ground state diagonalization or training the neural networks.
		
		The first 2 steps are finding a ground state representation and sampling observables. This task is performed by a derived class of the \texttt{FeatureSetFactory} which produces a \texttt{FeatureSet} that can easily be saved to disk using the HDF data format and provides a consistent interface for all 3 models and for learning on either correlation or spin configuration or even for the network science analysis. 
		
		The third step is performing the actual analysis itself. Since this usually involved parameter sweeps over computationally intensive tasks such as training many neural networks, checkpointing to disk and live plots were implemented by the \texttt{AnalyzerFigure} class\footnote{or rather by the 2 parents of this class to split representation and logic}. By deriving from this class, it allows for both viewing the intermediate result after any newly generated data, helping to fix mistakes early, while it also allows to resume a long-running simulation which was interrupted or failed for some parameters.
		
		\begin{figure}[hbt]
			\centering
			\includegraphics[width=0.8\textwidth]{\figpath{documentation}}
			\caption{Visual representation of the interaction between classes during an analysis within the developed framework. It is possible visualize intermediate results after calculation of a single point in parameter space. If the process is killed, a maximum of the result of one parameter point is lost.}
		\end{figure}
		\begin{figure}
			\centering
			\includegraphics[width=\textwidth]{\figpath{classdiagram.png}}
			\caption{Simplified class hierarchy of the confusion phase diagram \texttt{AnalyzerFigure} class as example of an implementation using this framework. Only the \texttt{ConfusionDetector2D} and the \texttt{ConfusionPhaseDiagram2D} classes had to be implemented as part of this analysis.}
		\end{figure}

		
		
		%\todo{If time: extend this section}
		%\chapter{List of figures}
		%\listoffigures
		
	%\end{appendix}
	\chapter*{Acknowledgements}
	I would like to thank the people for their help in writing this thesis. 
	
	First, I would like to express my gratitude to Professor Matthias Weidemüller, who allowed me to freely explore this emerging and interdisciplinary field of research, for the great amount of time he spent on this project in very helpful discussions and the inspiring ideas of which he was never short on supply.
	
	I would also like to thank my fellow students David Wellnitz, Klaus Kades, Julian Heiss, Kathinka Gerlinger and Armin Kekic for additional discussion on the methods and results presented in this thesis.
	
	I also appreciate the feedback and time spent proof-reading the thesis by Klaus Kades, David Wellnitz and Kathinka Gerlinger.
	
	Special thanks go to Lukas Kades for providing his personal Monte-Carlo Markov chain library and spending time to introduce me to his code.
	
	%\todo{Add these}
	\begingroup
    \chapter*{Bibliography}
    \addcontentsline{toc}{chapter}{Bibliography}
    \renewcommand{\chapter}[2]{}
    %\citestyle{egu}
    
    \printbibliography
	
	\endgroup
	\cleardoublepage
	\include{deposition}

\end{document}