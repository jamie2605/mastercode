\documentclass[MA.tex]{subfiles}
\begin{document}
\chapter{The Heisenberg XXZ model}
\label{chap:xxz}
We will now generalize our spin system and consider all 3 spin components. When introducing an anisotropic coupling by scaling the interaction between spins in $z$-direction by a parameter $\Delta$ with respect to the interaction of the $x$ and $y$ spin components, the resulting system is called XXZ model. The Hamiltonian of the one-dimensional XXZ model can be formulated as

\begin{equation}
 H = J\sum_i^L\left(\sigma^x_i\sigma^x_{i+1}+\sigma^y_i\sigma^y_{i+1}+\Delta\sigma^z_i\sigma^z_{i+1}\right) - h_x\sum_i^L\sigma^x_i
 \label{eq:xxz}
\end{equation}

 in terms of the Pauli matrices $\sigma^x$, $\sigma^y$, $\sigma^z$ with a coupling strength of $J$ which was set to one unless otherwise stated. Again, only nearest-neighbor interactions are considered. This is no limitation of our methods as both MPS and the neural network have shown to be able to cope with long-range interactions in Section \ref{chap:tfi:interactions}. However, as we want to compare our simulations and results to estimations performed by \cite{Dmitriev2002}, which were carried out for nearest-neighbor interactions only. In their paper, the phase diagram of this system was calculated. For reference, we show their phase diagram in Figure \ref{fig:xxz:phase-diagram}. It was calculated that there is $x$-magnetization in all regions. Additionally, there is Neel order in $z$-direction for region 1, ferromagnetic ordering in $z$-direction for region 2 and Neel order in $y$-direction for region 3.

\begin{figure}[tb]
	\centering
	\includegraphics[page=1,trim={11cm 15cm, 2cm, 6cm},clip, width=0.8\textwidth]{\figpath{XXZ.pdf}}
	\caption{Phase diagram of the one-dimensional ordered nearest neighbor XXZ model as presented in \cite{Dmitriev2002}, figure 1. There, the Hamiltonian was formulated in terms of spin operators rather than Pauli matrices, thus the h-axis must be scaled by a factor of 2 to match $h_x$ in Hamiltonian \ref{eq:xxz}. There is $x$-magnetization in all regions. Additionally, there is Neel order in $z$-direction for region 1, ferromagnetic ordering in $z$-direction for region 2 and Neel order in $y$-direction for region 3.}
	\label{fig:xxz:phase-diagram}
\end{figure}

We begin this chapter by a comparison of the spin orderings found in our simulated system with the phase diagram in Figure \ref{fig:xxz:phase-diagram}. For this comparison, we also include the disordered case. We proceed by using a machine learning method introduced in \cite{Nieuwenburg2017} that uses neural networks for phase detection in an unsupervised fashion. A new method for phase detection is then introduced in Section \ref{chap:xxz:border}. It can be categorized as being weakly supervised.
\clearpage
\section{Magnetization and Neel order}
\label{chap:xxz:orderings}
We will now simulate the one-dimensional XXZ model by both exact diagonalization and by using the same MPS algorithms and libraries as in the previous chapter. For the exact simulation, qutip\footnote{See \url{http://qutip.org} to obtain the software and both \cite{Johansson2012, Johansson2013} for the algorithms} is used to translate Hamiltonian \ref{eq:xxz} into a matrix representation which is then diagonalized for a grid of parameter values $h_x$ and $\Delta$. Spin configurations are obtained by explicitly sampling the full probability distribution of all $2^L$ possible spin configurations that is given by

$$ p_0 = p_{\upuparrows ... \updownarrows} = |\braket{\upuparrows ... \updownarrows|\psi}|^2 $$
$$ ... $$
$$ p_{(2^L-1)} = p_{\updownarrows ... \downdownarrows} = |\braket{\updownarrows ... \downdownarrows|\psi}|^2 $$

with $L$ being the number of spins and ground state $\ket{\psi}$. Since an up-down spin basis was chosen\footnote{Basis vectors $\{ \ket{\upuparrows ... \updownarrows}, \ket{\upuparrows ... \uparrow \downuparrows}, \ket{\upuparrows ... \uparrow \downdownarrows}, ..., \ket{\updownarrows ... \downdownarrows} \}$, essentially a binary number representation from 0 to $2^L$}, the probability distribution of spin configurations was obtained by simply squaring each entry of the ground state vector.

Due to the high computational demands, qutip was extended to be able to diagonalize on the GPU.\footnote{These modifications to qutip can be found in the supplied code (See appendix \ref{app:Code}), navigate to \texttt{modified\_libs/qutip.py}} While this resulted in a more than tenfold speed increase, the dense representation of the matrix resulted in a limit of 13 particles due to memory capacity of 8GB GDDR installed in the GPU used. However, offloading diagonalization to the GPU allowed for the CPU to sample concurrently, resulting in almost complete utilization of both GPU and CPU for a parameter sweep of $h_x$ and $\Delta$. Diagonalization performance could possibly be improved further by using a sparse eigensolver, transfer times could be reduced by building the Hamiltonian directly on the GPU, sampling time could be reduced by employing a Monte-Carlo Markov Chain-based algorithm. However, we will see that this simulation can be interpreted more as an excursion to disordered systems, a small intermezzo on our way to the Matrix Product State simulation of larger systems and no such optimizations were implemented.


We now turn to the results of an exact diagonalization of the of the ordered XXZ model with 10 spins as presented in  in Figure \ref{fig:xxz:exact-orderings}a. The magnetization was obtained by averaging over the absolute value of the mean spin orientation for each parameter point. Similarly, the neel order was estimated by the average number of sign changes along the sampled spin configurations. The black overlay is the phase diagram as seen in Figure \ref{fig:xxz:phase-diagram}, only $h$ is scaled by a factor of 2 to compensate the different definitions of Hamiltonian. Phases 2, 3 and 4 are visible, however the phase boundaries are shifted. Phase 2 exhibits additional features, the magnetization increases in steps as $h_x$ is incresed. These steps can be explained by the very discrete nature of the very small system. These features will vanish for a larger system size which will be simulated with MPS methods.

Figure \ref{fig:xxz:exact-orderings}b and c display simulations on the unordered XXZ model in 3 dimensions. We are unable able to simulate these systems for a large number of spins using Matrix Product State simulations, as they only work on lattices. We add disorder to the system by modifying the interaction parameter to $J = \frac{C}{r^x}$ with constants C, x and r denoting the distance between spins. For the disordered system only, we sum over all pairs of spins.

For \ref{fig:xxz:exact-orderings}b, we set $C=1$, $x=3$ and draw the spin positions from a Gaussian distribution in space with $\sigma=1$ once for for every value of ($h_x$, $\Delta$). Most of the structure vanishes, only the ferromagnetic ordering along z-direction in region 2 remains, albeit with a very smooth transition to region 4. Region 2 also seems to be stretched in $h$-direction, which is to be expected for this longer-ranged interaction. 



For the system depicted in subplot \ref{fig:xxz:exact-orderings}c, we scale our model to the parameter encountered in the experimental realization of the XXZ model in our group, based on Rydberg atoms where the cloud size is on the order of \SI{30}{\micro\meter} in $x$ and $y$-direction, and \SI{230}{\micro\meter} in $z$-direction. (See \cite{Franz2018} for details on the experiment.) We also set the interaction to zero if $r<\SI{5}{\micro\meter}$ to simulate a Rydberg blockade. Unfortunately, no structure remains in the phase diagram of long-range orderings in this regime. 

\begin{center}
	\begin{figure}[b!]
		\makebox[\textwidth]{\makebox[0.95\paperwidth]{
				\begin{subfigure}[b]{0.4\paperwidth}
					\includegraphics[trim={0 0 0 8mm}, width=\textwidth, clip]{\figpath{{19_3_20180702-132642-879723}.pdf}}
					\subcaption{ordered}
				\end{subfigure}
		}}
		
		
		\makebox[\textwidth]{\makebox[0.95\paperwidth]{
				\begin{subfigure}{0.4\paperwidth}
					\includegraphics[trim={0 0 0 8mm}, width=\textwidth, clip]{\figpath{19_3_20180702-132643-730245.pdf}}
					\subcaption{unordered, interaction coefficient $J=\frac{1}{r^{3}}$}
				\end{subfigure}
				\begin{subfigure}{0.4\paperwidth}
					\vspace{5mm}
					\includegraphics[trim={0 0 0 8mm}, width=\textwidth, clip]{\figpath{3_0_SpinConfigurationOrderings2D_2_20180830}}
					\subcaption{unordered, parameters similar to experimental realization \cite{Franz2018}}
				\end{subfigure}
				
		}}
		\caption{Magnetizations and Neel orders of the XXZ model, calculated from spin configurations sampled from the ground state which was obtained by exact diagonalization. The black overlays correspond to the phase diagram calculated in \cite{Dmitriev2002}.}
		\label{fig:xxz:exact-orderings}
		
	\end{figure}
\end{center}

Since very few features in the long-range orderings of the unordered model were found, there is very little knowledge about the phase diagram of such a system and we cannot simulate disorder far beyond 10 particles, we benchmark our phase detection methods introduced in the following section on the ordered XXZ model only.



For the ordered model, the OSMPS library is used once again to find a compressed representation of Hamiltonian \ref{eq:xxz} in Matrix Product Operator form, find a ground state and measure correlation matrices. The MPS approximation allows us to simulate the ordered, one-dimensional XXZ model for up to 50 spins on a single desktop computer. The magnetization was obtained estimated by the square root of a far offdiagonal entry of correlation matrix $\braket{\sigma^z_i\sigma^z_j}$ i.e the same way as for the quantum Ising chain. The neel order was estimated by multiplying the correlation matrix with a chessboard matrix of $1$ and $-1$. The average over the upper triangular of this resulting matrix was considered an estimate of the neel order. 

The results for simulations of 10, 20 and 50 spins are given in Figure \ref{fig:xxz:mps-orderings}. For 10 spins, we recover Figure \ref{fig:xxz:exact-orderings}a which validates the simulation. As the system size increases, the features in region 3 vanish, confirming that they were due to the very small system size. Region 1 and 3 become distinct for the system of 50 spins, however the transition between region 1 and 3 is still rather smooth for this system size. It is unclear whether this transition is a sharp phase boundary in the thermodynamic limit or whether it is a crossover. For our machine learning considerations, this distinction does not matter, as the neural network will only detect a difference in patterns between 2 phases. It should be able to find such a difference given our observations of the long range orderings.

\begin{center}
	\begin{figure}[hp]
		\makebox[\textwidth]{\makebox[0.95\paperwidth]{
				\begin{subfigure}[b]{0.4\paperwidth}
					\includegraphics[trim={0 0 0 8mm}, width=\textwidth, clip]{\figpath{3_0_CorrelationOrderings2D_3_20180830}}
					\subcaption{system size of 10 spins}
				\end{subfigure}
				
		}}
		
		
		\makebox[\textwidth]{\makebox[0.95\paperwidth]{
				\begin{subfigure}[t]{0.4\paperwidth}
					\includegraphics[trim={0 0 0 8mm}, width=\textwidth, clip]{\figpath{3_0_CorrelationOrderings2D_4_20180830}}
					\subcaption{system size of 20 spins}
				\end{subfigure}
				\begin{subfigure}[t]{0.4\paperwidth}
					\includegraphics[trim={0 0 0 8mm}, width=\textwidth, clip]{\figpath{3_0_CorrelationOrderings2D_7_20180830}}
					\subcaption{system size of 50 spins}
				\end{subfigure}
				
		}}
		\caption{Magnetizations and Neel orders of the ordered XXZ model, calculated from correlation functions obtained by the two-site variational ground state search in Matrix Product State formulation. The black overlays correspond to the phase diagram calculated in \cite{Dmitriev2002}.}
		\label{fig:xxz:mps-orderings}
		
	\end{figure}
\end{center}

\section{Automatic phase detection by confusion}
\label{chap:xxz:confusion}
Up to here, apart from the t-SNE visualization of the sampled Ising data in Section \ref{chap:classical:MCMC}, all employed machine learning methods were supervised. We now want to remove the necessity have a labeled training set and move into the domain of unsupervised machine learning. Shortly after the observable-based learning of quantum phase transitions was introduced by \cite{Carrasquilla2017}, an unsupervised method to accomplish this task was proposed by \cite{Nieuwenburg2017}.\footnote{Very shortly to be exact; both papers were published in the same issue of Nature Physics} Even though that work used the entanglement spectrum as feature for the neural network input and works on the Kitaev chain rather than the XXZ model, the method used is a solution to a quite general machine learning problem of which discovering phase transitions is likely the most important application. In general however, it should work on any binary classification problem where an ordered parameter space describing the training data needs to be linearly split into two subregions.

The method is called "Confusing neural networks" and builds on the introduced supervised observable-based approach, but does not need a labeled training set. To artificially label the training set, each point in parameter space of temperature, magnetic field, etc is assumed to be the critical point for the phase classification. All training points are then labeled according to this assumption. This procedure is repeated for many different assumed critical points. In an assumed critical point vs. prediction accuracy plot, a W shape emerges if the network was able to distinguish the 2 phases. On the center peak, the network is "least confused", therefore the peak location is assumed to be the correct critical point.

\cite{Broecker2017} applies this approach to quantum systems. The parameter is swept using a sliding window\footnote{In a one-dimensional phase-diagram, the window reduces to an interval}. The window edges are used as training points, the contents as testing points. If a change in structure lies within the window, the network can distinguish 2 different phases, otherwise it will return a random label with equal probability; the network is "maximally confused". We quantify the confidence of observing a phase transition by integration over the parameter window $[\lambda_1, \lambda_2]$:

$$ P = 2\frac{\int_{\lambda_1}^{\lambda_2} d\lambda |p(\lambda) - 0.5|}{\lambda_2-\lambda_1}$$

where $p(\lambda)$ is the networks confidence for class 0\footnote{choice is without restriction of generality, as the confidence for class 1 is $1-p$}. On a continuous parameter space, $P=0$ for a maximally confused network and 1 for a perfect classifier.



There are 2 main parameters for this algorithm: window size and scan direction. The window size must be chosen large enough for both endpoints to be in either phase, especially for small systems or for the detection of crossovers. However, choosing a larger window comes at the cost of reduced resolution. The scan direction - or sliding axis - only is relevant if the phase diagram has more than one dimension. Since the sliding window has only 1 dimension, phase boundaries parallel to the scan direction cannot be detected. In case of the XXZ model, by scanning along $h_x$ the boundaries between regions 1 and 2, as well as between 2 and 3 are therefore not detectable. A possible generalization approach for a d-dimensional phase diagram would be to use a d-dimensional window and 2d neurons in the output layer, depending on the exact definition of such a generalization, this may however be as expensive, or even equivalent to scanning along each parameter axis separately.


The results of scanning the MPS-simulated data for the one-dimensional XXZ model with 50 sites can be found in Figure \ref{fig:xxz:confusion}. The general structure of the phase diagram is recovered using this method. As expected, the phase boundaries parallel to $h_x$  are not visible when scanning in this direction.  Unexpectedly, even when scanning in $\Delta$-direction, the phase boundary between region 1 and 3 could not be detected. Looking back at \ref{fig:xxz:mps-orderings}, this is likely because the transition between these regions is very gradual. However, even with larger window sizes, this method was unable to detect the existence of two distinct phases.

With the utilized sampling resolution of the parameter space, we could not reduce the window size below 0.5, as a minimum of 4 points are needed for the sliding window. When a higher resolution of the phase diagram is needed, it is advisable to first scan the phase diagram with a large window size, return to the MPS simulation for a finer resolution along the detected borders and then a second scan only in the resampled areas of the phase diagram.

\begin{figure}[t!]
	\makebox[\textwidth]{\makebox[0.95\paperwidth]{
			\begin{subfigure}[b]{0.4\paperwidth}
				\includepdf{\textwidth}{{3_5_ConfusionPhaseDiagram2D_1delta0_5_20181008}}
				\subcaption{scanning in $\Delta$ direction, widow size 0.5}
			\end{subfigure}
			\begin{subfigure}[b]{0.4\paperwidth}
				\includepdf{\textwidth}{3_5_ConfusionPhaseDiagram2D_1delta1_20181008}
				\subcaption{scanning in $\Delta$ direction, window size 1}
			\end{subfigure}
	}}
	
	
	\makebox[\textwidth]{\makebox[0.95\paperwidth]{
			\begin{subfigure}[b]{0.4\paperwidth}
				\includepdf{\textwidth}{{3_5_ConfusionPhaseDiagram2D_1hx0_5_20181008}}
				\subcaption{scanning in $h_x$ direction, window size 0.5}
			\end{subfigure}
			\begin{subfigure}[b]{0.4\paperwidth}
				\includepdf{\textwidth}{3_5_ConfusionPhaseDiagram2D_1hx1_20181008}
				\subcaption{scanning in $h_x$ direction, window size 1}
			\end{subfigure}
			
	}}
	\caption{Applying the confusion method as used in \cite{Broecker2017} on the XXZ model. P quantifies the confidence of the neural network to observe a phase boundary at a given point in parameter space.}
	\label{fig:xxz:confusion}
\end{figure}

The major drawback of this generic approach is computation time. For every pixel in Figure \ref{fig:xxz:confusion}, we must perform an Matrix Product State ground state simulation and train a neural network instance. While this is still feasible for the simple systems and network architectures used here, this approach is too expensive for more complicated quantum and neural network models. The hyperparameters scan direction and window size increase the cost even further as expensive grid searches may be needed to optimize the results of this method.

One approach to reduce this computation cost is given in the follow-up paper by \cite{Liu2017}, in which the problem is interpreted as a contrast-finding task well known in Computer Vision. However, the employed snake algorithm limits the possible structures of the phase diagram and depends on an initial guess of the phase boundary, rendering the main advantage of being system-agnostic obsolete. 

Overall, the method is useful in detecting structural changes within a parameter space in an unsupervised manner. This phase detection can be used to create phase diagrams of systems from measurements without any further information. On top of that, the generic nature of the method may lead to further applications beyond the topic of phase transitions - or even physics - in the future.

\section{Automatic phase detection by asymptotic border cases}
\label{chap:xxz:border}
Physics is a science of methodological reductionism. In the quantum mechanics domain, Hamiltonians are reduced to a simple form that can be treated more easily and may be solvable analytically in order to provide insight to a more complicated system. The Ising model is a prime example of this philosophy, as its widespread use is not due to the close relation to an easily realizable experiment, but rather because of its simplicity.

This philosophy also manifests in the frequently discussed \emph{asymptotic limits}. By setting system parameters to 0, $\pm1$ or $\pm\infty$ the qualitative behavior of systems in intermediate parameter regimes can sometimes be deducted.

This approach is now incorporated into the observable-based machine learning of phases. Assuming a given system can be solved or simulated for certain points of the phase diagram. Will that be sufficient to recover the entire phase diagram? 

The MPS-simulated 50 spin XXZ-model is used and we assume that for a low transversal field, it is known that there are 3 different phases for the cases $\Delta \in \{0, -\infty, +\infty\}$ Additionally, it is assumed that for $\Delta = 0$, $h_x\rightarrow \infty$, different phases are observed.\footnote{In this specific case, we have strong evidence for these assumptions to be reasonable, as we can recover ferromagnetic ($\Delta\rightarrow\infty$), antiferromagnetic ($\Delta\rightarrow -\infty$) and paramagnetic ($h_x\rightarrow\infty$) phase of the Ising model} We train a single neural network instance with 4 output neurons corresponding to the 4 phases.


\begin{center}
	\begin{figure}[hbtp]
		\makebox[\textwidth]{\makebox[0.95\paperwidth]{
				\begin{subfigure}{0.37\paperwidth}
					%\includepdf{\textwidth}{3_6_Weak4PhaseDetection2D_1ideal_20180830}
					\begin{overpic}[width=0.37\paperwidth]{\figpath{3_6_Weak4PhaseDetection2D_1ideal_20180830.pdf}}
						\put (26, 82) {1}
						\put (68, 58) {2}
						\put (26, 30) {3}
						\put (84, 29) {4}
					\end{overpic}
					\subcaption{Original training selection (Very favorable selection)}
				\end{subfigure}
			
				\begin{subfigure}{0.37\paperwidth}
					%\includepdf{\textwidth}{3_6_Weak4PhaseDetection2D_1stable1_20180830}
					\begin{overpic}[width=0.37\paperwidth]{\figpath{3_6_Weak4PhaseDetection2D_1stable1_20180830.pdf}}
						\put (26, 82) {1}
						\put (68, 58) {2}
						\put (26, 30) {3}
						\put (84, 29) {4}
					\end{overpic}
					\subcaption{Robustness of phase boundary detection between regions 2 and 4}
				\end{subfigure}
				\begin{subfigure}{0.065\paperwidth}
					\vspace{-2.4cm}
					\includegraphics[width=\textwidth,clip,trim=350 30 50 20]{\figpath{15_3_20180418-142840-597172_103}}
				\end{subfigure}
		}}
		
		
		\makebox[\textwidth]{\makebox[0.95\paperwidth]{
				\begin{subfigure}{0.37\paperwidth}
					%\includepdf{\textwidth}{3_6_Weak4PhaseDetection2D_1stable2_20180830}
					\begin{overpic}[width=0.37\paperwidth]{\figpath{3_6_Weak4PhaseDetection2D_1stable2_20180830.pdf}}
						\put (26, 82) {1}
						\put (68, 58) {2}
						\put (26, 30) {3}
						\put (82, 29) {4}
					\end{overpic}
					\subcaption{Robustness of phase boundary detection between regions 3 and 4}
				\end{subfigure}
				\begin{subfigure}{0.37\paperwidth}
					%\includepdf{\textwidth}{3_6_Weak4PhaseDetection2D_1unstable_20180830}
					\begin{overpic}[width=0.37\paperwidth]{\figpath{3_6_Weak4PhaseDetection2D_1unstable_20180830.pdf}}
						\put (22, 82) {1}
						\put (68, 58) {2}
						\put (26, 30) {3}
						\put (84, 29) {4}
					\end{overpic}
					\subcaption{Robustness of phase boundary detection between regions 1 and 3}
				\end{subfigure}
			\begin{subfigure}{0.065\paperwidth}
				\vspace{-2.4cm}
				\includegraphics[width=\textwidth,clip,trim=350 30 50 20]{\figpath{15_3_20180418-142840-597172_103}}
			\end{subfigure}
				
		}}
		\caption{Influence of training data selection for the automatic phase detection by border cases applied to the ordered XXZ model. A single network instance is trained on configurations within the parameter ranges that are given by the white patches. Each of the 4 subplots corresponds to the mean test response of one output neuron. Within each neuron response subplot, the corresponding numeric class label is printed next to the training patch this label was applied to.}
		\label{fig:xxz:border}
		
	\end{figure}
\end{center}

Within seconds, the neural network was trained and tested on the MPS-simulated correlation matrices and Figure \ref{fig:xxz:border}a was obtained. All 4 regions are clearly distinguished by the neural network.

These results lead to a question we have encountered before: How critical is the selection of training patches in this case? To put this to the test, we trained the network with a variety of different training data selections. Computationally, it was quite cheap to perform this batch training compared to the previous confusion scheme, as calculating the entire phase diagram with this new method is only slightly more expensive than calculating a single pixel in the phase diagram of the confusion scheme. Three examples of this investigation are displayed in Figure \ref{fig:xxz:border}b - d. 



Again, we note that a clear boundary between the regions as seen in Figure \ref{fig:xxz:mps-orderings} translates to a robustness against unfavorable selection of training data. Not surprisingly, the predicted phase boundary between region 1 and 3 moves as the training patches move. The neural network can distinguish between the two phases, but since the change in structure between the two phases (in this case the Neel order in z-direction) is continuous, it interpolates the phase boundary to be at some seemingly arbitrary position between the 2 relevant training patches.

For all other phase boundaries, the prediction remains almost completely fixed for any selection of training data. Some minor "bleed-ins" can be observed, most significantly close to the tripoint border of regions 1, 3 and 4, visible when comparing Figure \ref{fig:xxz:border}a and d for example. This is due to the fact that the long range orderings of the simulated data\footnote{see Figure \ref{fig:xxz:mps-orderings}c} at the selected training patch and the bleed-in area are very similar. The predicted borders only start to move significantly as the training patches overlap with the phase boundary. This is to be expected even for a perfect classifier on a system where each phase contains only identical patterns which change exactly at the border.




There is one more important consideration for this method. What will happen if there are more phases than output neurons? And conversely: What if the network is passed 2 different labels for 2 areas within the same phase? To answer these questions, we trained our network once more for many different combinations of training data selections, but did not constrain each training patch to remain within its own region. 

The results can be summarized and discussed using a single training data selection as depicted in Figure \ref{fig:xxz:borderconfused}. There, neuron number 4 was moved into region 3, overspecifying this phase, while no neuron is trained on region 4.

\begin{figure}[htb!]
	\centering
	%\includepdf{0.8\textwidth}{3_6_Weak4PhaseDetection2D_1overlap_20180830}
	\begin{subfigure}{0.83\textwidth}
		\begin{overpic}[width=\textwidth]{\figpath{3_6_Weak4PhaseDetection2D_1overlap_20180830.pdf}}
			\put (21.5, 84) {1}
			\put (63.5, 59) {2}
			\put (21.5, 31) {3}
			\put (66.5, 30) {4}
		\end{overpic}
	\end{subfigure}
	\begin{subfigure}{0.145\textwidth}
		\vspace{-1.9cm}
		\includegraphics[width=\textwidth,clip,trim=350 30 50 20]{\figpath{15_3_20180418-142840-597172_103}}
	\end{subfigure}
	\caption{Automatic phase detection by asymptotic cases applied to the ordered XXZ model when missing phases or overspecifying phases. As before, a single network instance is trained on configurations within the parameter ranges that are given by the white patches. Each of the 4 subplots corresponds to the mean test response of one output neuron. Within each neuron response subplot, the corresponding numeric class label is printed onto the training patch to which this label was applied to.}
	\label{fig:xxz:borderconfused}
\end{figure}

Region 3 is clearly split into two subregions, the neural network is able to find distinct structures to separate the space. As we look back into Figure \ref{fig:xxz:mps-orderings}, we see similar structure to this predicted phase boundary within the long range orderings of the simulated measurements. The network interprets this structure as 2 different phases and labels them to be of the most similar class of the training data, as instructed. If region 3 was more homogeneous, it would be expected for each of the 2 labels within the region to be equally confident that a given test point is of their class.

Region 4 is unknown to the neural network, so it could be expected that the prediction confidence is zero for all neurons. This is not the case, we instead observe that neuron 4 claims most of the territory in region 4, neuron 1 finds the lower half of region 4 to be similar to its class and neuron 2 also has a small nonzero prediction in region 4. We can once again refer to Figure \ref{fig:xxz:mps-orderings} to explain these distributions. The fourth neuron is trained on data with no magnetization is $z$, and no neel order in $x$ and $z$-direction, as well as a $x$-magnetization value more similar to region 4 than in any other training patch. However, it also expects magnetization and neel order in y direction, which is not present in region 4. Nonetheless, as its training patch is much more similar to region 4 than any other, there is a fairly high response to this region anyway.

To conclude, this method of scanning a phase diagram by border cases may become a useful addition to the small toolbox of the emerging field of machine learning phases of matter. It is computationally cheap when compared to the confusion scheme, requires only a small amount of knowledge about the system and is robust against unfavorable selections of training data in most cases. This includes the position of the training patches in parameter space as well as the number of them as over- or underspecification of phases can be detected - at least in the case of the XXZ phase diagram.

%\todo{If time: look at non-normalized response}

\end{document}