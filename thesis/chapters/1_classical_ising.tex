\documentclass[main.tex]{subfiles}
\begin{document}
\chapter{The classical Ising model}
\label{chap:classical}

The classical Ising model is often cited as the fruit fly of statistical physics\footnote{This exact wording can be found in \cite{Selinger2016}}, as it is one of the simplest models to exhibit a phase transition yet still many thermodynamic quantities can be calculated analytically. Solution and properties of the model can be found in most textbooks of statistical physics such as \cite{Nolting2014}. The Hamiltonian is given by
$$ H = - J \sum_{i,j}S_iS_j - h \sum_iS_i $$
with the coupling strength $J$, classical spins arranged on a regular lattice $S_i \in \{1, -1\}$ and  the magnetic field $h = \mu\mu_0H$, which we will set to 0. We set the coupling strength to 1 for nearest neighbors, and zero otherwise and are left with the simple Hamiltonian
$$ H = - \sum_{<i, j>}S_iS_j $$

where $<i, j>$ denotes the sum over nearest neighbors only. 

Without magnetic field, this model does not exhibit a phase transition in one dimension as calculated by \cite{Ising1925}, but it does have one in 2 dimensions as is shown by \cite{Onsager1944} who first presented an analytical solution of this model. The transition is of second order, the order parameter is the spontaneous magnetization. It can be calculated according to
$$M = \frac{\sum_{\{S_i\}} \sum_iS_i\exp(-\beta H)}{\sum_{\{S_i\}}\exp(-\beta H)} $$
in the canonical ensemble. $\{S_i\}$ denotes all possible spin configurations. \cite{Yang1952} first published an analytic expression for the spontaneous magnetization of this model in the thermodynamic limit at temperature\footnote{Throughout this thesis, the temperature $T$ is always dimensionless and scaled according to $T=\frac{k_BT'}{J}$ with $T'$ as temperature in Kelvin} $T$:

$$ M = \left\{\begin{array}{lcr}
			(1-\sinh^{-4}(\frac{2}{T}))^\frac{1}{8} & T < T_c\\
			0 & T > T_c
			\end{array}\right\}
$$
with Boltzmann constant $k_B$ and critical temperature $T_c$ given by
$$ T_c = \frac{2}{\ln(1+\sqrt{2})} = 2.269 $$

At temperatures below $T_c$ there is a ferromagnetic phase i.e. at very low temperatures, all spins are aligned. Without an external magnetic field however, there is a degeneracy between all spins having the value +1 or -1, meaning the system exhibits $\mathbb{Z}_2$ symmetry. Once the spin lattice freezes in one of these 2 alignments, it is very unlikely for a global spin flip driven by temperature to occur. 

At temperatures above $T_c$ there is a paramagnetic phase i.e. the magnetization is zero, and all spins are randomly pointing up or down with equal probability. No long-range ordering exists.

At the critical point $T=T_c$, a thermodynamic quantity $F$ of any system can scale in reduced temperature $t = \frac{T-T_c}{T_c}$ according to a power law with a \textit{critical exponent}\footnote{In a more general way, the critical exponent is typically defined as $\phi = \lim_{t \rightarrow 0} \frac{\ln\left|F(t)\right|}{\ln t} $ and it is distinguished between left and right-sided limits} $\phi$: $$F \sim t^{\pm\phi} $$
The sign of the exponent is chosen such that $\phi$ is always positive.

The critical scaling exponents of selected quantities of the 2D Ising model as obtained from analytical calculations are given in the following table.

\vspace{1cm}
\begin{tabular}{|c|c|c|c|}
	\hline 
	$\beta$ & $\gamma$ & $\delta$ & $\nu$ \\ 
	order parameter & susceptibility  & critical isotherm  & correlation length \\ 
	\hline 
	0.125 & 1.75  & 15 & 1 \\ 
	\hline 
\end{tabular}
\vspace{5mm}


These values were taken from table 4.1 in \cite{Nolting2014}, as was the formulation of the critical behavior.

\vspace{5mm}

To continue, we will now sample synthetic measurements from the Ising model in Chapter \ref{chap:classical:MCMC}, followed by Chapter \ref{chap:classical:results}, which mostly reproduces results of \cite{Carrasquilla2017} to establish a baseline machine learning model and evaluation metric for the following chapters. We will then investigate the seemingly rather technical aspects of training data selection and limitation in Chapter \ref{chap:classical:selection}.
	


\section{Monte-Carlo Markov chain sampling}
\label{chap:classical:MCMC}

A standard approach for the simulation of many-body problems in classical statistical physics is the usage of algorithms based on a Monte-Carlo Markov chain. We are employing the Metropolis algorithm introduced in \cite{Metropolis1953}, as it allows us to sample microscopic states of our Ising model according to the Boltzmann distribution.\footnote{The Metropolis Hastings algorithm developed in \cite{Hastings1970} generalizes this method to arbitrary probability distributions.} Monte Carlo methods generally exhibit a favorable scaling in high dimensions and this sampling task is no exception.

The partition function of the classical Ising model in a canonical ensemble is given by

$$ Z = \sum_{\{\mathbf{s}\}} \exp\left[-\frac{1}{k_BT}\left(\frac{1}{2}\sum_{<i, j>}(1-s_is_j)\right)\right] $$

With $\{\mathbf{s}\}$ as permutation over all possible spin configurations, and the Boltzmann constant $k_B$.

The thermal average of a given quantity A such as magnetization is given by 
$$\langle A\rangle = \frac{1}{Z} \sum_{\{\mathbf{s}\}} A(\mathbf{s}) \exp\left(-\frac{H(\mathbf{s})}{k_BT}\right)$$

While this allows us to both calculate magnetization\footnote{or any arbitrary thermal average of a quantity} and even sample microscopic states by sampling from $ p(\mathbf{s}) \propto \exp\left( -\frac{H(\mathbf{s})}{k_BT}\right)$ in principle, the number of terms in these 2 expressions scales with $2^N$ in particle number $N$, making this approach unfeasible for larger systems. Monte Carlo sampling allows us to sample from the probability distribution without having to evaluate all $2^N$ terms explicitly  using a stochastic process which generates a Markov chain of points in phase space. A Markov chain generally is a sequence of events with the property that for any given state $\mathbf{s_t}$, the transition probability of observing a given state $\mathbf{s_{t-1}}$ as next element of the chain is determined solely by the state $\mathbf{s_t}$.

The Metropolis algorithm \cite{Metropolis1953} provides an explicit formulation of the transition probability required to ensure that for a chain growing to infinite length the 2 following properties are fulfilled:
\begin{enumerate}
	\item The state distribution within the Markov chain converges to the Boltzmann distribution
	\item Ergodicity: Every point in phase space is visited
\end{enumerate}
These properties are fulfilled by first drawing a random new state in phase space $\mathbf{s_{t+1}}$. If the energy change $\Delta E = H(\mathbf{s_{t+1}}) -H(\mathbf{s_{t}})$ is negative, append the new spin configuration to the Markov chain. If it is positive, append the new configuration to the Markov chain with a probability of $\exp\left(\frac{\Delta E}{k_BT}\right)$, otherwise reject the new state. These instructions can equivalently be formulated in a more compact transition probability of
$$ p = \min\left(1, \exp\left(\frac{\Delta E}{k_BT}\right)\right). $$

For nearest neighbor spin models, the algorithm can be simplified further by performing only single spin flips for each step. In that case, $\Delta E$ can be calculated by taking only the neighboring sites into account which greatly reduces the cost of a single step.

While the simplicity of this algorithm makes it very easy to implement\footnote{See \texttt{tools/ClassicalIsingSimulator.py} in the supplied code (Appendix \ref{app:Code}) for an implementation in less than 30 lines of python code}, the algorithm does have certain disadvantages. Starting from a random state, a burn-in phase is usually added before any sampling is performed to overcome the problem of the Markov chain having a long tail if the starting state had a very low probability and many spin flips are needed to move into a more likely domain of the feature space. Even after this phase, very many steps may be needed to converge to the Boltzmann distribution. Another problem, especially when implementing the algorithm with single spin flips, is a high degree of autocorrelation, meaning long sections of the Markov chain may contain very similar configurations, especially for low temperatures. All these issues require many Monte Carlo-steps per sampled point. Since a single MC step is very cheap on the computational side, the algorithm is still much less expensive than an explicit sampling, and for larger system sizes it is the only viable option.

For any sampled Monte-Carlo data, it is very important to assess whether the sampled distributions match expectations and theory. In case of the Ising model, the biggest challenge for the algorithm is to fully exhibit $ \mathbb{Z}_2 $-symmetry for low temperatures within the sampled configurations. In Figure \ref{fig:MCdata}, magnetization and magnetization trajectories are plotted for the 3 different MC libraries that were used. The data was sampled on a 16x16 lattice, 1000 configurations per temperature value T ($ T \in [1, 4]$).

\paragraph{Own implementation} A custom Monte-Carlo Markov chain implementation was written in 30 lines of Python code implementing the algorithm exactly as described above. This algorithm took about the same time to generate the MC data as the other 2 libraries, while performing an order of magnitude fewer MC steps between each sampled point and only a quarter of the number of temperature values. As we can see in the top right subplot of Figure \ref{fig:MCdata}, even for temperatures just below $T_c$, there is not a single global spin flip. While this could be overcome by simply starting multiple Markov chains from different random starting configurations and combining the sampled data, the sampled data of one instantiation is not correctly distributed.

\paragraph{MC library by Lukas Kades} Lukas Kades from the Theoretical Physics Institute in Heidelberg kindly provided his code for the simulation of spin systems using Monte-Carlo methods, written in C++.\footnote{This code is not in a public repository, please contact Lukas Kades (l.kades@thpys.uni-heidelberg.de)  if interested in the code} The code was extended to be able to sample full microscopic spin configurations. For temperatures just below $T_c$ some global spin flips can be observed. For low temperatures however, no global spin flip is present.


\begin{figure}[h!]
	\begin{center}
		\begin{subfigure}[t]{0.49\textwidth}
			\includetikz{0.95}{5cm}{1_0_Magnetization1D_1_20180828}
		\end{subfigure}
		\begin{subfigure}[t]{0.49\textwidth}
			\includetikz{0.95}{5cm}{1_0_Magnetization1D_2_20180828}
		\end{subfigure}
		\begin{subfigure}[t]{0.49\textwidth}
			\includetikz{0.95}{5cm}{1_0_Magnetization1D_3_20180828}
		\end{subfigure}
		\begin{subfigure}[t]{0.49\textwidth}
			\includetikz{0.95}{5cm}{1_0_Magnetization1D_4_20180828}
		\end{subfigure}
		\begin{subfigure}[t]{0.49\textwidth}
			\includetikz{0.95}{5cm}{1_0_Magnetization1D_5_20180828}
			\subcaption{Mean of the absolute value of the magnetization in dependence of the temperature T (scaled by coupling constant).}
		\end{subfigure}
		\begin{subfigure}[t]{0.49\textwidth}
			\includetikz{0.95}{5cm}{1_0_Magnetization1D_6_20180828}
			\subcaption{Magnetization trajectory during MCMC sampling. Color indicates scaled temperature.}
		\end{subfigure}
		\caption{Comparison of spin configurations sampled with 3 different Monte-Carlo Markov chain libraries. From top to bottom: Own naive implementation, C++ library by Lukas Kades, MC module of ALPS library. Only ALPS exhibits proper $ \mathbb{Z}_2 $-symmetry for low temperatures.}
		\label{fig:MCdata}
	\end{center}
	
\end{figure}
\afterpage{\clearpage}


\paragraph{The ALPS library} The "algorithms and libraries for physics simulations project" started with the arguably ambitious goal of "providing high-end simulation codes for strongly correlated quantum mechanical systems as well as C++ libraries for simplifying the development of such code. ALPS strives to increase software reuse in the physics community"\footnote{As stated on the project's home page, see \cite{ALPSProjectPage}}. The attempt of providing open source code for simulation of many-body systems is very important for the effectiveness of research on these systems. The MC codes of the ALPS library exhibited the highest performance and highest quality of sampled data, obeying $ \mathbb{Z}_2 $-symmetry for all temperatures below $T_c$. This is most likely due to the more advanced clustering algorithms used in the Alea sublibrary of ALPS as clustering methods drastically increase the probability of a global spin flip at low temperatures. Unfortunately however, the ALPS library prove quite difficult to compile, use and extend, especially because of dependencies that were difficult to completely satisfy and the documentation was very limited. 



%The code seemed poorly documented, and the online documentation is fragmented and too short to quickly grasp the implementation ideas. It is unfortunate that such an otherwise excellent library has such a steep learning curve even for a comparatively "simple" usage case. Intensive work on the library's documentation would most likely be required for a more widespread adoption.

The library was also extended to be able to sample full microscopic spin configurations as this quantity was not included in the list of observables that ALPS can sample out of the box.




\vspace{1cm}

Due to the high performance of the library and high quality of the sampled data by it, the ALPS library was chosen to generate the datasets needed for all following analyses of the classical Ising model.


\section{Machine learning and the phase diagram}
\label{chap:classical:results}
We are interested in the capability of machine learning methods to distinguish phases. We use the dataset generated in the previous chapter and interpret the spin configurations as points in a 256-dimensional vector space as feature space for our algorithms. Since there are only 2 possible values for each spin variable, all points lie on the edge of a 256-dimensional hypercube.


\paragraph{Unsupervised learning} Before training our models on finding two distinct phases, we employ methods from the field of unsupervised learning to find patterns in our data. A common technique of this field which is mostly used to aid visualization of high-dimensional spaces is the t-SNE algorithm. It fits an embedded, low dimensional space for visualization that aims to preserve the structure of the high dimensional original space. To achieve this, the algorithm first assigns a joint probability as similarity measure pairs of data points in the original vector space as well as in the lower dimensional embedded space. It then minimizes the Kullback-Leibler divergence between the two representations. For details, refer to \cite{VanDerMaaten2008}. Figure \ref{fig:tsne} visualizes the 2-dimensional embedded space that resulted from applying t-SNE on the sampled Monte-Carlo dataset\footnote{The dimensionality of the system was reduced using principal component analysis (PCA, see \cite{Hotelling1933}) before applying t-SNE to reduce computational cost.}. Three main clusters are clearly visible, two corresponding to the low-temperature spin configurations and one corresponding to the high-temperature configurations. This means that even the $\mathbb{Z}_2$-symmetry is visualized using this algorithm.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.65\textwidth]{\figpath{1_2_TSNEView_3Alps_20180828.pdf}}
	\caption{t-SNE plot for the dataset sampled using the Monte-Carlo Markov chain algorithm. Colored by temperature $T$. (16x16 model, $T \in \left[1, 4\right]$). The $x$ and $y$-coordinates denote position in the embedded space.}
	\label{fig:tsne}
\end{figure}


Finding clusters using an unsupervised learning approach is neither a necessary nor sufficient criteria for a supervised approach to classify the data correctly in a mathematical sense. However, it is a strong indication for a classification task to perform well if an unsupervised approach already places different labels in different clusters.

\paragraph{Supervised learning} We will now use the same classification approach that was used in \cite{Carrasquilla2017} and use a fully connected neural network\footnote{All networks of this thesis were implemented in TensorFlow, see tensorflow2015} to classify the dataset using the 2 labels ferromagnetic and paramagnetic. Neural networks are not the only model successful at this task, please see Appendix \ref{app:othermethods} for other machine learning models that were tested. We supply many spin configurations of each phase along with a phase label to the network during training. We then present unseen spin configurations to the network and record the neuron response in the output layer, which is the confidence of the neural network to observe a given phase and is generally normalized to 1.

\begin{center}
	\begin{figure}[bh]
		\makebox[\textwidth]{\makebox[0.95\paperwidth]{
				\begin{subfigure}{0.4\paperwidth}
					\includepgf{0.55}{1_4_DenseNetworkFigure_11layer_20180828}
					\subcaption{Single hidden layer}
				\end{subfigure}		
				\begin{subfigure}{0.4\paperwidth}
					\includepgf{0.55}{1_4_DenseNetworkFigure_1deep_20180828}
					\subcaption{Deep network architectures}
				\end{subfigure}
				
		}}
		\caption{Response of the 2 output neurons for different network architectures. Trained on spin configurations in $T>3$ and $T<1.5$. The color indicates the number of neurons in each hidden layer, colored vertical lines correspond to the respective neuron crossing. This crossing is estimated to be at the largest temperature value in the test set for which the mean response of the ferromagnetic neuron is larger than the mean response of the paramagnetic neuron. The black vertical line indicates the value for $T_c$ in the thermodynamic limit.}
		\label{fig:classical:response}
	\end{figure}
\end{center}

We train the neural network on spin configurations at temperatures below $T=1.5$ and above $T=3$. We test the network on the remaining, unseen configurations to produce Figure \ref{fig:classical:response}. We observe that even though the algorithm was never trained on configurations close to the vicinity of $T_c$, the neural network can distinguish the 2 phases with high accuracy. On the right hand side of the same figure, we evaluated the performance of models with more layers. As they did not increase prediction accuracy, we used the simplest working model, which consisted of only 1 hidden layer.




%\todo{Mention small bias}


\paragraph{Neuron response scaling} Since the neuron response in Figure \ref{fig:classical:response} is not a sharp step function and the smoothening of the step does not seem to depend strongly on the neural net architecture, it is a reasonable hypothesis to assume that this observation is due to finite size effects. We will now investigate this hypothesis by looking at the scaling of the neuron response in system size. The relevant critical exponent for this scaling is $\nu$, coming from the correlation length. For the classical 2D Ising, this exponent has been calculated analytically and is known to be $\nu=1$  (\cite{Nolting2014}). 
\begin{figure}[bht]
	
	\makebox[\textwidth]{\makebox[0.95\paperwidth]{
			\begin{subfigure}{0.45\paperwidth}
				\includepgf{0.8}{1_5_LScalingFigure_1_20180828}
				\subcaption{Scaled Neuron response}
			\end{subfigure}
			\begin{subfigure}{0.45\paperwidth}
				\includepgf{0.8}{1_5_Magnetization1DScalingClassicalIsing_2_20180828}
				\subcaption{Scaled magnetization}
			\end{subfigure}
	}}
	\caption{Neuron response and magnetization vs. temperature when temperature is scaled according to critical scaling. Reduced temperature $t$ is given by $t = \frac{T-T_c}{T}$, $L$ is the system length. The vertical colored lines indicate the corresponding neuron crossing, the black line indicates correct critical temperature in the thermodynamic limit. Neglecting finite size effects, one universal curve is obtained for both plots.}
	\label{fig:classical:scaling}
	
\end{figure}
By scaling the reduced temperature $t = \frac{T-T_c}{T}$ with $L^\frac{1}{\nu}$, we expect one universal curve close to $T_c$ for both magnetization and neuron response.



The result of the scaling is plotted in Figure \ref{fig:classical:scaling}. Except for very small systems, there is a universal behavior for all system sizes close to the critical point. Interestingly, the neuron response curves are universal on a larger temperature regime than the magnetization.


\section{Effect of training data selection}
\label{chap:classical:selection}

Up to now, we have fixed the test interval to the arbitrary temperatures $T \in [1.5, 3]$ and trained on configurations above and below these temperatures. However, it would be possible for the prediction of $T_c$ to depend on this arbitrary choice. In fact, the intuitive choice to use an interval symmetric around the already known critical temperature does not translate well to the application of this method to systems with unknown critical points.

Assuming the neural network learns the actual order parameter magnetization, to which there are strong indications\footnote{See appendix \ref{app:whatislearned}}, a symmetric interval may not even be the ideal choice for this system as the magnetization is not symmetric around $T_c$. The first indication to support this point is the bias in $T_c$ that was observed in Figure \ref{fig:classical:response}.

We therefore now investigate the interpolation behavior of the neural network by performing a grid sweep over possible training intervals, or rather over the complementary test intervals. The predicted critical temperatures for this sweep can be found in Figure \ref{fig:classical:asymmetric}.
\begin{figure}[h!]
	\includepdf{\textwidth}{1_6_AsymmetricTrainingFigure_1_20181022}
	\caption{Bias in predicted critical temperature for different selections of training data. For a given coordinate, the network was trained on configurations with $ T < T_{begin} $ and $T > T_{end}$ and evaluated on a test set of configurations within the corresponding intermediate temperature regime. The crossing of the neuron output was used as predicted critical temperature. The bias to $T_c$ is given by the color axis. Intervals symmetric to $T_c$ (image diagonal) exhibit no bias, while asymmetric intervals cause a bias.}
	\label{fig:classical:asymmetric}
\end{figure}

The results mostly confirm the intuition that symmetric intervals lead to a smaller bias than asymmetric ones. However, the $T_c$ bias on the diagonal is still non-zero. Again, this is likely due to the fact that the neural network learns a measure of magnetization. During training, the neural network is presented with magnetization values of zero for the paramagnetic phase and values between 1 and $1-\epsilon$ for the ferromagnetic phase, depending on the training interval selection. If a test spin configuration now has a magnetization of 0.1 for example, the neural network seems to label it as paramagnetic rather than ferromagnetic as along the previously seen configurations during training, this configuration will be more similar to known configurations with the paramagnetic label. This system-dependent bias will likely not improve even for very large systems, as the neural network is unable to interpolate the non-trivial behavior of the magnetization curve without being trained on configurations in the intermediate regime.

It would however be possible to train the network over the entire parameter space to improve generalization. The model could also be extended to incorporate the typical behavior of the order parameter into the network architecture itself. Here this might be achievable by simply introducing custom activation functions for the paramagnetic and ferromagnetic neuron, strongly damping or exaggerating high input respectively. This is one example of the introduction of domain knowledge to a machine learning problem to improve prediction performance. In this thesis, we will focus on what neural networks are able to learn without domain knowledge, as the Ising model is already completely solved when including such knowledge\footcite{Onsager1944}.

For the application to an unknown system, the observed bias is small enough to find the critical point with an algorithm similar to the bisection method: Once an arbitrary test interval is chosen, the predicted critical point can be used to symmetrize the interval around this point and repeat this process until the predicted critical point is stable which should reduce this bias significantly.

In appendix \ref{app:biasfix}, we sample the training data across the entire parameter space to remove this bias completely when classifying phases on the quantum Ising chain. For that model, this bias behavior becomes much more evident.

\section{Effect of limited measurement data}
Acquiring measurement data is expensive. While todays experiments are typically highly automated and scientists no longer have to painstakingly prepare and record each individual data point, measurement time is a large concern for a wide range of experiments. Quantum systems are by no means an exception.

Therefore, required measurement time must also be taken into account when proposing new analysis methods such as this machine learning approach. Many modern methods from computer science - including many from the domain of machine learning and network science - became widely successful only after training sets of sufficient size and the processing power to evaluate the models on these sets were readily available. This development is especially true for neural networks. With early theory basing on models of the brain introduced by \cite{Rosenblatt1958} and the basics for a training algorithm available by \cite{Dreyfus1962}, only by 1993 could these networks outperform many other methods in patter-recognition competitions due to the computational costs required to train larger models with a large amount of training data. Since graphical processing units (GPUs) have been used for training starting 2006, winning models of such competitions are deep neural networks with overwhelming majority, see \cite{Schmidhuber2014} for many examples. All this was due to the large amounts of data that could now be processed by these neural networks.

Our neural network model is very simple, containing one hidden layer of 100 neurons. Still, this translates to about 26\,000 weights that need to be fitted using the training data. We therefore pose the question: How much data do we need to saturate these weights?

The synthetic measurement data can be arranged in a matrix of 120\,000 rows (measurements) by 256 columns (sites).\footnote{During training outside of $T_{test} \in [1.5, 3]$, we used 48,000 rows of that matrix} There are 2 conceptually different ways to restrict the data for training: row-wise and column-wise, resulting in limitation of measured points and observed sites per measurement, respectively. We will investigate both cases and find that the spin site restriction holds more insight than initially expected.

\paragraph{Training point reduction}
The first possible approach to reduce measurement data is to decrease the number of measurements. This analysis has already been performed by \cite{Mills2018}. While this work does not directly focus on phase transitions, the 2D Ising model is also investigated using machine learning methods on the aspect of limited training data, please refer especially to figure 4 of that paper. 

Within this thesis, we perform the training point reduction only in the context of the bias-less learning of the phase transition of the quantum Ising chain, see appendix \ref{app:biasfix} for the results. The model and methods for the analysis will be introduced in the following chapter. Impressively, as little as 5 training points are sufficient to perform the classification successfully on the quantum Ising chain.




\paragraph{Symmetry learning by site reduction} Rather than limiting the number of measurement points, we no longer observe the complete spin configuration in every measurement. Instead, we either fix a selection of sites before the experiment and only measure these sites or we fix a number of sites and randomly draw which sites to observe each measurement. The results of these 2 approaches are given in Figure \ref{fig:classical:subselection}a and b respectively. The results are very similar; shuffling sites each measurement allows for a slightly lower number of selected sites for the same prediction quality. 

\begin{figure}[ht]
	\thispagestyle{empty}
	\centering
	%		\makebox[\textwidth]{\makebox[0.95\paperwidth]{
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth,clip,trim=0 0 0 8.5mm]{\figpath{1_7_DataShufflingFigure_1select_no_shuffle_20180828}}
		\subcaption{Randomly selecting number of sites to observe once before the experiment started. The number of sites observed is given by the color axis.}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth,clip,trim=0 0 0 8.5mm]{\figpath{1_7_DataShufflingFigure_1select_site_20180828}}
		\subcaption{Randomly selecting number of sites to observe, shuffle sites after every observation. The number of sites observed is given by the color axis.}
	\end{subfigure}
	
	
	\begin{subfigure}{0.6\textwidth}
		\centering
		\vspace{1cm}
		\includegraphics[width=0.9\textwidth,clip,trim=0 0 0 8.5mm]{\figpath{1_7_DataShufflingFigure_1shuffle_modes_20180828}}
		\subcaption{Different shuffle modes when selecting 20 sites. Random site selection corresponds to subfigure a, random spin selection to subfigure b. Shuffle mode full means drawing 20 random spins from the full training matrix and considering them as new measurement}
	\end{subfigure}
	%		}}
	\caption{Neuron response vs. temperature when selecting only a subset of the full spin configuration. The black vertical line indicates the analytically known critical temperature of the thermodynamic limit, the colored lines indicate the location of the neuron crossing.}
	\label{fig:classical:subselection}
\end{figure}
\afterpage{\clearpage}

As the number of observed sites is reduced, the contrast between the 2 neuron outputs drops expectedly. However, even when observing only 20 of the 256 sites, the phase transition is still clearly visible. Another small drawback is a slightly longer convergence time for the reduced dataset, but a single network instance still converge in a matter of seconds, most likely much cheaper than measuring more data.

This technique obviously requires certain symmetry of the order parameter. It is intuitive that by observing only a random subselection of sites in each measurement, an estimate of the magnetization can be obtained. Can we maybe use this statement constructively and find symmetries within a system? To test this, we select 20 sites of the system and completely shuffle the training data matrix within each temperature value. The average magnetization was therefore zero at every temperature due to $\mathbb{Z}_2$-symmetry. As to be expected, Figure \ref{fig:classical:subselection}c shows the neural network is no longer able to distinguish the 2 phases.

Therefore, a machine learning algorithm can be used to test system symmetries. For a given symmetry, apply its generating transformation to the training data. If the phases can still be identified on the new training set, the system is invariant under this transformation. If the phases can no longer be distinguished using the algorithm, it is likely that the system does not contain the given symmetry.

From a machine learning perspective, this method can also be interpreted as data augmentation. It is a standard technique, especially in the domain of image recognition to rotate, scale, skew and add noise to the image in the dataset, thus increasing the number of training points artificially. (See \cite{Taylor2017} for an overview of this method.) This improves prediction accuracies on unseen images, as the algorithm generalizes better beyond these transformations. This again is an opportunity to use domain knowledge to improve our model, or in this case, the training data for our model. A limited measurement set can therefore be enhanced by for example adding mirrored versions of the lattice to the dataset, increasing the effective training set size by a factor of 4.




\end{document}