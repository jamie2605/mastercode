\documentclass[main.tex]{subfiles}
\begin{document}
\chapter{Conclusion}
% \addcontentsline{toc}{chapter}{Conclusion}

We now want to summarize and discuss these results in the light of our original question: Can machine learning be a helpful tool in unveiling the underlying structure of a physical system?

\vspace{5mm}

In Chapter \ref{chap:classical}, microscopic spin configurations of the classical 2-dimensional Ising model were simulated using a Monte-Carlo Markov Chain algorithm and it was found that even the unsupervised t-SNE algorithm was able to find 3 main clusters in the data corresponding to a magnetization of -1, 1 and 0. It was shown that a neural network of a single hidden layer could distinguish paramagnetic and ferromagnetic phase when supplied a labeled training set, even if the training set does not contain points close to the critical point. The neuron output scales in system size with the critical exponent $\nu$. By applying transformations to the input data, the symmetry of the order parameter can be tested.

In Chapter \ref{chap:tfi}, the correlation matrix $\braket{\sigma^z_i\sigma^z_j}$ of a quantum Ising chain are simulated using Matrix Product States. A neural network was trained to successfully distinguish the paramagnetic and ferromagnetic phases. It was observed that if some training data between $h_x \in [0, h_c]$ was excluded, the neural network response for magnetic field values just below $h_c$ was only a smooth step function, resembling the transverse magnetization curve. This resulted in a negative bias of the predicted critical point. The prediction robustness against added noise was tested. The robustness was much higher when compared to an approach from network science. The neural network model also generalized to additional terms in the Hamiltonian introduced by long-range interactions.

In the final chapter, 2 methods were used to map out the two-dimensional phase diagram of the ordered, one-dimensional XXZ-model. The confusion method was able to identify all phase boundaries except for one in a completely unsupervised approach with only 2 hyperparameters to tune. The newly introduced weakly supervised method for automatic phase detection based on an initial phase guess which may be derived from asymptotic considerations was able to find all phase boundaries, to some extent even if the initial phase guess missed phases or had 2 proposed phases within the same phase.

\vspace{5mm}
At which point have we gained knowledge by applying machine learning? Assuming we did not know anything about the three presented models but simply had access to a measurement set of spin configurations and correlation functions, would we still have performed the same investigations and gained knowledge? 

It would definitely always be possible and even a standard approach to use unsupervised machine learning to visualize our data, such as t-SNE. By correlating the observed clusters with temperature or magnetic field, this would allow us to even label the dataset deep within the clusters, allowing our supervised phase transition approach without knowledge of phase transitions. In principle, the phenomenon of phase transitions could be found using this procedure.

Assuming that the general existence of a phase transition within a system known, but the order parameter is not, transformations as described in Section \ref{chap:classical:selection} can reveal symmetry of the order parameter. Or one of the two methods of Chapter \ref{chap:xxz} can directly map out phase diagrams.

These are just some examples where machine learning could have provided scientific progress by indicating interesting patterns that may hint at an underlying structure. These examples are limited to phase transitions in spin system, but this observable-based approach is much more general, as machine learning provides many methods for pattern recognition not tied to narrow applications and strong assumptions.

Only time can tell which fields of science will be able to productively use the pattern recognition power of machine learning on new problems. This will definitely also depend on the progress of interpretable machine learning, since the availability of methods and tools to better understand the more complex machine learning models will heavily influence the usefulness of these models to scientific research in the context of pattern recognition. In the meanwhile, there is a variety other scientific applications that profit from powerful regression models. These applications range from the already mentioned search for ground states of quantum systems to finding optimized evaporation ramps in ultra-cold atom experiments (\cite{Wigley2016}).

A significant amount of work has already been invested in creating methods that use machine learning to aid the scientific community in the past two years and it is currently a very active field of research. With the progress that has been seen in such a short amount of time, it will be very interesting to see what the near future holds. Overall, it becomes evident that tools from machine learning beyond low-dimensional regression for fitting a predetermined model to measurement data will become more and more important to the progress of scientific research.
\end{document}